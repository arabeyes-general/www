<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Main Include Point
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("output.inc.php");
require_once("mainlib.inc.php");
require_once("todos.inc.php");
require_once("credentials.inc.php");

//Global variables

$keywords = 'linux, unix, arabic, arabize, arabization, aunyx, arabic linux, arabic unix, linux arabization';
$desc = "Linux/Unix Arabization";

$ContactAntiSpamEmail = 'contact@_ANTISPAM_arabeyes.org';
$PasswordMinLength = 8;

// if(eregi("(htdig)([0-9]{1,2}.[0-9]{1,3}){0,1}",
//           $_SERVER['HTTP_USER_AGENT'],$match)) {
if(strstr($_SERVER['REMOTE_ADDR'], "216.18.0.60") ||
   strstr($_SERVER['REMOTE_ADDR'], "66.35.250.208"))
{
  // do nothing
  // this is to avoid multiple links to the same url because of
  // the php sessionid generated on each iteration through the
  // web pages
}
else
{
  @session_start();
}

@mysql_select_db($db_name, mysql_pconnect($db_host, $db_user, $db_pass));

SetLanguage($_SESSION[locale]);
?>
