<?php
/************************************************************************
 * $Id: nominate.php,v 1.0 2004/10/08 00:04:00 omar
 *
 * ------------
 * Description:
 * ------------
 * Arabeyes.org's Nomination PHP code
 *
 * -----------------
 * Revision Details:
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(isset($_SESSION[username]))
{
  # Nomination Code
  $mytime = GetDateYMDHM();
  $sql = "SELECT * from elections WHERE nominate_start_date < '".$mytime."'
          AND nominate_end_date > '".$mytime."'";
  $QueryResult = mysql_query($sql);
  # Find out if there is a valid nomination period
  if ($QueryRow = mysql_fetch_array($QueryResult))
  {
    # check if user has a cvs-commit in the last xx days
    $cvs_year=substr($QueryRow[cvs_commit_c],0,4);
    $cvs_mon=substr($QueryRow[cvs_commit_c],5,2);
    $cvs_day=substr($QueryRow[cvs_commit_c],8,2);
    $cvs_hour=substr($QueryRow[cvs_commit_c],11,2);
    $cvs_min=substr($QueryRow[cvs_commit_c],14,2);
    $cvs_time=$cvs_year."-".$cvs_mon."-".$cvs_day." ".$cvs_hour.":".$cvs_min;
    $test=GetUserLastCommit($_SESSION[user_id]);
    if ($test > $cvs_time)
    {
      # Show user which elections he might nominate himself
      if ($_GET[elections])
      {
	$buffer = showelectionform($_GET[electionid], 0);
	$buffer.="<br><br>";
	$buffer.=shownominees($_GET[electionid], 0);
	DisplayPage('Election Details', 'Election Details '."'".$QueryRow[title]."'", $buffer, DisplayJSLimitField());
      }
      else
      {    
	$nominee = $_POST[nominee] ? 1 : 0;

        # check if he has already nominated himself to this election
	$sql="select * from votes where election_id=".$QueryRow[id]." and
              user_id=".$_SESSION[user_id];
	$QueryResult1=mysql_query($sql);
	if (($QueryRow1 = mysql_fetch_array($QueryResult1)) && !$_POST[modify])
	{
	  $Buffer  = "<br>You have <b>already nominated youself</b> for
                      the '".$QueryRow[title]."' elections. ";
	  if ($QueryRow1[nominee] != 1)
	  {
	      $Buffer .= "<b>BUT</b> your nomination status is inactive,
                          please make sure to active it on the bottom of this
                          page else your forfit your ability to be considered
                          for elections.";
	  }
	  $Buffer .= "<br><br>You are welcome to modify you Campaign
                      Pledges below.<br><br>";
	  $Buffer .= NominateForm($QueryRow1[nominate_blurb],
				  $QueryRow1[nominee],
				  $QueryRow[id],
				  $QueryRow[title]
				  );
	}
	else
	{
#	  if ($_POST[submit]=="Submit Nomination")
	  if ($_POST[submit])
	  {
            # check if user already has an entry (gone through modifications)
	    $sql = "select * from votes where election_id=".$QueryRow[id]."
                    and user_id=".$_SESSION[user_id];
	    $QueryResult1=mysql_query($sql);
	    if ($QueryRow1=mysql_fetch_array($QueryResult1))
	    {
	      $sql="update votes set nominee='".$nominee."', 
                    nominate_blurb='".$_POST[nominate_blurb]."'
                    where election_id=".$QueryRow[id]."
                    and user_id=".$_SESSION[user_id];
	      $QueryResult1=mysql_query($sql);
	      $Buffer="<b>Congratulations !! </b> ";
	      if ($_POST[modify])
	      {
		$Buffer .= "You have successfuly modified your entry for the '";
	      }
	      else
	      {
		$Buffer.="You are now officially nominated for the '";
	      }
	      $Buffer.= $QueryRow[title]."' elections.";
	      if (!$_POST[modify])
	      {
		$Buffer.="<br><br>Here is your nomination text:<br>";
		$Buffer.=$_POST[nominate_blurb];
	      }
	    }
	    else
	    {
	      $sql="insert into votes (user_id, election_id, nominee,
                    nominate_blurb) values (".$_SESSION[user_id].",".$QueryRow[id]
                    .",".$nominee.",'".$_POST[nominate_blurb]."');"; 
	      $QueryResult1=mysql_query($sql);
	      $Buffer="Congratulations !! You have nominated yourself.<br><br>\n";
	      $Buffer.="Here is your nomination text:<br>";
	      $Buffer.="<i>".$_POST[nominate_blurb]."</i>";
	    }
	  }
	  else
	  {
	    $Buffer = NominateForm("", 1, $QueryRow[id], $QueryRow[title]);
	  }
	}
      }
    }
    else
    {
      $Buffer .= "Sorry, you don't have the proper credentials to Nominate
                  yourself.";
    }
  }
  else
  {
    $Buffer  = "There doesn't seem to be a valid Nomination period.<br><br>";
    $sql = "select * from elections where vote_end_date > '".$mytime."'";
    $QueryResult = mysql_query($sql);
    if ($QueryRow = mysql_fetch_array($QueryResult))
    {
      $Buffer .= "There is however an election coming up on '"
                  .$QueryRow[vote_start_date]."' which you are
                  encouraged to <a href=\"vote.php\">vote</a>
                  for in earnest<br><br>";
      $sql = "select * from votes where election_id=".$QueryRow[id]." and user_id=".$_SESSION[user_id]." and nominee = 1";
      $QueryResult1 = mysql_query($sql);
      if ($QueryRow1 = mysql_fetch_array($QueryResult1))
      {
	$Buffer .= "<b>Reminder:</b> You are a nominee in the aforementioned
                    election :-)";
      }
    }
  }
}
else
{
  DisplayError('You are not correctly logged in.');
}

DisplayPage('Nominate', 'Nominate', $Buffer, DisplayJSLimitField());
?>
