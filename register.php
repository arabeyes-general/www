<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * User Registration Page
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(!empty($_SESSION['freecap_word_hash']) && !empty($_POST['word']))
{
  // all freeCap words are lowercase.
  // font #4 looks uppercase, but trust me, it's not...
  if( $_SESSION['hash_func'](strtolower($_POST['word'])) ==
      $_SESSION['freecap_word_hash'] )
  {
    // reset freeCap session vars
    // cannot stress enough how important it is to do this
    // defeats re-use of known image with spoofed session id
    $_SESSION['freecap_attempts'] = 0;
    $_SESSION['freecap_word_hash'] = false;

    $captcha_ok = 1;
  }
  else
  {
    $captcha_ok = 0;
  }
}

if($_POST[submit] == 'Register')
{
  if (!$captcha_ok)
  {
    // We need to call freecap.php to change the CAPTCHA image/word in case someone hits BACK
    DisplayError('The CAPTCHA verification code entered was incorrect.
                 Please try again below.<br>
                 <img src="freecap/freecap.php" id="freecap"
                  style="visibility:hidden; height:0pt; width:0pt"><br>'.ShowRegistrationForm($_POST));
  }
  else if (!IsFormComplete($_POST))
  {
	DisplayError('Please fill out the form in its entirety. All fields are required.<br><br>'.ShowRegistrationForm($_POST));
  }
  else if(!IsCountryValid($_POST[country]))
	  {
	  DisplayError('Please select a valid Country of Origin<br><br>'.ShowRegistrationForm($_POST));
	  }
	  else if(!IsCountryValid($_POST[residence]))
		{
		DisplayError('Please select a valid Country of Residence<br><br>'.ShowRegistrationForm($_POST));
		}
  else if(IsEmailValid($_POST[email]) && $_POST[passwd] == $_POST[vpasswd])
  {
	$QueryResult = mysql_query("select id from user where email='$_POST[email]'");
	if(mysql_num_rows($QueryResult) > 0)
	{
	  DisplayError('This account has already been registered with us. Please email us at <b>contact at arabeyes dot org</b> if you have any further questions or concerns about your account registration.<br>Thank you.');
	  exit;
	}

		mysql_query("insert into user set username='".NewUserName($_POST[fname], $_POST[lname])."',lname='$_POST[lname]',fname='$_POST[fname]',birthdate='$_POST[bdate]',country='$_POST[country]',phone='$_POST[phone]',residence='$_POST[residence]',type='$_POST[type]',ixexp='$_POST[ixexp]',homepage='$_POST[homepage]',about='$_POST[comments]',email='$_POST[email]',created='".GetUTCTimeStamp()."',pgp='$_POST[pgp]',pass=PASSWORD('$_POST[passwd]'),cvspass='$_POST[passwd]',howheard='$_POST[howheard]',active='Y',lastlogin='".GetUTCTimeStamp()."',modified='".GetUTCTimeStamp()."'");

	SendRegistrationNotification(GetUserIDFromEmail($_POST[email]));
	// update_cvspass($_POST[passwd], $_POST[email]);
	//do this also in user.php - whenever a user udates his password, the cvspass must be updated too
	/*
	   function update_cvspass($new_cvspass, $username)
	   {
	   $cmd = 'htpasswd -b /share/CVS/CVSROOT/passwd '.$username.' '.
	   $new_cvspass;
	   system ($cmd);
	   }
	 */
	SendRegistrationConfirmation($_POST[fname], $_POST[lname], $_POST[email]);

	DisplayPage('Thank You !', 'Thank You and Welcome !', ShowRegistrationOK(), '');
  }
  else
  {
	DisplayError('The information provided in the form did not process correctly. This is most likely due to an invalid email address or passwords. Please fix those errors and try again.<br>Thank you<br><br>'.ShowRegistrationForm($_POST));
  }
}
else
{
  DisplayPage('Register', 'Join Us !', ShowRegistrationForm($_POST), '');
}
?>
