<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Arabization Contributors and Evangelists (ACE)
 * interview page.
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

// If I'm here due to a specific ACE, pull their info forward
if (isset($_GET[ace]))
{
  if ($_GET[ace] == 'shaikli')
  {
    $ace_title = "Interview with Nadim Shaikli - Aug 15, 2002";
    $filename  = "ace/ace1_" .$_GET[lang]. ".html";
  }   
  else if ($_GET[ace] == 'aljadhai')
  {
    $ace_title = "Interview with AdbulRahman Aljadhai - Feb 8, 2003";
    $filename  = "ace/ace2_" .$_GET[lang]. ".html";
  }   
  else if ($_GET[ace] == 'sameer')
  {
    $ace_title = "Interview with Mohammed Sameer - July 29, 2003";
    $filename  = "ace/ace3_" .$_GET[lang]. ".html";
  }   
  else if ($_GET[ace] == 'pournader')
  {
    $ace_title = "Interview with Roozbeh Pournader - Dec 17, 2003";
    $filename  = "ace/ace4_" .$_GET[lang]. ".html";
  }   
  else if ($_GET[ace] == 'alrasheedan')
  {
    $ace_title = "Interview with Ahmad Al-Rasheedan - Dec 17, 2003";
    $filename  = "ace/ace5_" .$_GET[lang]. ".html";
  }   
  else if ($_GET[ace] == 'khayat')
  {
    $ace_title = "Interview with Ossama Khayat - Jun 6, 2004";
    $filename  = "ace/ace6_" .$_GET[lang]. ".html";
  }   
  else if ($_GET[ace] == 'perrier')
  {
    $ace_title = "Interview with Christian Perrier - Sept 29, 2004";
    $filename  = "ace/ace7_" .$_GET[lang]. ".html";
  }   
  else if ($_GET[ace] == 'medini')
  {
    $ace_title = "Interview with Arafat Medini - Dec 30, 2004";
    $filename  = "ace/ace8_" .$_GET[lang]. ".html";
  }   

  // Now we have the file, display it
  $fd           = fopen($filename, "r");
  $ace_contents = fread($fd, filesize($filename));
  fclose($fd);
  DisplayPage('ACE', $ace_title, $ace_contents, '');
}
else
{
  DisplayPage('ACE', 'Arabization Contributors and Evangelists ', ACE(), '');
}

?>
