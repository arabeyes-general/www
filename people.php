<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * The PEOPLE Page ;-)
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if($_GET[username] && $_GET[history])
{
  DisplayPage('Arabeyes\' People', GetUserFLNamesLink(GetUserID($_GET[username])).'\'s CVS Commits', ShowUserCVSCommits($_GET[username]), '');
}
else if($_GET[userid])
{
  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
    DisplayPage('Arabeyes\' People', GetUserFLNames($_GET[userid]).'\'s Page [<a href="admin.php?user=yes&&edit=yes&&userid='.$_GET[userid].'">Edit</a>]', ShowUser($_GET[userid]), '');
  else
    DisplayPage('Arabeyes\' People', GetUserFLNames($UserID).'\'s Page', ShowUser($UserID), '');
}
else if($_GET[username])
{
  $UserID = GetUserID($_GET[username]);
  
  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
    DisplayPage('Arabeyes\' People', GetUserFLNames($UserID).'\'s Page [<a href="admin.php?user=yes&&edit=yes&&userid='.$UserID.'">Edit</a>]', ShowUser($UserID), '');
  else
  {
	if($UserID == '')
	{
	  DisplayPage('Arabeyes\' People', 'No Such User', ShowUser($UserID), '');
	}
	else
	{
	  DisplayPage('Arabeyes\' People', GetUserFLNames($UserID).'\'s Page', ShowUser($UserID), '');
	}
  }
}
else
{
  if($_GET[all]) {
    DisplayPage('Arabeyes\' People', 'All Arabeyes Committers !', ShowAllUsers('all'), '');
  }
  else {
    DisplayPage('Arabeyes\' People', 'All Arabeyes Committers !', ShowAllUsers('active'), '');
  }
}
?>
