<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * User Preferences and Settings Page
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(!isset($_SESSION[username]))
{
  DisplayError('You are not logged in to the system. How do you think you are going to change your user settings exactly ? Log in first, then try to change your settings ;-)');
  exit;
}

if($_POST[submit] == 'Update')
{
$sqlquery= "update user set lname='$_POST[lname]',fname='$_POST[fname]',birthdate='$_POST[bdate]',country='$_POST[country]',phone='$_POST[phone]',residence='$_POST[residence]',homepage='$_POST[homepage]',pgp='$_POST[pgp]',email='$_POST[email]',about='$_POST[comments]',modified='".GetUTCTimeStamp()."' where id=$_SESSION[user_id]";
  mysql_query($sqlquery) or die("$sqlquery\nInvalid query: " . mysql_error());

  DisplayOK('Your preferences have been successfully modified. Make sure everything is current and up-to-date so we can maintain continuous communication ;-)');
}
else if($_POST[submit] == 'Change Password') 
{
  $QueryResult = mysql_query("select * from user where id=$_SESSION[user_id]");
  $QueryRow = mysql_fetch_array($QueryResult);
  
  if(IsPasswordCorrect($_SESSION[user_id], $_POST[opass]) && $_POST[npass] == $_POST[vpass])
    {
      mysql_query("update user set pass=PASSWORD('$_POST[npass]') where id=$_SESSION[user_id]");
      
      DisplayOK('Your password was successfully changed. Be sure to keep it to yourself and periodically change it. This password change currently does not affect your CVS account, but soon will be integrated.');
    }
  else
    {
      DisplayError('I was unable to change your old password to the new one. Please make sure you have filled out the form correctly. If this problem persists, let it email you a newly generated password and then change it. If that doesn\'t work, please contact the system administrator: <b>contact (at) arabeyes.org</b>.');
    }
}
else
{
  DisplayPage('User Preferences', '', ShowUserPreferences($_SESSION[user_id]), '');
}
?>
