<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Switch the Display Language
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

$_SESSION[locale] = $_GET[locale];

header("Location: ". $hosturl);
?>
