<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Donations Page
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

DisplayPage('Donate', '', Donate(), '');
?>
