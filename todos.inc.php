<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Todos Module Library
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

$States = array("Pending", "InProgress", "Done", "Rejected", "Obsolete");
$Priorities = array("High", "Normal", "Low");
$Levels = array("Hard", "Average", "Easy");
$Publics = array("Yes", "No");
$Trashs = array("Yes", "No");

function GetTodoUserName($userid)
{
  if($userid == -1)
    return "None";
  if($userid == -2)
    return "All";
  
  $QueryResult = mysql_query("select * from user where id='$userid'");
  $QueryRow = mysql_fetch_array($QueryResult);
  
  return $QueryRow[username];
}

function GetTodoUserInfo($userid)
{
  if($userid == -1)
    return "None";
  if($userid == -2)
    return "All";
  
  $QueryResult = mysql_query("select * from user where id='$userid'");
  $QueryRow = mysql_fetch_array($QueryResult);
  $UserInfo = $QueryRow[fname]." ".$QueryRow[lname]." (".$QueryRow[username].")";
  return $UserInfo;
}

function GetTodoUserNameLink($userid)
{
  if($userid == -1 || $userid == -2)
    return GetTodoUserName($userid);
  else
    return '<a href="people.php?username='.GetTodoUserName($userid).'">'.GetTodoUserName($userid).'</a>';
}

function GetTodoUserInfoLink($userid)
{
  if($userid == -1 || $userid == -2)
    return GetTodoUserInfo($userid);
  else
    return '<a href="people.php?username='.GetTodoUserName($userid).'">'.GetTodoUserInfo($userid).'</a>';
}

function GetTodoUserProjects($userid)
{
  if($userid == '')
   return;

  $UserProjects[] = "-1";//Private
  $UserProjects[] = "-4";//'Arabeyes - Common'
  
  //Core
  $QueryResult = mysql_query("select * from user where id=$userid");
  $QueryRow = mysql_fetch_array($QueryResult);
  if($QueryRow[type] == 'c')
    $UserProjects[] = -2;
  
  if($userid == 94 || $userid == 138)//WWW - The Web Admin(s) here 
    $UserProjects[] = -3;
  
  $QueryResult = mysql_query("select * from proj_contrib where userid=$userid");
  
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
    {
      $QueryRow = mysql_fetch_array($QueryResult);
      $UserProjects[] = $QueryRow[proj_id];
    }
  
  return $UserProjects;
}

function GetTodoUserAccessLevel($projectid, $userid)
{
  if($projectid == -1 || $projectid == -2 || $projectid == -3)
    return 2;

  $QueryResult = mysql_query("select * from user where id=$userid");
  $QueryRow = mysql_fetch_array($QueryResult);
  if($projectid == -4 && $QueryRow[type] == 'c')
    return 2;
  else
   {
      $QueryResult = mysql_query("select * from proj_contrib where proj_id='$projectid' and userid='$userid'");
      $QueryRow = mysql_fetch_array($QueryResult);
      return $QueryRow[access_level];
   }
}

function GetTodoProjectName($projectid)
{
  if($projectid == -1)
    return "Private";
  if($projectid == -2)
    return "Core";
 if($projectid == -3)
    return "WWW";
  if($projectid == -4)
    return "Arabeyes - Common";

  $QueryResult = mysql_query("select * from proj_about where proj_id='$projectid'");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[proj_name];
}

function GetTodoProjectLink($projectid)
{
  if($projectid < 0)
    return GetTodoProjectName($projectid);
  else
    return '<a href="project.php?proj='.GetTodoProjectName($projectid).'">'.GetTodoProjectName($projectid).'</a>';
}

function GetTodoProjectContributors($projectid)
{
  if($projectid == -1)//Private - Only the current user is a contributor
    {
      $ProjectContributors[] = $_SESSION[user_id];
      return $ProjectContributors;
    }
  
  $ProjectContributors[] = -1;//None
  $ProjectContributors[] = -2;//All
  
  if($projectid == -2)//Core
    {
      $QueryResult = mysql_query("select * from user where type='c' order by lname,fname,username");
      
      for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  $ProjectContributors[] = $QueryRow[id];
	}
      
      return $ProjectContributors;
    }
  
  if($projectid == -3)//WWW
    {
      //The web administrator(s) here
      $ProjectContributors[] = 94;
      $ProjectContributors[] = 138;
      return $ProjectContributors;
    }
  
  if($projectid == -4)//'Arabeyes - Common'
    {
      $QueryResult = mysql_query("select * from user where active='Y' order by lname,fname,username");
      for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  $ProjectContributors[] = $QueryRow[id];
	}
      
      return $ProjectContributors;
    }
  
  $QueryResult = mysql_query("select * from proj_contrib where proj_id='$projectid'");
  
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
    {
      $QueryRow = mysql_fetch_array($QueryResult);
      $ProjectContributors[] = $QueryRow[userid];
    }
  
  return $ProjectContributors;
}

function DefineTodoProjectVars($projectid)
{
  global $Publics;
  
  if($projectid == -1)
    $Publics = array("No");
  else
    $Publics = array("Yes", "No");
}

function ShowSelectField($VisibleName, $FieldName, $Array, $VisibleArray, $InitialValue)
{
  $Buffer = '<tr valign="top" align="left"><td>'.$VisibleName.'</td><td><select name="'.$FieldName.'">';
  for($i = 0; $i < count($Array); $i++)
    {
      if($Array[$i] == $InitialValue)
	$Buffer .= '<option selected value="'.$Array[$i].'">'.$VisibleArray[$i];
      else
	$Buffer .= '<option value="'.$Array[$i].'">'.$VisibleArray[$i];
    }
  $Buffer .= '</select></td></tr>';

  return $Buffer;
}

function GetPriorityColor($priority)
{
  global $Priorities;
  
  if($priority == $Priorities[0])
    return "#FF6600";
  else if($priority == $Priorities[1])
    return "#FFCC00";
  else
    return "#FFFF99";
}

function GetStateColor($state)
{
  global $States;
  
  if($state == $States[0])
    return "#FF6600";
  if($state == $States[1])
    return "#FFFF00";
  else if($state == $States[2])
    return "#33CC00";
  else
    return "#C0C0C0";
}

function GetTrashColor($trash)
{
  global $Trashs;
  
  if($trash == $Trashs[0])
    return "#C0C0C0";
  else
    return "#FFFFFF";
}

function GetPublicColor($public)
{
  global $Publics;
  
  if($public == $Publics[count($Publics)-1])
    return "#FFFFFF";
  else
    return "#FF6600";
}

function GetLevelColor($level)
{
  global $Levels;
  
  if($level == $Levels[0])
    return "#FF6600";
  else if($level == $Levels[1])
    return "#FFCC00";
  else
   return "#FFFF99";
}

function ShowPublicProjectTodos($ProjectID)
{
  global $Publics;
  DefineTodoProjectVars($ProjectID);

  $Buffer = "";

  $QueryResult = mysql_query("select * from todos where projectid=$ProjectID and public='$Publics[0]' order by date");
  
  if(mysql_num_rows($QueryResult) == 0)
    {
      $Buffer .= 'No public todos.<br>';
    }
  else
    {
      $Buffer .=  '<center>';
      $Buffer .=  '<table align="center" width="95%" border>';
      $Buffer .=  '<tr valign="top">';
      $Buffer .=  '<th>ID</th>';
      $Buffer .=  '<th>Due Date</th>';
      $Buffer .=  '<th>Priority</th>';
      $Buffer .=  '<th>State</th>';
      $Buffer .=  '<th>Assigned To</th>';
      $Buffer .=  '<th>Description</th>';
      $Buffer .=  '</tr>';
      
      for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  
	  $Buffer .=  '<tr valign="top">';
	  $Buffer .=  '<td><a href="viewtodo.php?todoid='.$QueryRow[todoid].'">'.$QueryRow[todoid].'</a></td>';
	  $Buffer .=  '<td>'.$QueryRow[date].'</td>';
	  $Buffer .=  '<td style="background-color:'.GetPriorityColor($QueryRow[priority]).'">'.$QueryRow[priority].'</td>';
	  $Buffer .=  '<td style="background-color:'.GetStateColor($QueryRow[state]).'">'.$QueryRow[state].'</td>';
	  $Buffer .=  '<td>'.GetTodoUserNameLink($QueryRow[executorid]).'</td>';
	  $Buffer .=  '<td>'.$QueryRow[description].'</td>';
	  $Buffer .=  '</tr>';
	}
      $Buffer .=  '</table></center><br>';
      $Buffer .=  'Contribute by choosing an unassigned todo ("Assigned To"=None) or a shared todo ("Assigned To"=All).<br>';
    }
  
  return $Buffer;
}

function AddTodo($ProjectID, $UserID)
{
  global $States, $Priorities, $Levels, $Publics, $Trashs;
  DefineTodoProjectVars($ProjectID);
  
  $Buffer = '<form method="post" action="addtodo.php?projectid='.$ProjectID.'">';
  $Buffer .= '<table border=0 cellpadding=2 cellspacing=2 width="100%">';
  
  $Buffer .= ShowForm($ProjectID, date("Y-m-d"), $Priorities[1], "Put a description here", "Put details about the todo here", $UserID, $Levels[1], $Trashs[1], $Publics[count($Publics)-1], $States[0]);
  
  $Buffer .= '</table>';
  
  $Buffer .= '<br>';
  $Buffer .= '<input type=submit name="add" value="Add" align=center>';
  $Buffer .= '<input type=submit name="cancel" value="Cancel" align=center>';
  
  $Buffer .= '</form>';
  
  return $Buffer;
}

function ViewTodo($TodoID, $UserID)
{
  $QueryResult = mysql_query("select * from todos where todoid=$TodoID");
  $QueryRow = mysql_fetch_array($QueryResult);
  
  $Buffer = '<form method="post" action="viewtodo.php?todoid='.$TodoID.'">';
  $Buffer .= '<table border=0 cellpadding=2 cellspacing=2 width="100%">';
  $Buffer .= '<tr valign="top" align="left"><td>Created</td><td>'.$QueryRow[creationdate].' by '.GetTodoUserInfoLink($QueryRow[creatorid]).'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Modified</td><td>'.$QueryRow[lastmodificationdate].' by '.GetTodoUserInfoLink($QueryRow[lastmodifierid]).'</td></tr>';
  
  $Buffer .=  ShowForm($QueryRow[projectid], $QueryRow[date], $QueryRow[priority], $QueryRow[description], $QueryRow[details], $QueryRow[executorid], $QueryRow[level], $QueryRow[trash], $QueryRow['public'], $QueryRow[state]);
  $Buffer .=  '</table>';
  $Buffer .=  '<br>';
  
  if(GetTodoUserAccessLevel($QueryRow[projectid], $UserID) >= 2)
    {
      $Buffer .=  '<input type=submit name="update" value="Update" align=center>';
      $Buffer .=  '<input type=submit name="delete" value="Delete" onClick="return ConfirmDelete()" align=center>';
    }
  $Buffer .=  '<input type=submit name="cancel" value="Cancel" align=center>';
  
  $Buffer .=  '</form>';
  
  return $Buffer;
}

function ShowForm($projectid, $date, $priority, $description, $details, $executorid, $level, $trash, $public, $state)
{
  global $States, $Priorities, $Levels, $Publics, $Trashs;
  DefineTodoProjectVars($projectid);
  
  $UserProjectId[] = $projectid;
  $UserProjectName[] = GetTodoProjectName($projectid);
  $ProjectContributorsIds = GetTodoProjectContributors($projectid);
  foreach($ProjectContributorsIds as $Id)
    $ProjectContributorsNames[] = GetTodoUserInfo($Id);
  
  $Buffer = '<tr valign="top" align="left"><td>Project</td><td>'.GetTodoProjectLink($projectid).'</td></tr>';
  
  $Buffer .= ShowSelectField("Public Todo", "publicfield", $Publics, $Publics, $public);
  $Buffer .= ShowSelectField("Priority", "priority", $Priorities, $Priorities, $priority);
  $Buffer .= ShowSelectField("State", "state", $States, $States, $state);
  $Buffer .= ShowSelectField("Level", "level", $Levels, $Levels, $level);
  $Buffer .= ShowSelectField("Put in Trash", "trash", $Trashs, $Trashs, $trash);
  $Buffer .= ShowSelectField("Assign To", "executorid", $ProjectContributorsIds, $ProjectContributorsNames, $executorid);
  
  $Buffer .= '<tr valign="top" align="left"><td>Due Date</td><td><input name="date" value="'.$date.'" maxlength=10 size=20 type=text></td></tr>';
  $Buffer .= '<tr valign="top" align="left"><td>Description</td><td><input name="description" value="'.$description.'" maxlength=50 size=50 type=text></td></tr>';
  $Buffer .= '<tr valign="top" align="left"><td>Details</td><td><textarea name="details" rows=40 cols=75>'.$details.'</textarea></td></tr>';
  
  return $Buffer;
}

function ViewPublicTodo($TodoID)
{
  $QueryResult = mysql_query("select * from todos where todoid=$TodoID");
  $QueryRow = mysql_fetch_array($QueryResult);
  
  $Buffer = '<table border=0 cellpadding=2 cellspacing=2 width="100%">';
  $Buffer .=  '<tr valign="top" align="left"><td>Created</td><td>'.$QueryRow[creationdate].' by '.GetTodoUserInfoLink($QueryRow[creatorid]).'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Modified</td><td>'.$QueryRow[lastmodificationdate].' by '.GetTodoUserInfoLink($QueryRow[lastmodifierid]).'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Project</td><td>'.GetTodoProjectLink($QueryRow[projectid]).'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Priority</td><td>'.$QueryRow[priority].'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>State</td><td>'.$QueryRow[state].'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Level</td><td>'.$QueryRow[level].'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Assigned To</td><td>'.GetTodoUserInfoLink($QueryRow[executorid]).'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Due Date</td><td>'.$QueryRow[date].'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Description</td><td>'.$QueryRow[description].'</td></tr>';
  $Buffer .=  '<tr valign="top" align="left"><td>Details</td><td>'.$QueryRow[details].'</td></tr>';
  $Buffer .=  '</table>';
  
  return $Buffer;
}

function ShowAllPublicTodos()
{
  $QueryResult = mysql_query("select * from proj_about where hide='N' order by type desc,proj_name");
  
  $Buffer = DisplaySection(GetTodoProjectLink(-4), ShowPublicProjectTodos(-4));
  
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
    {
      $QueryRow = mysql_fetch_array($QueryResult);
      
      $Buffer .= '<br>'.DisplaySection(GetTodoProjectLink($QueryRow[proj_id]), ShowPublicProjectTodos($QueryRow[proj_id]));
    }
  
  return $Buffer;
}

function ShowAllUserTodos($UserID)
{
  $UserProjects = GetTodoUserProjects($UserID);
  
  $Buffer = "";
  
  for($i = 0; $i < count($UserProjects); $i++)
    {
      if($i != 0)
	$Buffer .= '<br>';
      
      $Buffer .= ShowAllProjectTodos($UserProjects[$i], $UserID);
    }
  
  return $Buffer;
}

function ShowAllProjectTodos($ProjectID, $UserID)
{
  $Buffer = "";
  
  if(GetTodoUserAccessLevel($ProjectID, $UserID) >= 2)
    $Buffer .= '<a href="addtodo.php?projectid='.$ProjectID.'">Add a new todo</a><br><br>';
  
  if($ProjectID == -1)
    $QueryResult = mysql_query("select * from todos where projectid=$ProjectID and creatorid=$UserID order by date");
  else
    $QueryResult = mysql_query("select * from todos where projectid=$ProjectID order by date");
  
  if(mysql_num_rows($QueryResult) == 0)
    {
      $Buffer .= 'No todos.<br>';
    }
  else
    {
      $Buffer .= '<center>';
      $Buffer .= '<table align="center" width="95%" border>';
      $Buffer .= '<tr valign="top">';
      $Buffer .= '<th>ID</th>';
      $Buffer .= '<th>Due Date</th>';
      $Buffer .= '<th>Priority</th>';
      $Buffer .= '<th>State</th>';
      $Buffer .= '<th>Assigned To</th>';
      $Buffer .= '<th>Description</th>';
      $Buffer .= '</tr>';
      
      for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  
	  $Buffer .= '<tr valign="top">';
	  $Buffer .= '<td><a href="viewtodo.php?todoid='.$QueryRow[todoid].'">'.$QueryRow[todoid].'</a></td>';
	  $Buffer .= '<td>'.$QueryRow[date].'</td>';
	  $Buffer .= '<td style="background-color:'.GetPriorityColor($QueryRow[priority]).'">'.$QueryRow[priority].'</td>';
	  $Buffer .= '<td style="background-color:'.GetStateColor($QueryRow[state]).'">'.$QueryRow[state].'</td>';
	  $Buffer .= '<td>'.GetTodoUserNameLink($QueryRow[executorid]).'</td>';
	  $Buffer .= '<td>'.$QueryRow[description].'</td>';
	  $Buffer .= '</tr>';
	}
      
      $Buffer .= '</table></center>';
    }
  
  return DisplaySection(GetTodoProjectLink($ProjectID), $Buffer);
}

function EmailProjectContributors($TodoID, $Flag)
{
  global $hosturl;
  
  $QueryResult = mysql_query("select * from todos where todoid=$TodoID");
  $QueryRow = mysql_fetch_array($QueryResult);
  
  $State = 'added';
  if($Flag == 1)
    $State = 'updated';
  if($Flag == 2)
    $State = 'deleted';
  
  $Message = '
Salam,

This is to inform you that todo item #'.$TodoID.' has been '.$State.'.

  Project      : '.GetTodoProjectName($QueryRow[projectid]).'
  Public       : '.$QueryRow['public'].'
  Todo creator : '.GetTodoUserInfo($QueryRow[creatorid]).'
  Last modifier: '.GetTodoUserInfo($QueryRow[lastmodifierid]).'
  Assignee     : '.GetTodoUserInfo($QueryRow[executorid]).'
  Due Date     : '.$QueryRow[date].'
  Priority     : '.$QueryRow[priority].'
  State        : '.$QueryRow[state].'
  Level        : '.$QueryRow[level].'
  Description  : '.$QueryRow[description].'

'.$QueryRow[details];
  
  if($Flag == 0 || $Flag == 1)
    {
      $Message .= '
--------------------------------------------------
Please review at your earliest convenience,

 + Login to your arabeyes web account
 + '.$hosturl.'viewtodo.php?todoid='.$TodoID;
    }
  
  $Message .= '

Arabeyes.org\'s Todo Manager.';
  
  $ProjectContributorsIds = GetTodoProjectContributors($QueryRow[projectid]);
  foreach($ProjectContributorsIds as $Id)
    {
      if($Id > 0)
	SendMail("Arabeyes Todo Manager <support@arabeyes.org>", "Arabeyes Support <support@arabeyes.org>", GetUserFLNames($Id).' <'.GetUserEmail($Id).'>', "", "", "Todo #".$TodoID.' '.$State. ' ('.GetTodoProjectName($QueryRow[projectid]).')', $Message);
    }
}
?>
