<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Main Page (Index)
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if($_GET[forgotpass] && !isset($_SESSION[username]))
{
  if($_POST[Submit])
    {
      $QueryResult = mysql_query("select * from user where username='$_POST[username]' and email='$_POST[email]'");
  
      if(mysql_num_rows($QueryResult) != 1)
	{
	  DisplayError('Username "'.$_POST[username].'" and Email "'.$_POST[email].'" don\'t match !');
	}
      else
	{
	  $NewPassword = NewPassword();
	  
	  mysql_query("update user set pass=PASSWORD('$NewPassword') where username='$_POST[username]'");
	  
	  SendNewPassword(GetUserID($_POST[username]), $NewPassword);
	  
	  DisplayPage('OK', 'User Password Emailed', 'An email to "'.$_POST[username].', '.$_POST[email].'" has been sent with the new password. Please be sure to check your mail. You should receive it in a few minutes.', '');
	}
    }
  else if($_POST[Cancel])
    {
      header("Location: ". $hosturl);
    }
  else
    { 
      DisplayPage('Forgot Password', 'Enter the Required Information', ForgotPassword(), '');
    }
}
else if($_POST[submit] == 'Login')
{
  if (isset($_POST[uname]) && isset($_POST[pass]))
    {
      $QueryResult = mysql_query("select * from user where username='$_POST[uname]' and pass=PASSWORD('$_POST[pass]')");
      if(mysql_num_rows($QueryResult) == 1)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  
	  $_SESSION[username] = $QueryRow[username];
	  $_SESSION[user_id] = $QueryRow[id];
	  $_SESSION[type] = $QueryRow[type];
	  
	  mysql_query("update user set lastlogin='".GetUTCTimeStamp()."' where username='$_POST[uname]'");
	  
	  header("Location: ". $hosturl);
	}
      else
	{
	  DisplayError('Wrong login !<br><br><a href="index.php?forgotpass=yes">Forgot Password ?</a>');
	  
	}
    }
}
else
{
  DisplayPage('Welcome', '', Index(), '');
}
?>
