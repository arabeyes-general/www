<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Adding a Todo to the List of the Actual Connected User's Todos
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if($_POST[add] || $_POST[cancel])
{
  if($_POST[add])
    {
      mysql_query("insert into todos (creatorid,lastmodifierid,executorid,projectid,priority,date,creationdate,lastmodificationdate,description,details,level,trash,public,state) values ('$_SESSION[user_id]','$_SESSION[user_id]','$_POST[executorid]','$_GET[projectid]','$_POST[priority]','$_POST[date]','".date("Y-m-d H:i:s", date("U")-date("Z"))."','".date("Y-m-d H:i:s", date("U")-date("Z"))."','$_POST[description]','$_POST[details]','$_POST[level]','$_POST[trash]','$_POST[publicfield]','$_POST[state]')");
      
      $QueryResult = mysql_query("select max(todoid) as todoid from todos");
      $QueryRow = mysql_fetch_array($QueryResult);
      
      EmailProjectContributors($QueryRow[todoid], 0);
      
      header("Location: todos.php");
      
    }
  if($_POST[cancel])
    {
      header("Location: todos.php");
    }
}
else
{
  $UserProjects = GetTodoUserProjects($_SESSION[user_id]);
  $Test = false;
  
  foreach($UserProjects as $Project)
    if($Project == $_GET[projectid])
      $Test = true;
  
  if(!$Test || !isset($_SESSION[user_id]))
    DisplayError('You are not one of this project\'s contributors, so you can\'t add a todo to it.');
  else
    DisplayPage('Add Todo', 'Add a New Todo to '.GetTodoProjectLink($_GET[projectid]), AddTodo($_GET[projectid], $_SESSION[user_id]), '');
}
?>
