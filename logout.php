<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Logout: Kill Session and Redirect to Main Page
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

session_destroy();

header("Location: ".$hosturl);
?>
