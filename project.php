<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Projects Page
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(GetUserAccessLevel($_SESSION[user_id], $_GET[projectid]) > 1 || $_SESSION[type] == 'c')
{
  if($_GET[project])
    {
      if($_POST[Add])
	{
	  mysql_query("insert proj_about set proj_name='$_POST[name]',about='$_POST[about]',status='$_POST[status]',type='$_POST[type]',listid=$_POST[listid],aim='$_POST[aim]',comment='$_POST[comment]',startdate='$_POST[startdate]',hide='$_POST[hide]',notes='$_POST[notes]',downloads='$_POST[downloads]',screenshots='$_POST[screenshots]',links='$_POST[links]'");
	  header("Location: project.php");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from proj_about where proj_id=$_GET[projectid]");
	  mysql_query("delete from proj_history where proj_id=$_GET[projectid]");
	  mysql_query("delete from proj_contrib where proj_id=$_GET[projectid]");
	  mysql_query("delete from todos where projectid=$_GET[projectid]");
          //empty project's cvs
	  //remove project's logo, screenshots etc
	  header("Location: project.php");
	}
      else if($_POST[Update])
	{
	  mysql_query("update proj_about set proj_name='$_POST[name]',about='$_POST[about]',type='$_POST[type]',status='$_POST[status]',listid=$_POST[listid],aim='$_POST[aim]',comment='$_POST[comment]',startdate='$_POST[startdate]',hide='$_POST[hide]',notes='$_POST[notes]',downloads='$_POST[downloads]',screenshots='$_POST[screenshots]',links='$_POST[links]' where proj_id=$_GET[projectid]");
	  
	  header("Location: project.php?proj=".GetProjectName($_GET[projectid]));
	}
      else if($_POST[Cancel])
	{
	  if(isset($_GET[projectid]))
	    header("Location: project.php?proj=".GetProjectName($_GET[projectid]));
	  else
	    header("Location: admin.php");
	}
      else if(isset($_GET[add]))
	{
	  DisplayPage('Add Project', 'Add a New Project', EditProject(''), '');
	}
      else
	{
	  if(isset($_GET[edit]) && isset($_GET[projectid]))
	    {
	      $QueryResult = mysql_query("select * from proj_about where proj_id=$_GET[projectid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit Project', 'Edit '.$QueryRow[proj_name], EditProject($_GET[projectid]), '');
	    }
	}
    }
    else if($_GET[projectcontribution])
    {
      if($_POST[Add])
	{
	  mysql_query("insert proj_contrib set userid=$_POST[userid],access_level=$_POST[access_level],proj_id=$_GET[projectid]");
	  
	  header("Location: project.php?project=yes&&projectid=".$_GET[projectid]."&&edit=yes");
	}
      else if($_POST[Update])
	{

	   mysql_query("update proj_contrib set userid=$_POST[userid],access_level=$_POST[access_level] where id=$_GET[contributionid]");
 		  
	  header("Location: project.php?project=yes&&projectid=".$_GET[projectid]."&&edit=yes");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from proj_contrib where id=$_GET[contributionid]");
	  
	  header("Location: project.php?project=yes&&projectid=".$_GET[projectid]."&&edit=yes");
	}
      else if($_POST[Cancel])
	{
	  header("Location: project.php?project=yes&&projectid=".$_GET[projectid]."&&edit=yes");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Project Contribution', 'Add a New Contribution to Project', EditProjectContribution($_GET[projectid], ''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[projectid]))
	    {
	      DisplayPage('Edit Project Contribution', 'Edit '.GetProjectName($_GET[projectid]).' Contribution', EditProjectContribution($_GET[projectid], $_GET[contributionid]), '');
	    }
	}
    }
  else if($_GET[log])
    {
      if($_POST[Add])
	{
	  mysql_query("insert proj_history set log_date='$_POST[date]',log_msg='$_POST[message]',proj_id=$_GET[projectid]");
	  
	  header("Location: project.php?proj=".GetProjectName($_GET[projectid]));
	}
      else if($_POST[Update])
	{
	  mysql_query("update proj_history set log_date='$_POST[date]',log_msg='$_POST[message]' where log_id=$_GET[logid]");
	  
	  header("Location: project.php?proj=".GetProjectName($_GET[projectid]));
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from proj_history where log_id=$_GET[logid]");
	  
	  header("Location: project.php?proj=".GetProjectName($_GET[projectid]));
	}
      else if($_POST[Cancel])
	{
	  header("Location: project.php?proj=".GetProjectName($_GET[projectid]));
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Log', 'Add a New Log', EditProjectLog($_GET[projectid], ''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[logid]))
	    {
	      $QueryResult = mysql_query("select * from proj_about where proj_id=$_GET[projectid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit Log', 'Edit '.$QueryRow[proj_name].' Log', EditProjectLog($_GET[projectid], $_GET[logid]), '');
	    }
	}
    }
  else
    {
      DisplayPage('Projects', '', ShowProject($_GET[proj]), '');
    }
}
else
{
  DisplayPage('Projects', '', ShowProject($_GET[proj]), '');
}
?>
