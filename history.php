<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * History Page
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

DisplayPage('History', 'Arabeyes History', ArabeyesHistory(), '');
?>
