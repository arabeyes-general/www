<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Adminstration Page
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
{
  if($_GET[email])
    {
      if($_POST[Send])
	{
	  SendMail($_POST[from], $_POST[replyto], $_POST[to], $_POST[cc], $_POST[bcc], $_POST[subject], $_POST[content]);
	  header("Location: ");
	}
      else if($_POST[Cancel])
	{
	  header("Location: ");
	}
      else
	{
	  DisplayPage('Send EMail','Compose Email', ComposeEmail(), '');
	}
    }

  else if($_GET[user])
    {//we should be able to edit cvs commit table: for ex move commits to another user, or remove them ?
      if($_POST[Add])
	{
	  //check for username if it exists, also if it is not empty ?... active should not be settable by core, and nor username ? core should be able to see all users, not only the active ones... add a n option to warn usrs ?
	  
	  //links in user/project contribution are invisible...
	  //catch the cases when the userid is empty or the user doesnot exist ?
	  //fix the worldcountires bug
	  //add a minusername length
	  //min lenght on the fname,lname ? check valid email address ? send confirmation (at least when connected from the register.php page)
	  global $PasswordMinLength;
	  
	  $QueryResult = mysql_query("select * from user where username='$_POST[username]'");
	  
	  if($_POST[fname] == '' || $_POST[lname] == '')
	    DisplayError('Please fill the first name and last name entries !');
	  else if($_POST[password1] != $_POST[password2])
	    DisplayError('The two password entries don\'t match ! No user has been added to the database.');
	  else if(strlen($_POST[password1]) < $PasswordMinLength)
	    DisplayError('The password must be at least '.$PasswordMinLength.' characters long ! No user has been added to the database.');
	  else if(mysql_num_rows($QueryResult) > 0)
	    DisplayError('The chosen username already exists. Try with "'.NewUserName($_POST[fname], $_POST[lname]).'"');
	  else
	    {
	      if($_POST[username] == '')
		$_POST[username] = NewUserName($_POST[fname], $_POST[lname]);
	      
	      mysql_query("insert user set fname='$_POST[fname]',lname='$_POST[lname]',birthdate='$_POST[birthdate]',country='$_POST[country]',residence='$_POST[residence]',phone='$_POST[phone]',email='$_POST[email]',homepage='$_POST[homepage]',pgp='$_POST[pgp]',ixexp='$_POST[ixexp]',about='$_POST[about]',howheard='$_POST[howheard]',username='$_POST[username]',active='$_POST[active]',type='$_POST[type]',pass=PASSWORD('$_POST[password1]'),cvspass='$_POST[password1]',created='".GetUTCTimeStamp()."',lastlogin='".GetUTCTimeStamp()."',modified='".GetUTCTimeStamp()."'");

	      header("Location: people.php");//point to the right user
	    }
	}
      else if($_POST[Update])
	{
	  $QueryResult = mysql_query("select * from user where username='$_POST[username]'");
	  
	  if($_POST[fname] == '' || $_POST[lname] == '')
	    DisplayError('Please fill the first name and last name entries !');
	  else if(mysql_num_rows($QueryResult) > 0 && $_POST[username] != GetUsername($_GET[userid]))
	    DisplayError('The chosen username already exists. Try with "'.NewUserName($_POST[fname], $_POST[lname]).'"');
	  else
	    {
	      if($_POST[username] == '')
		$_POST[username] = NewUserName($_POST[fname], $_POST[lname]);
	      
	      mysql_query("update user set fname='$_POST[fname]',lname='$_POST[lname]',birthdate='$_POST[birthdate]',country='$_POST[country]',residence='$_POST[residence]',phone='$_POST[phone]',email='$_POST[email]',homepage='$_POST[homepage]',pgp='$_POST[pgp]',ixexp='$_POST[ixexp]',about='$_POST[about]',howheard='$_POST[howheard]',username='$_POST[username]',active='$_POST[active]',type='$_POST[type]',modified='".GetUTCTimeStamp()."' where id=$_GET[userid]");
	      header("Location: people.php?username=".GetUsername($_GET[userid]));
	    }
	}
      else if($_POST[Delete])
	{
	  //delete from cvs
	  //delete from contrib
	  //delete from todos: change the todoid's to 'None' and remove his private todos
	  //delete picture
	  //delete wiki account, bugzilla, sourceforge
	  
	  mysql_query("delete from user where id=$_GET[userid]");
	  header("Location: people.php");
	}
      else if($_POST[Cancel])
	{
	  if(isset($_GET[userid]))
	    header("Location: people.php?username=".GetUsername($_GET[userid]));
	  else
	    header("Location: people.php");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add User', 'Add a New User', EditUser(''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[userid]))
	    {
	      $QueryResult = mysql_query("select * from user where id=$_GET[userid]");
	      $QueryRow = mysql_fetch_array($QueryResult);

	      DisplayPage('Edit User', 'Edit '.$QueryRow[fname].' '.$QueryRow[lname], EditUser($_GET[userid]), '');
	    }
	}
    }
  else if($_GET[password])
    {
      if($_POST[Update])
	{
	  global $PasswordMinLength;
	  if($_POST[password1] != $_POST[password2])
	    DisplayError('The two entries don\'t match ! The password has not been updated.');
	  else if(strlen($_POST[password1]) < $PasswordMinLength)
	    DisplayError('The password must be at least '.$PasswordMinLength.' characters long ! The password has not been updated.');
	  else
	    {
	      mysql_query("update user set pass=PASSWORD('$_POST[password1]'),cvspass='$_POST[password1]' where id=$_GET[userid]");
	      header("Location: admin.php?user=yes&&userid=".$_GET[userid]."&&edit=yes");
	    }
	}
    }
  else if($_GET[usercontribution])
    {
      if($_POST[Add])
	{
	  mysql_query("insert proj_contrib set userid=$_GET[userid],access_level=$_POST[access_level],proj_id=$_POST[projectid]"); 
	  
	  header("Location: admin.php?user=yes&&userid=".$_GET[userid]."&&edit=yes");
	}
      else if($_POST[Update])
	{
	  mysql_query("update proj_contrib set proj_id=$_POST[projectid],access_level=$_POST[access_level] where id=$_GET[contributionid]");	 
	  
	  header("Location: admin.php?user=yes&&userid=".$_GET[userid]."&&edit=yes");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from proj_contrib where id=$_GET[contributionid]");	 
	  
	  header("Location: admin.php?user=yes&&userid=".$_GET[userid]."&&edit=yes");
	}
      else if($_POST[Cancel])
	{
	  header("Location: admin.php?user=yes&&userid=".$_GET[userid]."&&edit=yes");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add User Contribution', 'Add a New Contribution to User', EditUserContribution($_GET[userid]), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[userid]))
	    {
	      DisplayPage('Edit User Contribution', 'Edit '.GetUserFLNames($_GET[userid]).' Contribution', EditUserContribution($_GET[userid], $_GET[contributionid]), '');
	    }
	}
    }
  else if($_GET[mailinglist])
    {
      if($_POST[Add])
	{
	  mysql_query("insert mailinglists set name='$_POST[name]',link='$_POST[link]',description='$_POST[description]',details='$_POST[details]',public='$_POST[public]',link_archives='$_POST[link_archives]'");
	  
	  header("Location: mailinglists.php");
	}
      else if($_POST[Update])
	{
	  mysql_query("update mailinglists set name='$_POST[name]',link='$_POST[link]',description='$_POST[description]',details='$_POST[details]',public='$_POST[public]',link_archives='$_POST[link_archives]' where id=$_GET[mailinglistid]");

	  header("Location: mailinglists.php");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from mailinglists where id=$_GET[mailinglistid]");
	  
	  header("Location: mailinglists.php");
	}
      else if($_POST[Cancel])
	{
	  header("Location: mailinglists.php");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Mailing List', 'Add a New Mailing List', EditMailingList(''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[mailinglistid]))
	    {
	      $QueryResult = mysql_query("select * from mailinglists where id=$_GET[mailinglistid]");
	      $QueryRow = mysql_fetch_array($QueryResult);

	      DisplayPage('Edit Mailing List', 'Edit '.$QueryRow[name], EditMailingList($_GET[mailinglistid]), '');
	    }
	}
    }
  else if($_GET[relatedsite])
    {
      if($_POST[Add])
	{
	  mysql_query("insert relatedsites set name='$_POST[name]',link='$_POST[link]'");
	  
	  header("Location: ");
	}
      else if($_POST[Update])
	{
	  mysql_query("update relatedsites set name='$_POST[name]',link='$_POST[link]' where id=$_GET[relatedsiteid]");
	  
	  header("Location: ");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from relatedsites where id=$_GET[relatedsiteid]");
	  
	  header("Location: ");
	}
      else if($_POST[Cancel])
	{
	  header("Location: ");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Related Site', 'Add a Related Site', EditRelatedSite(''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[relatedsiteid]))
	    {
	      $QueryResult = mysql_query("select * from relatedsites where id=$_GET[relatedsiteid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit Related Site', 'Edit '.$QueryRow[name], EditRelatedSite($_GET[relatedsiteid]), '');
	    }
	}
    }
  else if($_GET[document])
    {
      if($_POST[Add])
	{
	  mysql_query("insert docs set name='$_POST[name]',audience='$_POST[audience]',description='$_POST[description]'");
	  
	  header("Location: docs.php");
	}
      else if($_POST[Update])
	{
	  mysql_query("update docs set name='$_POST[name]',audience='$_POST[audience]',description='$_POST[description]' where id=$_GET[documentid]");
	  
	  header("Location: docs.php");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from docs where id=$_GET[documentid]");
	  mysql_query("delete from docs_versions where docid=$_GET[documentid]");
	  
	  header("Location: docs.php");
	}
      else if($_POST[Cancel])
	{
	  header("Location: docs.php");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Document', 'Add a New Document', EditDocument(''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[documentid]))
	    {
	      $QueryResult = mysql_query("select * from docs where id=$_GET[documentid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit Document', 'Edit '.$QueryRow[name], EditDocument($_GET[documentid]), '');
	    }
	}
    }
  else if($_GET[version])
    {
      if($_POST[Add])
	{
	  mysql_query("insert docs_versions set link='$_POST[link]',format='$_POST[format]',lang='$_POST[lang]',docid=$_GET[documentid]");
	  
	  header("Location: admin.php?document=yes&&documentid=".$_GET[documentid]."&&edit=yes");
	}
      else if($_POST[Update])
	{
	  mysql_query("update docs_versions set link='$_POST[link]',format='$_POST[format]',lang='$_POST[lang]' where id=$_GET[versionid]");
	  
	  header("Location: admin.php?document=yes&&documentid=".$_GET[documentid]."&&edit=yes");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from docs_versions where id=$_GET[versionid]");
	  
	  header("Location: admin.php?document=yes&&documentid=".$_GET[documentid]."&&edit=yes");
	}
      else if($_POST[Cancel])
	{
	  header("Location: admin.php?document=yes&&documentid=".$_GET[documentid]."&&edit=yes");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Version', 'Add a New Version', EditDocumentVersion($_GET[documentid]), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[documentid]))
	    {
	      $QueryResult = mysql_query("select * from docs where id=$_GET[documentid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit Version', 'Edit '.$QueryRow[name].' Version', EditDocumentVersion($_GET[documentid], $_GET[versionid]), '');
	    }
	}
    }
  else if($_GET[press])
    {
      if($_POST[Add])
	{
	  mysql_query("insert press set date='$_POST[date]',name='$_POST[name]',link='$_POST[link]',source='$_POST[source]',sourcelink='$_POST[sourcelink]',description='$_POST[description]'");
	  header("Location: about.php");
	}
      else if($_POST[Update])
	{
	  mysql_query("update press set date='$_POST[date]',name='$_POST[name]',link='$_POST[link]',source='$_POST[source]',sourcelink='$_POST[sourcelink]',description='$_POST[description]' where id=$_GET[pressid]");
	  header("Location: about.php");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from press where id=$_GET[pressid]");
	  
	  header("Location: about.php");
	}
      else if($_POST[Cancel])
	{
	  header("Location: about.php");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Press Release', 'Add a New Press Release', EditPress(''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[pressid]))
	    {
	      $QueryResult = mysql_query("select * from press where id=$_GET[pressid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit Press Release', 'Edit '.$QueryRow[name], EditPress($_GET[pressid]), '');
	    }
	}
    }
  else if($_GET[resource])
    {
      if($_POST[Add])
	{
	  mysql_query("insert resources set type='$_POST[type]',name='$_POST[name]',link='$_POST[link]',description='$_POST[description]'");
	  header("Location: resources.php");
	}
      else if($_POST[Update])
	{
	  mysql_query("update resources set type='$_POST[type]',name='$_POST[name]',link='$_POST[link]',description='$_POST[description]' where id=$_GET[resourceid]");
	  header("Location: resources.php");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from resources where id=$_GET[resourceid]");
	  
	  header("Location: resources.php");
	}
      else if($_POST[Cancel])
	{
	  header("Location: resources.php");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add Resource', 'Add a New Resource', EditResource(''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[resourceid]))
	    {
	      $QueryResult = mysql_query("select * from resources where id=$_GET[resourceid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit Resource', 'Edit '.$QueryRow[name], EditResource($_GET[resourceid]), '');
	    }
	}
    }
  else if($_GET[news])
    {
      if($_POST[Add])
	{
	  mysql_query("insert news set date='$_POST[date]',article='$_POST[article]',title='$_POST[title]'");
	  header("Location: news.php");
	}
      else if($_POST[Update])
	{
	  mysql_query("update news set date='$_POST[date]',article='$_POST[article]',title='$_POST[title]' where id=$_GET[newsid]");
	  header("Location: news.php");
	}
      else if($_POST[Delete])
	{
	  mysql_query("delete from news where id=$_GET[newsid]");
	  
	  header("Location: news.php");
	}
      else if($_POST[Cancel])
	{
	  header("Location: news.php");
	}
      else
	{
	  if(isset($_GET[add]))
	    {
	      DisplayPage('Add News', 'Add a News', EditNews(''), '');
	    }
	  else if(isset($_GET[edit]) && isset($_GET[newsid]))
	    {
	      $QueryResult = mysql_query("select * from news where id=$_GET[newsid]");
	      $QueryRow = mysql_fetch_array($QueryResult);
	      
	      DisplayPage('Edit News', 'Edit '.$QueryRow[title], EditNews($QueryRow[id]), '');
	    }
	}
    }
  else if($_GET[elections])
  {
    $sql = "select * from elections where id = '".$_GET[electionid]."'";
    $QueryResult = mysql_query($sql);
    $QueryRow = mysql_fetch_array($QueryResult);
    if ($_POST[submit_election]=="Add Election")
    {
      $sql="insert into elections (title, description, max_vote_num,
            nominate_start_date, nominate_end_date, vote_start_date,
            vote_end_date) values ('".$_POST[election_title]."','"
            .$_POST[election_description]."','".$_POST[election_max_vote_num]
            ."','".$_POST[election_nominate_begin_date]." "
            .$_POST[election_nominate_begin_time]."','"
            .$_POST[election_nominate_end_date]." "
            .$_POST[election_nominate_end_time]."','"
            .$_POST[election_vote_begin_date]." "
            .$_POST[election_vote_begin_time]."','"
            .$_POST[election_vote_end_date]." "
            .$_POST[election_vote_end_time]."');";
      $QueryResult = mysql_query($sql);
      $buffer=showelections(0);
      DisplayPage('Manage Elections', 'Manage Elections - Election added', $buffer, '');
    }
    elseif ($_POST[submit_election]=="Change Election")
    {
      if ($_POST[election_id])
      {
	$sql="update elections set title='".$_POST[election_title]
              ."' ,description='".$_POST[election_description]
              ."' ,max_vote_num='".$_POST[election_max_vote_num]
              ."' ,nominate_start_date='".$_POST[election_nominate_begin_date]
              ." ".$_POST[election_nominate_begin_time]
              ."' ,nominate_end_date='".$_POST[election_nominate_end_date]
              ." ".$_POST[election_nominate_end_time]."' ,vote_start_date='"
              .$_POST[election_vote_begin_date]." "
              .$_POST[election_vote_begin_time]."' ,vote_end_date='"
              .$_POST[election_vote_end_date]." "
              .$_POST[election_vote_end_time]."' ,cvs_commit_c='"
              .$_POST[election_cvs_commit_c_date]." "
              .$_POST[election_cvs_commit_c_time]."', cvs_commit_v='"
              .$_POST[election_cvs_commit_v_date]." "
              .$_POST[election_cvs_commit_v_time]."' where id="
              .$_POST[election_id].";";
	$QueryResult = mysql_query($sql);
      }
      $buffer=showelections(0)."<br><br>".showelectionform(0, 1);
      DisplayPage('Manage Elections', 'Manage Elections', $buffer, '');
    }
    elseif ($_GET[electionid])
    {
      $buffer=showelectionform($_GET[electionid], 1);
      $buffer.="<br><br>";
      $buffer.=shownominees($_GET[electionid], 1);
#      DisplayPage('Manage Elections', 'Manage Election #'.$_GET[electionid], $buffer, '');
      DisplayPage('Manage Elections', 'Manage Election '."'".$QueryRow[title]."'", $buffer, '');
    }
    else
    {
      # List available elections (old ones too)
      $buffer=showelections(0)."<br><br>".showelectionform(0, 1);
      DisplayPage('Manage Elections', 'Manage Elections', $buffer, '');
    }
  }
  else
    {
      DisplayPage('Administration', 'Administer the Website', AdminWebsite(), '');
    }
}
else
{
  DisplayError('You are not correctly logged in.');
}
