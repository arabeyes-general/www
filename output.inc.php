<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Output Library
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

function ComposeEmail()
{
  $Buffer = '
	<form method="post" action="admin.php?email=yes">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>From</td><td><input name="from" value="'.GetUserFLNames($_SESSION[user_id]).' <'.$_SESSION[username].'@arabeyes.org>" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Reply To</td><td><input name="replyto" value="" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>To</td><td><input name="to" value="" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>CC</td><td><input name="cc" value="" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>BCC</td><td><input name="bcc" value="Arabeyes Archives <arch@arabeyes.org>" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Subject</td><td><input name="subject" value="" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Content</td><td><textarea name="content" rows=10 cols=60></textarea></td></tr>
	</table>
	<input type=submit name="Send" value="Send" align=center>
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>';

  return $Buffer;
}

function Index()
{
  $Buffer = About().'
	<hr>';

  $Buffer .= '
	<table cellspacing=0 cellpadding=0 border=0 width="100%">
	<tr valign="top">';


  $Buffer .= '<td width="25%">'.ShowProjectUpdates().'</td>';

  $Buffer .= '<td>&nbsp;&nbsp;</td>';

  $Buffer .= '<td width="75%">'.ShowNews(5).'</td>';




  $Buffer .= '
	</tr>
	</table>';

  return DisplaySection(gettext("About the Arabeyes Project"), $Buffer);
}

function About()
{
  $Buffer = '
	Arabeyes is a Meta project that is aimed at fully supporting the Arabic language in the Unix/Linux environment. It is designed to be a central location to standardize the Arabization process. Arabeyes relies on voluntary contributions by computer professionals and enthusiasts all over the world.
	<br><br>
Arabeyes has the following <a href="http://wiki.arabeyes.org/OpenPositions">
positions</a> available.
        <br><br>
	<i><a href="about.php">More...</a></i>';

  return $Buffer;
}

function FullAbout()
{
  $Buffer = DisplaySection('About Arabeyes', '
	  <p>
	  Arabeyes is a Meta project that is aimed at fully supporting the Arabic
	  language in the Unix/Linux environment. It is designed to be a central
	  location to standardize the Arabization process. Arabeyes relies on
	  voluntary contributions by computer professionals and enthusiasts
	  all over the world.
	  </p>
	  <p>
	  Unlike other Arabized products, Arabeyes is <a href="
	  http://www.opensource.org/docs/definition_plain.html">Open Source</a>,
	  completely <b>FREE</b> and abides by the ideals of the open source 
	  communities.
	  Arabeyes aims for all active participants and volunteers
	  to partake in the development process and to speak their mind on
	  this professional effort.
	  </p>
	  <p>
	  The idea of Arabizing Unix is not a new one. However, most of the attempts
	  were done by Arab computer science students studying outside of the Arab
	  world. Once those individuals graduated, their projects were abandoned. 
	  Resulting in very sparse code and in every subsequent attempt in having to
	re-invent the wheel. Arabeyes aims to eliminate this redundancy and lack
	of information by not depending on any one person or group - <i>Arabeyes
	is aimed to succeed irrespective of the interest or availability of the
	parties involved</i>.
	</p>
	<p>
	Arabeyes plans to address every aspect of making the use of Arabic on
	linux/unix common and without headaches/hurdles.  Arabeyes\' goal is not
	to create new applications (if those applications exist), but to
	incorporate modifications/additions to existing common everyday-use
	applications.  Arabeyes will initially engage in the following projects,
  </p>
	<ul>
	<li>Address the various font/keyboard-mapping issues
	<li>Create the ability to write plain-text Arabic easily
	<li>Create the ability to spell-check your Arabic text documents
	<li>Create the ability to send/receive mail through an Arabic interface
	<li>Localize (or translate) Common Graphical-User-Interface (GUI)
	<li>Create the ability to take Arabic text files and create various enriched documents (TeX, word, etc)
	</ul>
	The Arabeyes project acknowledges the importance of documentation.
	It is the only way to ensure that Arabization will proceed forward.
	Documentation creates a concrete foundation which others can refer
	to and follow in the future.');

  $Buffer .= DisplaySection('Introductory Material', Docs('a'));

  // $Buffer .= DisplaySection('In the Press', Press());

  return $Buffer;
}

function InPress()
{
  $Buffer = DisplaySection('In the Press', Press());
  return $Buffer;
}

function Press()
{
  $Buffer = '';

  $QueryResult = mysql_query("select *,DATE_FORMAT(date, '%b %d, %Y') as pressdate from press order by date desc");

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	  $Buffer .= '
		<p>
		<table border=0 cellpadding=0 cellspacing=0 width="100%">
		<tr>
		<td align="left"><a class="pressrelease" href="'.$QueryRow[link].'">['.$QueryRow[pressdate].'] - '.$QueryRow[name].'</a></td>
		<td align="right">[<a href="admin.php?press=yes&&edit=yes&&pressid='.$QueryRow[id].'">Edit</a>]</td>
		</tr>
		</table>';
	else
	  $Buffer .= '
		<p>
		<a class="pressrelease" href="'.$QueryRow[link].'">'.$QueryRow[pressdate].': '.$QueryRow[name].'</a><br>';

	$Buffer .= '
	  '.$QueryRow[description].'<br>
	  In <a href="'.$QueryRow[sourcelink].'">'.$QueryRow[source].'</a>.
	  </p>';
  }

  return $Buffer;
}

function ShowProjectCategory($Title, $Type)
{
  $Buffer = '';
  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
  {
	$QueryResult = mysql_query("select * from proj_about where type='$Type' order by proj_name");
  }
  else
  {
	$QueryResult = mysql_query("select * from proj_about where type='$Type' and hide='N' order by proj_name");
  }


  $Buffer .= '
	<table border=0 cellpadding=0 cellspacing=0 width="100%">';
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	if($i != 0)
	{
	  $Buffer .= '<tr><td colspan=2><br></td></tr>';
	}

	$Buffer .= '
	  <tr>
	  <td valign="top">
	  <a href="project.php?proj='.$QueryRow[proj_name].'">'.$QueryRow[proj_name].' - details</a>
	  <ul>
	  <li><b>Aim:</b> '.$QueryRow[aim].'<br>
	  '.$QueryRow[comment].'
	  </ul>
	  </td>';

	$Image = 'images/projects/logos/'.strtolower($QueryRow[proj_name]).'.png';

	if(is_file($Image))
	{
	  $Buffer .= '<td valign="top" align="right"><a href="project.php?proj='.$QueryRow[proj_name].'"><img border=0 src="'.$Image.'" alt="'.$QueryRow[proj_name].'\'s Logo"></a></td>';
	}

	$Buffer .= '</tr>';
  }

  $Buffer .= '</table>';

  return DisplaySection($Title, $Buffer);
}

function ShowProject($ProjectName)
{
  if(empty($ProjectName))
  {
	$Buffer = ShowProjectCategory('Translation/Localization Projects', 't');
	$Buffer .= ShowProjectCategory('Software Development Projects', 'd');
	$Buffer .= ShowProjectCategory('External Projects/Patches', 'e');
	$Buffer .= ShowProjectCategory('Fonts/Artistic Projects', 'f');

	return $Buffer;
  }

  $Buffer = '';

  $QueryResult = mysql_query("select *,DATE_FORMAT(startdate, '%b %d, %Y') as fmtdate from proj_about where proj_name='$ProjectName'");
  if(mysql_num_rows($QueryResult) != 0)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	if($QueryRow[type] == 't')
	{
	  $CVSLink = "http://cvs.arabeyes.org/viewcvs/translate/".strtolower($ProjectName);
	  $SnapshotLink = "http://cvs.arabeyes.org/viewcvs/translate/".strtolower($ProjectName)."/".strtolower($ProjectName)."_cvs_snapshot_".GetUTCTimeStamp().".tar.gz?tarball=1";
	  $Admin = 'Coordinator';
	}
	else if($QueryRow[type] == 'd')
	{
	  $CVSLink = "http://cvs.arabeyes.org/viewcvs/projects/".strtolower($ProjectName);
	  $SnapshotLink = "http://cvs.arabeyes.org/viewcvs/projects/".strtolower($ProjectName)."/".strtolower($ProjectName)."_cvs_snapshot_".GetUTCTimeStamp().".tar.gz?tarball=1";
	  $Admin = 'Maintainer';
	}
	else if($QueryRow[type] == 'e')
	{
	  $CVSLink = "http://cvs.arabeyes.org/viewcvs/projects/external/".strtolower($ProjectName);
	  $SnapshotLink = "http://cvs.arabeyes.org/viewcvs/projects/external/".strtolower($ProjectName)."/".strtolower($ProjectName)."_cvs_snapshot_".GetUTCTimeStamp().".tar.gz?tarball=1";
	  $Admin = 'Maintainer';
	}
	else if($QueryRow[type] == 'f')
	{
	  $CVSLink = "http://cvs.arabeyes.org/viewcvs/art/".strtolower($ProjectName);
	  $SnapshotLink = "http://cvs.arabeyes.org/viewcvs/art/".strtolower($ProjectName)."/".strtolower($ProjectName)."_cvs_snapshot_".GetUTCTimeStamp().".tar.gz?tarball=1";
	  $Admin = 'Maintainer';
	}


	$TmpBuffer = '
	  <table border=0 cellpadding=0 cellspacing=0 width="100%">
	  <tr>
	  <td td align="left" valign="top">
	  <table border=0 cellpadding=2 cellspacing=2 width="100%">
	  <tr><th align="left" valign="top">About:</th><td>'.$QueryRow[about].'</td></tr>
	  <tr><th align="left">Start Date:</eh><td>'.$QueryRow[fmtdate].'</td></tr>
	  <tr><th align="left">Status:</th><td>'.$QueryRow[status].'</td></tr>
	  <tr><th align="left">'.$Admin.':</th><td>'.GetProjectUsers($QueryRow[proj_id], 2, true).'</td></tr>
	  <tr><th align="left" valign="top">Contributors:</th><td>'.GetProjectUsers($QueryRow[proj_id], 1).'</td></tr>
	  <tr><th align="left">Mailing List:</th><td>'.GetMailingListNameLink($QueryRow[listid]).'</td></tr>
	  </table>
	  <br>
	  <a href="'.$CVSLink.'">Project\'s CVS</a><br>
	  <br>
	  <a href="'.$SnapshotLink.'">CVS Snapshot</a>
	  </td>';

	$Image = 'images/projects/logos/'.strtolower($QueryRow[proj_name]).'.png';

	if(is_file($Image))
	  $TmpBuffer .= '<td>&nbsp;&nbsp;</td><td align="right" valign="top"><img src="'.$Image.'" border=0 alt="'.$QueryRow[proj_name].'\'s Logo"></td>';

	$TmpBuffer .= '
	  </tr>
	  </table>';


	if($_SESSION[type] == 'c' || GetUserAccessLevel($_SESSION[user_id], $QueryRow[proj_id]) > 1)
	  $Buffer .= DisplaySection($ProjectName.' [<a href="project.php?projectid='.$QueryRow[proj_id].'&&project=yes&&edit=yes">Edit</a>]', $TmpBuffer).'<br>';
	else
	  $Buffer .= DisplaySection($ProjectName, $TmpBuffer).'<br>';


	$TmpBuffer = '<b>Notes</b><br>'.$QueryRow[notes].'<br>';
	$TmpBuffer .= '<b>Links</b><br>'.$QueryRow[links].'<br>';;
	$TmpBuffer .= '<b>Screenshots</b><br>'.$QueryRow[screenshots].'<br>';
	$TmpBuffer .= '<b>Downloads</b><br>'.$QueryRow[downloads].'<br><br>';
	$Buffer .= DisplaySection('Details', $TmpBuffer);  

	$Buffer .= DisplaySection('Todos', ShowPublicProjectTodos($QueryRow[proj_id]).'<br>');

	$TmpBuffer = '';
	$QueryResult3 = mysql_query("select *,DATE_FORMAT(log_date, '%b %d, %Y') as fmtdate from proj_history where proj_id=".$QueryRow[proj_id]." order by log_date desc");
	for($i = 0; $i < mysql_num_rows($QueryResult3); $i++)
	{
	  $QueryRow3 = mysql_fetch_array($QueryResult3);

	  if($i != 0)
		$TmpBuffer .= '<br>';

	  if(GetUserAccessLevel($_SESSION[user_id], $QueryRow[proj_id]) > 1 || $_SESSION[type] == 'c')
		$TmpBuffer .= '<hr width=20% align="left"><b>('.$QueryRow3[fmtdate].')</b> [<a href="project.php?projectid='.$QueryRow[proj_id].'&&log=yes&&edit=yes&&logid='.$QueryRow3[log_id].'">Edit</a>]<hr width=20% align="left">'.$QueryRow3[log_msg].'<br>';
	  else
		$TmpBuffer .= '<hr width=20% align="left"><b>('.$QueryRow3[fmtdate].')</b><hr width=20% align="left">'.$QueryRow3[log_msg].'<br>';
	}

	if(GetUserAccessLevel($_SESSION[user_id], $QueryRow[proj_id]) > 1 || $_SESSION[type] == 'c')
	  $Buffer .= DisplaySection('History Log [<a href="project.php?projectid='.$QueryRow[proj_id].'&&log=yes&&add=yes">Add a log</a>]', $TmpBuffer);
	else
	  $Buffer .= DisplaySection('History Log', $TmpBuffer);
  }
  else
  {
	$Buffer="Project " . $ProjectName . " does not exist.<BR>" .
	  "Please return to <a href='project.php'>Project</a> home page " .
	  "and select another";

  }

  return $Buffer;
}

function GetMailingListNameLink($MailingListID)
{
  $QueryResult = mysql_query("select * from mailinglists where id=$MailingListID");

  $QueryRow = mysql_fetch_array($QueryResult);

  $Buffer = '<a href="'.$QueryRow[link].'">'.$QueryRow[name].'</a>';

  return $Buffer;
}

function AllResources()
{
  $Buffer = DisplaySection('Codepages and Shaping', Resources('cs').'<br>');
  $Buffer .= DisplaySection('Free Fonts', Resources('ff').'<br>');
  $Buffer .= DisplaySection('Font Manipulation Utilities', Resources('fmu'));

  return $Buffer;
}

function Resources($Type)
{
  $QueryResult = mysql_query("select * from resources where type='$Type' order by name");

  $Buffer = '<table border=0 cellpadding=0 cellspacing=0 width="100%">';

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	  $Buffer .= '
		<tr valign="top"><td align="left"><a href="'.$QueryRow[link].'">'.$QueryRow[name].'</a></td><td>&nbsp;</td><td align="left">'.$QueryRow[description].'</td><td align="right">[<a href="admin.php?resource=yes&&edit=yes&&resourceid='.$QueryRow[id].'">Edit</a>]</td></tr>';
	else
	  $Buffer .= '
		<tr valign="top"><td align="left"><a href="'.$QueryRow[link].'">'.$QueryRow[name].'</a></td><td>&nbsp;</td><td align="left">'.$QueryRow[description].'</td></tr>';
  }

  $Buffer .= '</table>';

  return $Buffer;
}

function AllDocs()
{
  $Buffer = DisplaySection('User Documentation', Docs('u'));
  $Buffer = $Buffer.'<br>'.DisplaySection('Developer Documentation', Docs('d'));
  $Buffer = $Buffer.'<br>'.DisplaySection('Technical Writer Documentation', Docs('t'));
  $Buffer = $Buffer.'<br>'.DisplaySection('Artist Documentation', Docs('g'));

  return $Buffer;
}

function Docs($Audience)
{
  $Buffer = '
	<table align="center" border=0 cellpadding=0 cellspacing=0 width="80%">';

  $QueryResult = mysql_query("select * from docs where audience='$Audience' order by name");

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$NoSep = '';
	if($i == mysql_num_rows($QueryResult)-1)
	  $NoSep = '_final';


	$QueryRow = mysql_fetch_array($QueryResult);

	$Buffer .= '
	  <tr valign="top">
	  <td class="docbold'.$NoSep.'" width="20%"><br>'.$QueryRow[name].'<br></td>
	  <td class="doc'.$NoSep.'"><br>'.$QueryRow[description].'<br><br></td>';

	//These should be read from the Db: with the possibility to add a new entry
	$Formats = array('HTML', 'PDF');

	for($j = 0; $j < count($Formats); $j++)
	{
	  $QueryResult1 = mysql_query("select * from docs_versions where docid=$QueryRow[id] and format='$Formats[$j]'");
	  $QueryRow1 = mysql_fetch_array($QueryResult1);
	  if(!$QueryRow1)
	  {
		$Buffer .= '
		  <td class="doc'.$NoSep.'" width="5%"><br>&nbsp;<br></td>
		  <td class="doc'.$NoSep.'" width="5%"><br>&nbsp;<br></td>
		  <td class="doc'.$NoSep.'" width="5%"><br>&nbsp;<br></td>
		  <td class="doc'.$NoSep.'" width="5%"><br>&nbsp;<br></td>';
		continue;}

		$Buffer .= '
		  <td class="docformat'.$NoSep.'" width="5%">
		  <br>&nbsp;<span class="'.strtolower($Formats[$j]).'">&nbsp;'.$Formats[$j].'&nbsp;</span><br>
		  </td>';

		//These should be read from the Db: with the possibility to add a new entry
		$Langs = array('EN', 'AR', 'FR');

		for($k = 0; $k < count($Langs); $k++)
		{
		  $QueryResult1 = mysql_query("select * from docs_versions where docid=$QueryRow[id] and format='$Formats[$j]' and lang='$Langs[$k]'");

		  $QueryRow1 = mysql_fetch_array($QueryResult1);

		  $Buffer .= '
			<td class="doc'.$NoSep.'" width="5%"><br>&nbsp;<a class="imlang" href="'.$QueryRow1[link].'">'.$QueryRow1[lang].'</a><br></td>';
		}
	}

	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	  $Buffer .= '
		<td class="doc'.$NoSep.'" align="right"><br>[<a href="admin.php?document=yes&&documentid='.$QueryRow[id].'&&edit=yes">Edit</a>]<br></td>';

	$Buffer .= '
	  </tr>';
  }

  $Buffer .= '
	</table>';

  return $Buffer;
}

function ThankYou()
{
  $Buffer = '
        Arabeyes would like to thank various individuals, companies and
        organizations which have directly or indirectly assisted in the
        shaping of Arabeyes\' image and/or assisted in assuring Arabeyes\'
        longterm survivability, growth and prosperity.
        <br><br>
        Arabeyes\' managers, volunteers and users owe the donors listed
        an enormous level of gratitude.  Thank you all for believing in
        Arabeyes, its mandate and its cause.
	<br><br>

	<table align="center" border=0 cellpadding=0 cellspacing=0 width="90%">
	<tr valign="top">
	<td class="doc" width="80%">
        <br><i>Oct 06, 2003:</i> <b>Arabeyes Attains Free Hosting</b><br>
        <a href="http://www.binaryenvironments.com">Binary Environments</a>,
        Vancouver\'s premier ISP, via Mr. John Batyka, its director of sales,
	has offered to host Arabeyes at no-cost until further notice.
      	<br><br>
	</td></tr>

	<tr valign="top">
	<td class="doc" width="80%">
        <br><i>Aug 08, 2003:</i> <b>Arabeyes Receives a Donated Server</b><br>
	<a href="http://www.zajilcorp.com">Zajil Telecom\'s</a> Mr. Khalifa
	Al-Soulah (at the behest of Mr. Ahmad Al-Rasheedan\'s asking) donated
        an
	<a href="http://www.ibm.com/servers/eserver/support/xseries/">
	IBM x305</a> (a 1u form-factor) server 
        <a href="http://www.arabeyes.org/download/documents/about/server.pdf">
        [datasheet]</a> to be used at Arabeyes\' discretion.
      	<br><br>
	</td></tr>

	<tr valign="top">
	<td class="doc" width="80%">
        <br><i>Nov 12, 2002:</i> <b>Arabeyes Gets a New Logo</b><br>
	<a href="http://perso.wanadoo.fr/hassan.massoudy">Mr. Hassan
	Massoudy</a> a world-renowned calligraphy artist donates one of his
	art pieces to be used as Arabeyes\' logo.
      	<br><br>
	</td></tr>

	</table>
        ';

  $Buffer = DisplaySection('Acknowledgments', $Buffer);

  return $Buffer;
}

function ACE()
{
  $Buffer = '
	The following interview section is noted not only to highlight the
	contributions various enthusiasts have brought forth but to show their
	collective <i>vision, thoughts, drive</i> and <i>aspirations</i>.
	There are many out there that work tirelessly to fulfill the goal
	of bringing Arabic to Linux/Unix, here we try to shed some light
	on that passion so that you and we can better know and understand
	them.
	<br><br>
	<table align="center" border=0 cellpadding=0 cellspacing=0 width="90%">
	<tr valign="top">
	  <td class="doc" width="80%"><br>Q4, 2004: <b>Arafat Medini</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <a href="people.php?username=nadim">Nadim Shaikli</a>.<br><br></td>
	  <td class="docformat" width="5%"><br>&nbsp;&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=medini&&lang=en">EN</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=medini&&lang=ar">AR</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<br></td>
	</tr>
	<tr valign="top">
	  <td class="doc" width="80%"><br>Q3, 2004: <b>Christian Perrier</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <a href="people.php?username=nadim">Nadim Shaikli</a>/
	  <a href="people.php?username=alarfaj">Abdulaziz Al-Arfaj</a>.<br><br></td>
	  <td class="docformat" width="5%"><br>&nbsp;&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=perrier&&lang=en">EN</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=perrier&&lang=ar">AR</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<br></td>
	</tr>
	<tr valign="top">
	  <td class="doc" width="80%"><br>Q2, 2004: <b>Ossama Khayat</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <a href="people.php?username=nadim">Nadim Shaikli</a>.<br><br></td>
	  <td class="docformat" width="5%"><br>&nbsp;&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=khayat&&lang=en">EN</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=khayat&&lang=ar">AR</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<br></td>
	</tr>
	<tr valign="top">
	  <td class="doc" width="80%"><br>Q1, 2004: <b>Ahmad Al-Rasheedan</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <a href="people.php?username=nadim">Nadim Shaikli</a>.<br><br></td>
	  <td class="docformat" width="5%"><br>&nbsp;&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=alrasheedan&&lang=en">EN</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=alrasheedan&&lang=ar">AR</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<br></td>
	</tr>
	<tr valign="top">
	  <td class="doc" width="80%"><br>Q4, 2003: <b>Roozbeh Pournader</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <!--
	  <a href="people.php?username=XX">Sabrine Oueja</a>.<br><br></td>
	  -->
	  Sabrine Oueja.<br><br></td>
	  <td class="docformat" width="5%"><br>&nbsp;&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=pournader&&lang=en">EN</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=pournader&&lang=ar">AR</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<br></td>
	</tr>
	  <tr valign="top">
	  <td class="doc" width="80%"><br>Q3, 2003: <b>Mohammed Sameer</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <!--
	  <a href="people.php?username=XX">Sabrine Oueja</a>.<br><br></td>
	  -->
	  Sabrine Oueja.<br><br></td>
	  <td class="docformat" width="5%"><br>&nbsp;&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=sameer&&lang=en">EN</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=sameer&&lang=ar">AR</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<br></td>
	</tr>
	<tr valign="top">
	  <td class="doc" width="80%"><br>Q1, 2003: <b>AbdulRahman Aljadhai</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <a href="people.php?username=eldesoky">Mohamed Eldesoky</a>.<br><br></td>
	  <td class="docformat" width="5%"><br>&nbsp;&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=aljadhai&&lang=en">EN</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=aljadhai&&lang=ar">AR</a><br></td>
	  <td class="doc" width="5%"><br>&nbsp;<br></td>
	</tr>
	<tr valign="top">
	  <td class="doc_final" width="80%"><br>Q3, 2002: <b>Nadim Shaikli</b><br>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interview conducted by
	  <a href="people.php?username=eldesoky">Mohamed Eldesoky</a>.<br><br></td>
	  <td class="docformat_final" width="5%"><br>&nbsp;<span class="html">&nbsp;HTML&nbsp;</span><br></td>
	  <td class="doc_final" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=shaikli&&lang=en">EN</a><br></td>
	  <td class="doc_final" width="5%"><br>&nbsp;<a class="imlang" href="ace.php?ace=shaikli&&lang=ar">AR</a><br></td>
	  <td class="doc_final" width="5%"><br>&nbsp;<br></td>
	</tr>
	</table>
	</ul>
	';

  return $Buffer;
}


function ArabeyesHistory()
{
  $Buffer = '
	This is the page to keep track of the Arabeyes history.<br><br>
 
	A few of us were disappointed with the lack of Arabic support on the
	Linux/UNIX platforms and decided to do something about it ourselves.<br><br>
 
	The Arabeyes project used to be under the name \'Aunyx\', which was started
	on January 2001. Our main recruiting efforts targeted the underground and
	the hacker world, under the assumption that they would be more familiar
	with the Linux/UNIX systems. That search did not yield the expected results.
	Plus the mandate and steps, during our Aunyx days, weren\'t very clear and
	didn\'t seem to lend themselves to a concrete goal.<br><br>

	During the Aunyx days, most of the work concentrated on documentation and
	how to go about it. Several papers/proposals were written on the subject.
	Then, in the summer of 2001, with the addition of a few more members, we
	changed our name to Arabeyes in an attempt to make it catchier, easier
	to remember and to spell :-). We also rethought and revamped throughly our
	approach and how to attain our goals. Most of those
	exchanges and emails are still on our <a href="http://lists.arabeyes.org/archives/general/">\'general\' mailing-list archives</a>.<br><br>
 
	With the change over to Arabeyes, we adapted a charter and looked at past
	projects\' failures and successes, and decided that this effort is larger
	than any and all of us, and that the project is paramount and should survive
	irrespective of the founders. We decided to adopt an Open Source license
	to further enlighten the Arabic World of its existence and its importance.
	We wanted to put something together that was a grass-roots effort in which
	decisions are made by consensus not based on politics or money, but on
	technical merit with the notion that the "best idea wins" always.<br><br>

	Because our philosophy in Arabeyes is that this is a grass-roots effort, the
	question of who started what takes a backseat. <a href="people.php?username=elzubeir">Mohammed Elzubeir</a> was the one
	who started the Aunyx project, which was soon joined by <a href="people.php?username=chahine">Chahine Hamila</a> (author
		of <a href="project.php?proj=Akka">Akka</a>). When <a href="people.php?username=nadim">Nadim Shaikli</a> came across Aunyx back in July 2001, he put forth
	several suggestions and the project was re-ignited. The name was changed to
	Arabeyes and it quickly evolved to what it is today. If you want to see the
	way things happened, it is all archived in the <a href="http://lists.arabeyes.org/archives/general/">\'general\' list archives</a> [start with the <a href="http://lists.arabeyes.org/archives/general/2001/July/index.html">July 2001 archives</a>].';

  $Buffer .= DisplaySection('Old Logos', '
	  <img src="images/arabeyes/logos/old/logo1.gif" align="middle" alt="Old Logo 1"></img> Logo 1<br>
	  <img src="images/arabeyes/logos/old/logo2.png" align="middle" alt="Old Logo 2"></img> Logo 2<br>
	  <img src="images/arabeyes/logos/old/logo3.png" align="middle" alt="Old Logo 3"></img> Logo 3<br>');

  $Buffer .= DisplaySection('Old Banners', '
          Banner 1<br><img src="images/arabeyes/banners/old/banner1.jpg" align="middle" alt="Old Banner 1"></img><br><br>
          Banner 2<br><img src="images/arabeyes/banners/old/banner2.jpg" align="middle" alt="Old Banner 2"></img><br><br>
          Banner 3<br><img src="images/arabeyes/banners/old/banner3.png" align="middle" alt="Old Banner 3"></img><br><br>');
 
  return $Buffer;
}

function ShowAllUsers($status)
{
  $Buffer = '
	Some of the individuals who work hard to make Arabeyes a successful project are listed below ordered by the highest number of CVS commits. Please note that a high number of commits does not necessarily reflect the amount of work put into each single commit, but chances are if someone has a high number of commits, they are working very hard !<br><br>
	<table align="center" width="95%" cellspacing=2 cellpadding=2 border=0>
	<tr valign="middle">
	<th align="left">Username</th>
	<th align="left">First Name</th>
	<th align="left">Last Name</th>
	<th align="left">Latest Commit</th>
	<th align="right">Number of Commits</th>
	</tr>';
  if ($status == 'all') {
      $QueryResult = mysql_query("select *,count(*) as ncommits from user,cvs where user.id=cvs.userid group by user.id order by ncommits desc");
        }
  else {
      $QueryResult = mysql_query("select *,count(*) as ncommits from user,cvs where user.id=cvs.userid and user.active='Y' group by user.id order by ncommits desc");
        }

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$Buffer .= '
	  <tr valign="middle">
	  <td align="left">'.GetUserNameLink($QueryRow[user.id]).'</td>
	  <td align="left">'.$QueryRow[fname].'</td>
	  <td align="left">'.$QueryRow[lname].'</td>
          <td align="left">'.GetUserLastCommit($QueryRow[user.id]).'</td>
	  <td align="right">'.$QueryRow[ncommits].'</td>
	  </tr>';
  }

  $Buffer .= '</table>';

  return $Buffer;
}

function ShowUser($UserID)
{
  if($UserID == '')
  {
	return "User " . $UserID . " does not exist.<BR>" .
	  "Please return to <a href='people.php'>People</a> page " .
	  "and select another";
  }
  $QueryResult = mysql_query("select * from user where id=$UserID");
  if(mysql_num_rows($QueryResult) != 0)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$Buffer = '
	  <table cellpadding=0 cellspacing=0 border=0 width="100%">
	  <tr>
	  <td valign="top" align="left">
	  <table cellpadding=2 cellspacing=2 border=0 width="100%">
	  <tr><td><b>Username:</b></td><td>'.GetUserName($UserID).'</td></tr>
	  <tr><td><b>My Projects:</b></td><td>'.GetUserProjects($UserID).'</td></tr>
	  <tr><td><b>Status:</b></td><td>'.GetUserStatus($UserID).'</td></tr>
	  <tr><td><b>Number of Commits:</b></td><td>'.GetUserCommitsNumber($UserID).'</td></tr>
	  <tr><td><b>First Commit:</b></td><td>'.GetUserFirstCommit($UserID).'</td></tr>
	  <tr><td><b>Last Commit:</b></td><td>'.GetUserLastCommit($UserID).'</td></tr>
	  <tr><td><b>History:</b></td><td><a href="people.php?username='.
	  GetUserName($UserID).'&history=1">CVS Commits</a></td></tr>
	  <tr><td><b>Country of Origin:</b></td><td>'.$QueryRow[country].'</td></tr>
	  <tr><td><b>Country of Residence:</b></td><td>'.$QueryRow[residence].'</td></tr>';

	if(isset($_SESSION[type]) && $_SESSION[type]=='c')
	{
	  $Buffer .= '<tr><td><b>Email:</b></td><td><a href="mailto:' .
		      GetUserEmail($UserID).'">'.GetUserEmail($UserID) .
		      '</a></td></tr>
                      <tr><td><b>Phone:</b></td><td>'.$QueryRow[phone].'</td></tr>';
	}

	$Buffer .= '
	  <tr><td><b>Homepage:</b></td><td><a href="'.
	  CheckedURL($QueryRow[homepage]).'">'.$QueryRow[homepage].
	  '</a></td></tr><tr><td><b>About Me:</b></td><td>'.$QueryRow[about].
	  '</td></tr><tr><td colspan=2><br><b>Public PGP Key:</b></td></tr><tr><td colspan=2><pre>'.
	  $QueryRow[pgp].
	  '</pre></td></tr></table></td><td valign="top" align="right"><img src="'.
	  GetUserPicture($UserID).
	  '" alt="'.GetUserFLNames($UserID).'\'s Picture"align="right"></td> </tr></table>';

  }
  else
  {
	$Buffer="User " . $UserID . " does not exist.<BR>" .
	  "Please return to <a href='people.php'>People</a> page " .
	  "and select another";

  }
  return $Buffer;
}

function ShowUserCVSCommits($UserName)
{
  $Buffer = '';

  $QueryResult = mysql_query("select *,DATE_FORMAT(logdate, '%b %d, %Y %T') as commitdate from cvs where userid=".GetUserID($UserName)." order by logdate DESC");

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$Buffer .= '<b>'.$QueryRow[commitdate].'</b><pre>'.wordwrap(htmlspecialchars($QueryRow[logmsg]), 80).'</pre>';
  }

  return $Buffer;
}

function Contact()
{
  global $ContactAntiSpamEmail;

  $QueryResult = mysql_query("select * from user where type='c' order by lname,fname");

  $Buffer = '
	If you have read our call for <a href="help.php">help</a> and are ready to join the project, or would like to enquire about Arabeyes in general you can always write us an email. One of our \'core\' staff members will respond to you as soon as possible.<br><br>
	Core Staff:
	<ul>';

  while($QueryRow = mysql_fetch_array($QueryResult))
	$Buffer .= '<li>'.GetUserFLNamesLink($QueryRow[id]);
  $Buffer .= '
	</ul>
	For more information, email: <a href="mailto:'.$ContactAntiSpamEmail.'">contact (at) arabeyes dot org</a>.<br><br>';

  $Buffer = DisplaySection('Arabeyes Management Team', $Buffer);

  $Buffer .= DisplaySection('<a href="mailinglists.php">Mailing Lists</a>', '
	  Offer the ability to discuss Arabization issues, render contributions, put-forth inquires and receive answers. It continues to be the focal point of this effort. Its intent is for all to share their knowledge and experiences and offer advice and direction.<br><br>');

  $Buffer = $Buffer.DisplaySection('IRC - Internet Relay Chat', '
	  Offers the ability to discuss Arabeyes issues, render contributions, put-forth inquires and receive answers.<br><br>
	  <table width="50%" border=0 cellpadding=0 cellspacing=0>
	  <tr><td align="left">Server</td><td align="left">: <b>irc.freenode.net</b></td></tr>
	  <tr><td align="left">Channel</td><td align="left">: <a href="http://www.xgoogle.com/details?channel=%23arabeyes&network=163\">#arabeyes</a></td></tr>
	  </table>');

  return $Buffer;
}

function Donate()
{
  $Buffer = DisplaySection('Why Donate to Arabeyes', '
	  By donating money to the Arabeyes Project you are contributing to the Open Source
          community in many ways. You are above all ensuring the survival of our Arabisation
          efforts and the ideals that go with it. Your small donations can help us:<br>
	  <ul>
	  <li>Purchase better/new hardware
	  <li>Purchase more bandwidth
	  <li>Advertise/recruit new volunteers
	  <li>Accelerate the realization of some of our projects
	  <li>Pay for legal services
	  <li>Pay for traveling expenses to attend conferences/seminars	
	  <li>..and other costs endured for facilitating the project\'s development
	  </ul>
	  <br>
	  <b>Donation Remarks:</b>
	  <ul>
	  <li>As a donor you will be entitled to have access to Arabeyes\' donation and
              expenditure records, upon request.
	  <li>Your privacy is paramount to us and as such it will be guarded and dealt with
              in the utmost of care.
	  <li>Donations <b>do not</b> influence Arabeyes.org direction and commitment to the
              Open Source and Arab community.
	  </ul>');
/*
 Removed due to recent fraud (damn you thieves !!)
  $Buffer .= '<br>'.DisplaySection('How to Donate', 'Wire transfers are considered the best, safest
             and most widespread manner in which to transfer funds, as such below is Arabeyes\'
             account information,
	     <table align=center width=80% border=0 cellpadding=2 cellspacing=0>
               <tr>
	         <td><b>Bank Name</b></td>
	         <td>PacificTrust Bank</td>
		 <td width=20%>&nbsp;</td>
	       </tr><tr>
	         <td><b>Bank Address</b></td>
	         <td>610 Bay Blvd.<br>
                     Chula Vista, CA 91910</td>
	       </tr><tr>
	         <td><b>Bank Phone</b></td>
	         <td>800-338-3130</td>
	       </tr><tr>
	         <td><b>Beneficiary</b></td>
	         <td>Nadim Shaikli</td>
	       </tr><tr>
	         <td><b>Bank ABA Route Number</b></td>
	         <td>32-227-4527</td>
	       </tr><tr>
	         <td><b>Account Number</b></td>
	         <td>2020-00-5383</td>
	       </tr><tr>
	         <td><b>Fedline Short Name</b></td>
	         <td>PAC TRUST BK CV</td>
	       </tr><tr>
	       </tr>
	     </table>
	     <br>
	     Do please send us an email to <a href="mailto:contact@_ANTISPAM_arabeyes.org">
             contact (at) arabeyes dot org</a> if funds are in transit in order to confirm receipt.
             ');
*/
  $Buffer .= '<br>'.DisplaySection('How to Donate', 'If you are interested in
             helping this great cause, please send us mail to
	     <a href="mailto:contact@_ANTISPAM_arabeyes.org">
             contact (at) arabeyes dot org</a> and a reply with instructions
             will be forth-coming on how to proceed.
             ');

  $Buffer .= '<br>'.DisplaySection('Financial Records', 'All of Arabeyes\'
  <a href="finance.php">financial transactions</a> are available for
  public scrutiny.');

  return $Buffer;
}

function ShowUserPreferences($UserID)
{
  $QueryResult = mysql_query("select * from user where id=$UserID");
  $FormVars = mysql_fetch_array($QueryResult);

  $Buffer = DisplaySection(GetUserFLNames($UserID).'\'s Preferences', '
		<form method="post" action="user.php">
		<table border=0 cellpadding=2 cellspacing=2 width="100%">
		<tr><td><b>First Name:</b></td>
		<td><input type="text" name="fname" value="'.$FormVars[fname].'" size="25">
		</td></tr>
		<td><b>Last Name:</b></td>
		<td><input type="text" name="lname" value="'.$FormVars[lname].'" size="25">
		</td></tr>
		<tr><td><b>Email Address:</b></td>
		<td><input type="text" name="email" value="'.$FormVars[email].'" size="25">
		</td></tr>
		<td><b>Homepage:</b></td>
		<td><input type="text" name="homepage" value="'.$FormVars[homepage].'" 
		size="25"></td></tr>
		<tr><td><b>Date of Birth <i>yyyy-mm-dd</i>:</b></td>
		<td><input type="text" name="bdate" value="'.$FormVars[birthdate].'" size="25">
		</td></tr>
		<td><b>Country of Origin:</b></td>
		<td>'. select_arableague("country", $FormVars[country]).'</td>
		</tr>
		<tr><td><b>Country of Residence:</b></td>
		<td>'. select_world("residence", $FormVars[residence]).'</td></tr>
		<td><b>Contact Phone:</b></td>
		<td><input type="text" name="phone" value="'.$FormVars[phone].'" size="25">
		</td></tr>
		<tr><td colspan="4"><br><b>Please tell us a little about yourself:</b>
		<textarea rows="12" name="comments" cols="70">'.$FormVars[about].'
		</textarea></td></tr>
		<tr><td colspan="4"><br><b>Public PGP Key (if any):</b>
		<textarea rows="5" name="pgp" cols="70">'.$FormVars[pgp].'</textarea></td>
		</tr>
		<tr><td colspan=4 align="left">
                    <input type="submit" name="submit" value="Update">
                </td></tr>
		</table>
		</form>');

  $Buffer .= DisplaySection('Changing Password', '
		  You have to know what your original password is before you can change your password. PLEASE pick a <b>real</b> password.
		  <br><br>
		  <FORM METHOD="post" ACTION="user.php">
		  <table border="0" cellpadding="2" cellspacing="2">
		  <tr><td><b>Original Password:</b></td>
		  <td><input type="password" name="opass" value=""></td>
		  <td>&nbsp;</td><td>&nbsp;</td></tr>
		  <tr><td><b>New Password:</b></td>
		  <td><input type="password" name="npass" value=""></td>
		  <td><b>Verify Password:</b></td>
		  <td><input type="password" name="vpass" value=""></td></tr>
		  <tr><td colspan="2"><input type="submit" name="submit" value="Change Password">
		  </td><td>&nbsp;</td><td>&nbsp;</td>
		  </td></tr>
		  </table>
		  </form>');

  return $Buffer;
}

function MailingLists()
{	
  $QueryResult = mysql_query("select * from mailinglists where public='Y' order by name");

  $Buffer = '
	There are '.mysql_num_rows($QueryResult).' public Arabeyes mailing lists:
	<br>
	<br>
	<table align="center" border=0 cellpadding=0 cellspacing=0 width="80%">';
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);
	$Buffer .= '
	  <tr>
	  <th align="left"><a href="'.$QueryRow[link].'">'.$QueryRow[name].'</a></th>
	  <td>'.$QueryRow[description].'</td>
	  <td align="right">[<a href="'.$QueryRow[link_archives].'">Archives</a>]</td>';

	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	  $Buffer .= '
		<td align="right">[<a href="admin.php?mailinglist=yes&&edit=yes&&mailinglistid='.$QueryRow[id].'">Edit</a>]</td>';

	$Buffer .= '
	  </tr>';
  }
  $Buffer .= '</table>';

  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
  {
	$QueryResult = mysql_query("select * from mailinglists where public='N' order by name");
	$Buffer .= '
	  <br>
	  There are '.mysql_num_rows($QueryResult).' private Arabeyes mailing lists:
	  <br>
	  <br>
	  <table align="center" border=0 cellpadding=0 cellspacing=0 width="80%">';
	for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  $Buffer .= '
		<tr>
		<th align="left"><a href="'.$QueryRow[link].'">'.$QueryRow[name].'</a></th>
		<td>'.$QueryRow[description].'</td>
		<td align="right">[<a href="'.$QueryRow[link_archives].'">Archives</a>]</td>
		<td align="right">[<a href="admin.php?mailinglist=yes&&edit=yes&&mailinglistid='.$QueryRow[id].'">Edit</a>]</td>
		</tr>
		';
	}
	$Buffer .= '</table>';
  }

  $Buffer .= '
	<br>
	Please feel free to browse through the mailing lists\' archives and/or to utilize the "Search" option on the right menu before posting questions.';

  return $Buffer;
}

function HowToHelp()
{
  global $ContactAntiSpamEmail;

  $Buffer = '
	The <a href="'.$hosturl.'">Arabeyes</a> Project, a volunteer based not-for-profit (ie. not a company, no agenda, no bias) effort, aims at bringing forth Arabic to Linux in all its forms among other <a href="about.php">goals</a>.  As such Arabeyes, is always looking for volunteers with different skills to join in and fulfill it and its members\' goals and <a href="http://www.arabeyes.org/download/documents/about/manifesto-en">manifesto</a>.
	<br>
	<ul>
	<li><b>Translation/Technical Writing:</b><br>
	Arabeyes is searching for translators and technical writers, ranging from the semi-unexperienced hobbyist translators to professional linguists.
	<p>
	<a href="proj_trans.php">Translation Projects</a><br>
	<a href="http://www.arabeyes.org/download/documents/guide/translator-guide-en/">Translator Guide</a>
	</p>
	</li>
	<li><b>Code Development:</b><br>
	Arabeyes is searching for developers skilled in various languages (C, C++, Python, Perl, PHP, Java, etc), ranging from those just learning to those with many years of experience.
	<p><a href="http://www.arabeyes.org/download/documents/guide/developer-guide-en/">Developer Guide</a></p>
	</li>
	<li><b>Artistic Talent:</b><br>
	Arabeyes is searching for those gifted in the Art world in order to fill its need for original works of Art for such things are logos, banners, various artistic layout issues, etc.
	<p><a href="http://www.arabeyes.org/download/documents/guide/artist-guide-en/">Artist Guide</a></p>
	</li>
	<li><b>Public Relations:</b><br>
	Arabeyes is searching for those interested in shaping Arabeyes\' image and is looking for a media director/coordinator to better educate the world of our goals and achievements.
	<p><a href="http://www.arabeyes.org/download/documents/guide/public_relations-guide-en/">Public Relations Guide</a></p>
	</li>
	<li><b>Donations:</b><br>
	Arabeyes is in need of monetary funding primarily for hardware upkeep and bandwidth allocation (website, upgrades, etc).  Arabeyes\' records and ALL its purchases/transaction are accounted for and are openly available for all for inspection.
	<!-- <p><a href="donate.php">Donations</a></p> -->
	<p>Email: contact (at) arabeyes (dot) org</p>
	</li>
	</ul>
	<br>
	<p>
	Arabeyes\' efforts and its <a href="benefits.php">benefits</a> are of tangible, credible value to its volunteers and the Arab World at large. Our Interests and drive are genuine and will not falter in attaining its prescribed goals.
	<p>
	We thank you for your time and interest and look forward to <a href="mailto:'.$ContactAntiSpamEmail.'">hearing back</a> from you in hopes of future cooperation and further progress.
	</p>';

  return $Buffer;
}

function HelpTranslationProjects()
{
  $Buffer = '
	Here we a give a list and a small description of the current active translation
	projects. If you think that you are able to contribute in one or more projects from the 
	list below, then please read the 
	<a href="http://www.arabeyes.org/download/documents/guide/translator-guide-en/">Translator Guide</a> 
	and follow its guidelines.<br><br>
	<ul>
	<li><a href="project.php?proj=KDE">KDE</a>
	<p>
	This project aims to translate the KDE\'s interface to Arabic.
	</p>
	<li><a href="project.php?proj=GNOME">GNOME</a>
	<p>
	This project aims to translate the GNOME\'s interface to Arabic.
	</p>
	<li><a href="http://cvs.arabeyes.org/viewcvs/translate/misc/">
	Full Wordlist</a>
	<p>
	This file contains about 90000 English words and will be extremely useful 
	in any future translation enterprise. It is also used by the 
	<a href="project.php?proj=QaMoose">QaMoose project</a>.
	</p>
	<li>Arabization of <a href="'.$hosturl.'">Arabeyes</a>\' Website.
	<p>
	The current interface of our website is exclusively in English. We are 
	planning to translate the entire content to Arabic, so we can offer both
	English and Arabic interfaces on our website.
	</p>
	<li><a href="http://cvs.arabeyes.org/viewcvs/translate/docs/kde/">KDE docs</a>
	<p>
	Translation of KDE documentation and help files.
	</p>
	<li>More arabization of existing applications, projects, documentation...
	<li>Writing native Arabic documentation on different aspects of the open source
	world.
	</ul>';

  return $Buffer;
}

function Benefits()
{
  $Buffer = '
	<p>
	How does joining <a href="'.$hosturl.'">Arabeyes</a> benefit
	you ?  We offer the following in hopes of answering people\'s concerns.
	</p>
	<p>

	Why should a <b>volunteer</b> and someone who\'s concerned about this
	effort help out ?
	<ul>
	<li><b><i>Arabic Community benefits - </b></i>Your assistance and help in
	bringing Arabic to the Linux/Unix environment will bring you a great
	sense of satisfaction; for you are helping propel the Arabic language
	and culture into future generations of computer users.  Seeing your
	work used by many Arab users across the globe will bring you an
	indescribable feeling of joy
	</li>
	<li><b><i>Build your portfolio - </b></i>High-School/College students
	will get the opportunity to have include something very valuable
	to their resume/CV and portfolio.  Any work you accomplish
	under the Arabeyes umbrella can be stressed to future employers
	as proof of your ability to not only produce results, but most
	importantly, to work in a team.
	</li>
	<li><b><i>Foster Responsibility - </b></i>Arabeyes stresses the need to
	conduct all its projects in a professional manner in which schedules
	and upholding to predefined time-lines is paramount.  Arabeyes will
	surround you in an atmosphere where one\'s word and actions are equated
	to one\'s responsibility and reputation.
	<li><b><i>Get challenged - </b></i>Many of the projects Arabeyes hosts
	are challenging and will provide you with a chance to get immersed
	in solving complex issues.  If you are always looking for something
	to push you to your limit and are looking for a challenge, Arabeyes
	is for you.
	</li>
	<li><b><i>Gain Experience - </b></i>As you use the tools provided by the
	Arabeyes project, you build a solid foundation in fast-paced global
	development.  It will give you experience to carry on and use in your
	own projects/work.
	</li>
	</ul>
	<br>
	<p>
	Why should <b>Universities</b> work on Open Source Projects/Systems ?
	<ul>
	<li>Ensures continuity of projects
	</li>
	<li>Provide extra experience to students when dealing with existing
	code; its been proven that 80% of "real-world" coding is for
	maintenance and upgrades rather than building from scratch
	</li>
	<li>Provide students with all the needed development/test tools
	</li>
	<li>Working on Open Source Software lowers the Cost of Ownership
	</li>
	<li>Working on Open Source projects generates respect and garners a
	reputation
	</li>
	</ul>
	<br>
	<p>
	Why should <b>Universities</b> contribute to Arabization projects ?
	<ul>
	<li>Arabization projects need a continuous stream of developers to
	reach their goals
	</li>
	<li>Working on projects that require altering/updating existing code
	will ensure students abilities to work on others\' code (mimic real
		life experience)
	</li>
	<li>Arabic language support in Linux/Unix systems is still in its
	early stages with ample room to make a difference
	</li>
	<li>Arabeyes\' efforts and code are freely available and could
	serve as an educational experience for issues related to
	Arabic Language computing
	<li>Arabization projects offer students study/research avenues
	</li>
	</ul>
	<br>
	<p>
	How do <b>Universities</b> Benefits ?
	<ul>
	<li>Ideas for future research
	</li>
	<li>Personal Development
	</li>
	<li>Case studies for teaching
	</li>
	<li>More involved student projects
	</li>
	<li>Improved global reputation
	</li>
	<li>Enriching curriculum
	</li>
	<li>Possibly enticing external funding of internal projects
	</li>
	</ul>';

  return $Buffer;
}

function ShowRegistrationForm($FormVars)
{ 
  $Buffer = 'You read <a href="about.php">about Arabeyes</a> and you <a href="help.php">want to help</a> ?
	<br><br>';

  $Buffer .= DisplaySection(
	  'Registration Form', '
	  <form method="post" action="register.php">
	  <table border=0 cellpadding=0 cellspacing=0>
	  <tr>
	  <td colspan="3"><strong><font color="#FF0000">NOTE: Please fill
            this form with real and valid information or your application
            will be discarded.</font></strong></td>
          </tr>
          <tr>
	  <td>&nbsp;</td>
	  </tr>
	  <tr>
	  <td><b>First Name:</b></td>
	  <td width="2px"></td>
	  <td align="left"><input type="text" name="fname" value="'.$FormVars[fname].'" size="20"></td>
	  </tr><tr>
	  <td><b>Last Name:</b></td>
	  <td width="2px"></td>
	  <td align="left"><input type="text" name="lname" value="'.$FormVars[lname].'" size="20"></td>
	  </tr>
	  <tr>
	  <td><b>Email Address:</b></td>
	  <td></td>
	  <td align="left"><input type="text" name="email" value="'.$FormVars[email].'" size="20"></td>
	  </tr><tr>
	  <td><b>Homepage:</b></td>
	  <td></td>
	  <td align="left"><input type="text" name="homepage" value="'.$FormVars[homepage].'" size="20"></td>
	  </tr>
	  <tr>
	  <td><b>Date of Birth<br><i>yyyy-mm-dd</i>:</b></td>
	  <td></td>
	  <td align="left"><input type="text" name="bdate" value="'.$FormVars[bdate].'" size="20"></td>
	  </tr><tr>
	  <td><b>Contact Phone:</b></td>
	  <td></td>
	  <td align="left"><input type="text" name="phone" value="'.$FormVars[phone].'" size="20"></td>
	  </tr>
	<tr>
	<td><b>Web Password:</b></td>
	  <td></td>
	<td align="left"><input type="password" name="passwd" value="" size="20"></td>
	  </tr><tr>
	<td><b>Verify Password:</b></td>
	  <td></td>
	<td align="left"><input type="password" name="vpasswd" value="" size="20"></td>
	</tr>
	<tr>
	<td><b>Join as a:</b></td>
	<td></td>
	<td>'.create_selectform("type", GetVolunteerType(), $FormVars[type]).'</td>
	</tr><tr>
	<td><b>UNIX/LINUX<br>Experience:</b></td>
	<td></td>
	<td>'.create_selectform("ixexp", GetExperianceLevels(), $FormVars[ixexp]).'</td>
	</tr>
	</table>
	<table>
	<tr>
	<td><b>Country of Origin:</b></td>
	<td></td>
	<td>'. select_world("country", $FormVars[country]).'</td>
	</tr>
	<tr>
	<td><b>Country of Residence:</b></td>
	<td></td>
	<td>'. select_world("residence", $FormVars[residence]).'</td>  
	</tr>
	<tr><td colspan="4"><b>Public PGP Key:</b>
	<br>
	<p><textarea rows="5" name="pgp" cols="70">'.$FormVars[pgp].'</textarea></p></td>
	</tr>
	<tr><td colspan="4"><b>Please tell us a little about yourself:</b>
	<br>
	<p><textarea rows="12" name="comments" cols="70">'.$FormVars[comments].'</textarea></p></td>
	</tr>
	<tr><td colspan="4"><b>How did you hear about Arabeyes ?</b>
	<br>
	<p><textarea rows="3" name="howheard" cols="70">'.$FormVars[howheard].'</textarea></p></td>
	</tr>
	<br>
	<tr><td colspan="4"><b>Verify Your Registration...</b></td></tr>
	<br>
	<tr><td colspan="4"><img src="freecap/freecap.php" id="freecap"></td></tr>
	<br>
	<tr><td colspan="2">Enter the code shown:</td><td><input type="text" name="word"></td></tr>
	<br>

	<tr><td colspan=4 align="left"><input type="submit" name="submit" value="Register"></td>
	</tr>
	</table>
	</form>');

  $Buffer .= DisplaySection('Arabeyes Privacy Statement', '
	  <b>Arabeyes.org</b> is committed to protecting the privacy of the users of our site.
	  At Arabeyes.org we try to keep the amount of information we collect on you to an 
	  absolute bare minimum. We also do NOT publically display any of the data that we
	  consider to be personal (e.g. date of birth, phone numbers, etc.)

	  <p>The information we gather on our users consist of the following:</p>
	  <ul>
	  <li>Web logs (ip addresses, pages visited, etc)
	  <li>Email addresses (via mailing-lists which are closed-lists and only members can
		view the list of emails)
	  <li>Volunteers fill out other extra information that is contained in this form as well.
	  However, only the full name, homepage and country of origin/residence will be
	  publically displayed
	  </ul>');

  return $Buffer;
}

function ShowRegistrationOK()
{
  $Buffer = '
	<p>The Arabeyes.org Team welcomes you aboard !</p>
	<br>
	<p>As soon as one of our coordinators reviews your application, you will be assigned a username and your CVS account will be created. In the meantime, please be sure to subscribe to the following mailing-lists as a minimum:
	</p>
	<ul>
	<li>General - general Arabization discussions.
	<li>Announce- Official Arabeyes announcements (extremely low-traffic).
	<li>Other related mailing-lists, depending on your inclination: <a href="mailinglists.php">Complete List</a>.
	</ul>';

  return $Buffer;
}

function Maintenance()
{
  $Buffer = '
	We apologize for any inconvenience you may be experiencing on our web site. We are
	currently moving from our older connection to a better and faster one. This process
	is unfortunately affecting the way some of the links here as well as some subtle
	undesirable behaviors. We are aware of it and working on it.
	<br>
	Thank you'; 

  return $Buffer;
}

function EditNews($NewsID)
{
  $PostID = '?news=yes';
  $SubmitType = 'Add';

  $Date = GetDateYMD();
  $Title = '';
  $Article = '';

  if(!empty($NewsID))
  {
	$PostID .= '&&newsid='.$NewsID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from news where id=$NewsID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Date = $QueryRow[date];
	$Title = $QueryRow[title];
	$Article = $QueryRow[article];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Date</td><td><input name="date" value="'.$Date.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Title</td><td><input name="title" value="'.$Title.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Article</td><td><textarea name="article" rows=10 cols=60>'.$Article.'</textarea></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($NewsID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function EditProject($ProjectID)
{
  $PostID = '?project=yes';
  $SubmitType = 'Add';

  //These should be read from the Db: with the possibility to add a new entry
  $Hides = array('N', 'Y');
  $VisibleHides = array('Unhide', 'Hide');
  $Types = array('t', 'd', 'e', 'f');
  $VisibleTypes = array('Translation', 'Developement', 'External/Patches', 'Fonts/Art');

  $ListIDs = '';
  $VisibleListIDs = '';
  $QueryResult = mysql_query("select * from mailinglists where public='Y' order by name");
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$VisibleListIDs[] = $QueryRow[name];
	$ListIDs[] = $QueryRow[id];
  }

  $Name = '';
  $About = '';
  $Image = '';
  $Status = '';
  $Type = '';
  $ListID = '';
  $Aim = '';
  $Comment = '';
  $StartDate = GetDateYMD();
  $Hide = 'Y';
  $Notes = '';
  $Downloads = '';
  $Screenshots = '';
  $Links = '';

  if(!empty($ProjectID))
  {
	$PostID .= '&&projectid='.$ProjectID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from proj_about where proj_id=$ProjectID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Name = $QueryRow[proj_name];
	$About = $QueryRow[about];
	$Status = $QueryRow[status];
	$Type = $QueryRow[type];
	$ListID = $QueryRow[listid];
	$Aim = $QueryRow[aim];
	$Comment = $QueryRow[comment];
	$StartDate = $QueryRow[startdate];
	$Hide = $QueryRow[hide];
	$Notes = $QueryRow[notes];
	$Downloads = $QueryRow[downloads];
	$Screenshots = $QueryRow[screenshots];
	$Links = $QueryRow[links];
  }

  $Buffer = '
	<form method="post" action="project.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">';

  // Core Members are very cool. They get far more options to edit
  // Maintainers will still have these options in their forms, but hidden
  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
    $Buffer .= '
	<tr valign="top" align="left"><td>Name</td><td><input name="name" value="'.$Name.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Type</td><td><select name="type">'.DisplaySelectField($Types, $VisibleTypes, $Type).'</select></td></tr>
	<tr valign="top" align="left"><td>Hide</td><td><select name="hide">'.DisplaySelectField($Hides, $VisibleHides, $Hide).'</select></td></tr>
	<tr valign="top" align="left"><td>Project list</td><td><select name="listid">'.DisplaySelectField($ListIDs, $VisibleListIDs, $ListID).'</select></td></tr>
	<tr valign="top" align="left"><td>Start Date</td><td><input name="startdate" value="'.$StartDate.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Aim</td><td><textarea name="aim" rows=5 cols=60>'.$Aim.'</textarea></td></tr>
	<tr valign="top" align="left"><td>Comment</td><td><textarea name="comment" rows=5 cols=60>'.$Comment.'</textarea></td></tr>';
  else
    $Buffer .= '
	<tr valign="top" align="left"><td>Name: </td><td>'.$Name.'</td></tr>
	<tr valign="top" align="left"><td>Type: </td><td>'.$Type.'</td></tr>
	<tr valign="top" align="left"><td>Start Date: </td><td>'.$StartDate.'</td></tr>
	<tr valign="top" align="left"><td>Aim: </td><td>'.$Aim.'</td></tr>
	<tr valign="top" align="left"><td>Comment: </td><td>'.$Comment.'</td></tr>
	<tr valign="top" align="left">
		<td>
			<input type=hidden name="name" value="'.$Name.'">
			<input type=hidden name="type" value="'.$Type.'">
			<input type=hidden name="hide" value="'.$Hide.'">
			<input type=hidden name="listid" value="'.$ListID.'">
			<input type=hidden name="startdate" value="'.$StartDate.'">
			<input type=hidden name="aim" value="'.$Aim.'">
			<input type=hidden name="comment" value="'.$Comment.'">
		</td>
	</tr>';

  $Buffer .= '
  	<tr valign="top" align="left"><td>Status</td><td><input name="status" value=\''.$Status.'\' size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>About</td><td><textarea name="about" rows=10 cols=60>'.$About.'</textarea></td></tr>
	<tr valign="top" align="left"><td>Notes</td><td><textarea name="notes" rows=10 cols=60>'.$Notes.'</textarea></td></tr>
	<tr valign="top" align="left"><td>Links</td><td><textarea name="links" rows=10 cols=60>'.$Links.'</textarea></td></tr>
	<tr valign="top" align="left"><td>Screenshots</td><td><textarea name="screenshots" rows=10 cols=60>'.$Screenshots.'</textarea></td></tr>
	<tr valign="top" align="left"><td>Downloads</td><td><textarea name="downloads" rows=10 cols=60>'.$Downloads.'</textarea></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($ProjectID) && isset($_SESSION[type]) && $_SESSION[type] == 'c')
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>';

  if(!empty($ProjectID))
  {
	$QueryResult = mysql_query("select * from proj_contrib where proj_id=$ProjectID order by access_level desc");

	$TmpBuffer = '<table cellspacing=0 cellpadding=0 border=0 width="100%">';

	for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);

	  $TmpBuffer .= '<tr><td align="left">'.GetUserFLNamesLink($QueryRow[userid]).'</td><td align="left">'.GetAccessLevelName($QueryRow[access_level]).'</td><td align="right">[<a href="project.php?projectcontribution=yes&&contributionid='.$QueryRow[id].'&&projectid='.$ProjectID.'&&edit=yes">Edit</a>]</td></tr>';
	}

	$TmpBuffer .= '</table>';

	$Buffer .= DisplaySection('Users of this Project [<a href="project.php?projectcontribution=yes&&projectid='.$ProjectID.'&&add=yes">Add a user</a>]', $TmpBuffer);
  }

  return $Buffer;
}

function EditPress($PressID)
{
  $PostID = '?press=yes';
  $SubmitType = 'Add';

  $Date = GetDateYMD();
  $Name = '';
  $Link = '';
  $Source = '';
  $SourceLink = '';
  $Description = '';

  if(!empty($PressID))
  {
	$PostID .= '&&pressid='.$PressID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from press where id=$PressID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Date = $QueryRow[date];
	$Name = $QueryRow[name];
	$Link = $QueryRow[link];
	$Source = $QueryRow[source];
	$SourceLink = $QueryRow[sourcelink];
	$Description = $QueryRow[description];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Date</td><td><input name="date" value="'.$Date.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Topic</td><td><input name="name" value="'.$Name.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Link</td><td><input name="link" value="'.$Link.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Source Name</td><td><input name="source" value="'.$Source.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Source Link</td><td><input name="sourcelink" value="'.$SourceLink.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Description</td><td><textarea name="description" rows=10 cols=60>'.$Description.'</textarea></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($PressID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function EditProjectLog($ProjectID, $LogID)
{
  $PostID = '?projectid='.$ProjectID.'&&log=yes';
  $SubmitType = 'Add';

  $Date = GetDateYMD();
  $Message = '';

  if(!empty($LogID))
  {
	$PostID .= '&&logid='.$LogID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from proj_history where log_id=$LogID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Date = $QueryRow[log_date];
	$Message = $QueryRow[log_msg];
  }

  $Buffer = '
	<form method="post" action="project.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Date</td><td><input name="date" value="'.$Date.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Message</td><td><textarea name="message" rows=10 cols=60>'.$Message.'</textarea></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($LogID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function ForgotPassword()
{
  $Buffer = '
	<form method="post" action="index.php?forgotpass=yes">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Username</td><td><input name="username" value="" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Email</td><td><input name="email" value="" size=50 type=text></td></tr>
	</table>
	<input type=submit name="Submit" value="Submit" align=center>
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function EditDocument($DocumentID)
{
  $PostID = '?document=yes';
  $SubmitType = 'Add';

  //These should be read from the Db: with the possibility to add a new entry
  $Audiences = array('a', 'd', 'g', 't', 'u');
  $VisibleAudiences = array('About Arabeyes', 'Developer', 'Graphical Designer', 'Translator', 'User');

  $Name = '';
  $Audience = '';
  $Description = '';

  if(!empty($DocumentID))
  {
	$PostID .= '&&documentid='.$DocumentID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from docs where id=$DocumentID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Name = $QueryRow[name];
	$Audience = $QueryRow[audience];
	$Description = $QueryRow[description];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Name</td><td><input name="name" value="'.$Name.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Audience</td><td><select name="audience">'.DisplaySelectField($Audiences, $VisibleAudiences, $Audience).'</select></td></tr>
	<tr valign="top" align="left"><td>Description</td><td><textarea name="description" rows=10 cols=60>'.$Description.'</textarea></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($DocumentID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>';

  if(!empty($DocumentID))
  {
	$QueryResult = mysql_query("select * from docs_versions where docid=$DocumentID");

	$TmpBuffer = '<table cellspacing=0 cellpadding=0 border=0 width="100%">';

	for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);

	  $TmpBuffer .= '<tr><td align="left"><a href="'.$QueryRow[link].'">'.$QueryRow[format].' | '.$QueryRow[lang].'</a></td><td align="right">[<a href="admin.php?version=yes&&versionid='.$QueryRow[id].'&&documentid='.$DocumentID.'&&edit=yes">Edit</a>]</td></tr>';
	}

	$TmpBuffer .= '</table>';

	$Buffer .= DisplaySection('Versions of this Document [<a href="admin.php?version=yes&&documentid='.$DocumentID.'&&add=yes">Add a version</a>]', $TmpBuffer);
  }

  return $Buffer;
}

function EditDocumentVersion($DocumentID, $VersionID)
{
  $PostID = '?documentid='.$DocumentID.'&&version=yes';
  $SubmitType = 'Add';

  //These should be read from the Db: with the possibility to add a new entry
  $Formats = array('HTML', 'PDF');
  $VisibleFormats = array('HTML', 'PDF');

  $Langs = array('EN', 'AR', 'FR');
  $VisibleLangs = array('English', 'Arabic', 'French');

  $Lang = '';
  $Format = '';
  $Link = '';

  if(!empty($VersionID))
  {
	$PostID .= '&&versionid='.$VersionID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from docs_versions where id=$VersionID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Lang = $QueryRow[lang];
	$Format = $QueryRow[format];
	$Link = $QueryRow[link];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Link</td><td><input name="link" value="'.$Link.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Language</td><td><select name="lang">'.DisplaySelectField($Langs, $VisibleLangs, $Lang).'</select></td></tr>
	<tr valign="top" align="left"><td>Format</td><td><select name="format">'.DisplaySelectField($Formats, $VisibleFormats, $Format).'</select></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($VersionID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function EditUser($UserID)// use this fct to edit user pref
{
  $PostID = '?user=yes';
  $SubmitType = 'Add';
  
  $Actives = array('Y', 'N');
  $VisibleActives = array('Active', 'Inactive');
  $Types = array('a', 'c', 'd', 'p', 't');
  $VisibleTypes = array('Artist', 'Core-ite', 'Developer', 'Public relations officer', 'Technical writer');
  $LinuxExps = array('>5', '>3', '>1', '<1');
  $VisibleLinuxExps = array('More than five years', 'More than three years', 'More than one year', 'Less than one year');
  
  $FirstName = '';
  $LastName = '';
  $BirthDate = '';
  $Origin = '';
  $Residence = '';
  $Phone = '';
  $Email = '';
  $HomePage = 'http://';
  $PGPKey = '';
  $LinuxExp = '<1';
  $About = '';
  $HowHeard = '';
  $UserName = '';
  $Active = 'Y';
  $Type = 't';
  
  $user_string = '
	<tr valign="top" align="left"><td>Username</td><td><input name="username" value="'.$UserName.'" size=50 type=text></td></tr>';

  if(!empty($UserID))
    {
      $PostID .= '&&userid='.$UserID;
      $SubmitType = 'Update';
      
      $QueryResult = mysql_query("select * from user where id=$UserID");
      $QueryRow = mysql_fetch_array($QueryResult);
      
      $UserName = $QueryRow[username];
      $FirstName = $QueryRow[fname];
      $LastName = $QueryRow[lname];
      $BirthDate = $QueryRow[birthdate];
      $Origin = $QueryRow[country];
      $Residence = $QueryRow[residence];
      $Phone = $QueryRow[phone];
      $Email = $QueryRow[email];
      $HomePage = $QueryRow[homepage];
      $PGPKey = $QueryRow[pgp];
      $LinuxExp = $QueryRow[ixexp];
      $About = $QueryRow[about];
      $HowHeard = $QueryRow[howheard];
      $Active = $QueryRow[active];
      $Type = $QueryRow[type];

      $user_string = '
	<tr valign="top" align="left"><td>Username</td><td>'.$UserName.'</td></tr>';
    }
  
  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">'
        .$user_string.
	'<tr valign="top" align="left"><td>First name</td><td><input name="fname" value="'.$FirstName.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Last name</td><td><input name="lname" value="'.$LastName.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Birth date (YYYY-MM-DD)</td><td><input name="birthdate" value="'.$BirthDate.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Origin</td><td><select name="country">'.DisplaySelectField(GetArabLeagueCountries(), GetArabLeagueCountries(), $Origin).'</select></td></tr>
	<tr valign="top" align="left"><td>Residence</td><td><select name="residence">'.DisplaySelectField(GetArabLeagueCountries(), GetArabLeagueCountries(), $Residence).'</select></td></tr>
	<tr valign="top" align="left"><td>Phone</td><td><input name="phone" value="'.$Phone.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Email</td><td><input name="email" value="'.$Email.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Homepage</td><td><input name="homepage" value="'.$HomePage.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>PGP key</td><td><textarea name="pgp" rows=10 cols=60>'.$PGPKey.'</textarea></td></tr>
	<tr valign="top" align="left"><td>Linux experience</td><td><select name="ixexp">'.DisplaySelectField($LinuxExps, $VisibleLinuxExps, $LinuxExp).'</select></td></tr>
	<tr valign="top" align="left"><td>About</td><td><textarea name="about" rows=10 cols=60>'.$About.'</textarea></td></tr>
	<tr valign="top" align="left"><td>How did you hear about Arabeyes ?</td><td><textarea name="howheard"] rows=10 cols=60>'.$HowHeard.'</textarea></td></tr>';

  if(empty($UserID))
    {
      $Buffer .= '
	  <tr valign="top" align="left"><td>Password</td><td><input name="password1" value="" size=50 type=password></td></tr>
	  <tr valign="top" align="left"><td>Verify password</td><td><input name="password2" value="" size=50 type=password></td></tr>';
    }
  
  $Buffer .= '
	<tr valign="top" align="left"><td>Active</td><td><select name="active">'.DisplaySelectField($Actives, $VisibleActives, $Active).'</select></td></tr>
	<tr valign="top" align="left"><td>Type</td><td><select name="type">'.DisplaySelectField($Types, $VisibleTypes, $Type).'</select></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';
  
  if(!empty($UserID))
    $Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';
  
  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>';
  
  if(!empty($UserID))
    {
      $TmpBuffer = '
	  <form method="post" action="admin.php?password=yes&&userid='.$UserID.'">
	  <table cellspacing=2 cellpadding=2 border=0 width="100%">
	  <tr valign="top" align="left"><td>New password</td><td><input name="password1" value="" size=50 type=password></td></tr>
	  <tr valign="top" align="left"><td>Verify password</td><td><input name="password2" value="" size=50 type=password></td></tr>
	  </table>
	  <input type=submit name="Update" value="Update" align=center>
	  </form>';
      
      $Buffer .= DisplaySection('User Password', $TmpBuffer);
      
      $QueryResult = mysql_query("select * from proj_contrib where userid=$UserID order by access_level desc");

      $TmpBuffer = '<table cellspacing=0 cellpadding=0 border=0 width="100%">';
      
      for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  
	  $TmpBuffer .= '<tr><td align="left">'.GetProjectLink($QueryRow[proj_id]).'</td><td align="left">'.GetAccessLevelName($QueryRow[access_level]).'</td><td align="right">[<a href="admin.php?usercontribution=yes&&contributionid='.$QueryRow[id].'&&userid='.$UserID.'&&edit=yes">Edit</a>]</td></tr>';
	}
      
      $TmpBuffer .= '</table>';
      
      $Buffer .= DisplaySection('Projects of this User [<a href="admin.php?usercontribution=yes&&userid='.$UserID.'&&add=yes">Add a project</a>]', $TmpBuffer);
    }
  
  return $Buffer;
}

function GetAccessLevelName($AccessLevel)
{
  if($AccessLevel == 0)
	return 'Read-Only Contributor';
  else if($AccessLevel == 1)
	return 'Contributor';
  else if($AccessLevel == 2)
	return 'Maintainer';
  else
	return 'Unkown Access !';
}

function EditUserContribution($UserID, $ContributionID)
{
  $PostID = '?userid='.$UserID.'&&usercontribution=yes';
  $SubmitType = 'Add';

  $VisibleProjects = '';
  $Projects = '';
  $VisibleAccessLevels = '';
  $AccessLevels = '';

  $QueryResult = mysql_query("select * from proj_about order by type desc,proj_name");
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$VisibleProjects[] = $QueryRow[proj_name];
	$Projects[] = $QueryRow[proj_id];
  }

  for($i = 0; $i < 3; $i++)
  {
	$VisibleAccessLevels[] = GetAccessLevelName($i);
	$AccessLevels[] = $i;
  }

  $Project = '';
  $AccessLevel = '';

  if(!empty($ContributionID))
  {
	$PostID .= '&&contributionid='.$ContributionID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from proj_contrib where id=$ContributionID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Project = $QueryRow[proj_id];
	$AccessLevel = $QueryRow[access_level];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Project</td><td><select name="projectid">'.DisplaySelectField($Projects, $VisibleProjects, $Project).'</select></td></tr>
	<tr valign="top" align="left"><td>Access level</td><td><select name="access_level">'.DisplaySelectField($AccessLevels, $VisibleAccessLevels, $AccessLevel).'</select></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($ContributionID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function EditProjectContribution($ProjectID, $ContributionID)
{
  $PostID = '?projectid='.$ProjectID.'&&projectcontribution=yes';
  $SubmitType = 'Add';

  $VisibleUsers = '';
  $Users = '';
  $VisibleAccessLevels = '';
  $AccessLevels = '';

  $QueryResult = mysql_query("select * from user order by fname");
  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$VisibleUsers[] = $QueryRow[fname].' '.$QueryRow[lname];
	$Users[] = $QueryRow[id];
  }

  for($i = 0; $i < 3; $i++)
  {
	$VisibleAccessLevels[] = GetAccessLevelName($i);
	$AccessLevels[] = $i;
  }

  $Project = '';
  $AccessLevel = '';

  if(!empty($ContributionID))
  {
	$PostID .= '&&contributionid='.$ContributionID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from proj_contrib where id=$ContributionID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$User = $QueryRow[userid];
	$AccessLevel = $QueryRow[access_level];
  }

  $Buffer = '
	<form method="post" action="project.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>User</td><td><select name="userid">'.DisplaySelectField($Users, $VisibleUsers, $User).'</select></td></tr>
	<tr valign="top" align="left"><td>Access level</td><td><select name="access_level">'.DisplaySelectField($AccessLevels, $VisibleAccessLevels, $AccessLevel).'</select></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($ContributionID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function EditMailingList($MailingListID)
{
  $PostID = '?mailinglist=yes';
  $SubmitType = 'Add';

  //These should be read from the Db: with the possibility to add a new entry
  $Publics = array('N', 'Y');
  $VisiblePublics = array('Private', 'Public');

  $Name = '';
  $Link = '';
  $LinkArchives = '';
  $Details = '';
  $Description = '';
  $Public = '';

  if(!empty($MailingListID))
  {
	$PostID .= '&&mailinglistid='.$MailingListID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from mailinglists where id=$MailingListID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Name = $QueryRow[name];
	$Link = $QueryRow[link];
	$LinkArchives = $QueryRow[link_archives];
	$Details = $QueryRow[details];
	$Description = $QueryRow[description];
	$Public = $QueryRow['public'];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Name</td><td><input name="name" value="'.$Name.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Public List</td><td><select name="public">'.DisplaySelectField($Publics, $VisiblePublics, $Public).'</select></td></tr>
	<tr valign="top" align="left"><td>Link</td><td><input name="link" value="'.$Link.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Link to Archives</td><td><input name="link_archives" value="'.$LinkArchives.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Description</td><td><input name="description" value="'.$Description.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Details</td><td><textarea name="details" rows=10 cols=60>'.$Details.'</textarea></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($MailingListID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function EditRelatedSite($RelatedSiteID)
{
  $PostID = '?relatedsite=yes';
  $SubmitType = 'Add';

  $Name = '';
  $Link = '';

  if(!empty($RelatedSiteID))
  {
	$PostID .= '&&relatedsiteid='.$RelatedSiteID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from relatedsites where id=$RelatedSiteID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Name = $QueryRow[name];
	$Link = $QueryRow[link];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Name</td><td><input name="name" value="'.$Name.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Link</td><td><input name="link" value="'.$Link.'" size=50 type=text></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($RelatedSiteID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>';

  return $Buffer;
}

function EditResource($ResourceID)
{
  $PostID = '?resource=yes';
  $SubmitType = 'Add';

  //These should be read from the Db: with the possibility to add a new entry
  $Types = array('cs', 'ff', 'fmu');
  $VisibleTypes = array('Codepages and Shaping', 'Free Fonts', 'Font Manipulation Utilities');

  $Type = '';
  $Name = '';
  $Link = '';
  $Description = '';

  if(!empty($ResourceID))
  {
	$PostID .= '&&resourceid='.$ResourceID;
	$SubmitType = 'Update';

	$QueryResult = mysql_query("select * from resources where id=$ResourceID");
	$QueryRow = mysql_fetch_array($QueryResult);

	$Type = $QueryRow[type];
	$Name = $QueryRow[name];
	$Link = $QueryRow[link];
	$Description = $QueryRow[description];
  }

  $Buffer = '
	<form method="post" action="admin.php'.$PostID.'">
	<table cellspacing=2 cellpadding=2 border=0 width="100%">
	<tr valign="top" align="left"><td>Type</td><td><select name="type">'.DisplaySelectField($Types, $VisibleTypes, $Type).'</select></td></tr>
	<tr valign="top" align="left"><td>Name</td><td><input name="name" value="'.$Name.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Link</td><td><input name="link" value="'.$Link.'" size=50 type=text></td></tr>
	<tr valign="top" align="left"><td>Description</td><td><textarea name="description" rows=10 cols=60>'.$Description.'</textarea></td></tr>
	</table>
	<input type=submit name="'.$SubmitType.'" value="'.$SubmitType.'" align=center>';

  if(!empty($ResourceID))
	$Buffer .= '
	  <input type=submit name="Delete" value="Delete" onClick="return ConfirmDelete()" align=center>';

  $Buffer .= '
	<input type=submit name="Cancel" value="Cancel" align=center>
	</form>
	';

  return $Buffer;
}

function Finances()
{
	global $llimit,$rowsonpage;
	$Buffer = '<table align="center" width="80%" border=0 cellpadding=0 cellspacing=0>
	<tr>
	<th align="center" colspan=2>Date</th>
	<th align="center">Type</b></th>
	<th align="right">Amount</th>'
	#<th align="left">Comment</b></th>
	.'<th align="center" colspan=1>Donor</b></th>
	</tr>';
	if ($llimit==-1)
	{
		# -1 defined as default:
		# this means show the last record!
		$QueryResult = mysql_query("select count(*) from finances");
		$finances_count = mysql_fetch_array($QueryResult);
		#$llimit=$finances_count[0]-$rowsonpage;
		$llimit=intval($finances_count[0]/10)*10;
		if ($llimit<0)
		{
			$llimit=0;
		}
	}
	$prevTotal=0;
	$uppertable='Arabeyes in keeping with its transparent policy is fully divulging all its
                     financial translations below (both donations and expenditures).  The donors\'
                     names are listed below unless specific instructions or requests were made
                     to remain anonymous.  All Figures are in US dollar.<br><br>
       		     <table width=80% border=0 align=center cellpadding=0 cellspacing=0>
		     <tr>
		     <td width=50%>&nbsp;</td>';
	if ($rowsonpage==-1)
	{
		$sql="select * from finances order by date";
	}
	else
	{
		$QueryResult = mysql_query("select Amount,Type from finances order by date asc limit 0,".strval($llimit));
		while($QueryRow = mysql_fetch_array($QueryResult))
		{
			$prevTotal = ($QueryRow[Type]=="Deposit") ? $prevTotal+$QueryRow[Amount] :
                                                                    $prevTotal-$QueryRow[Amount];
		}
		$QueryResult = mysql_query("select count(*) from finances");
		$QueryRow = mysql_fetch_array($QueryResult);
		$lastrow = $QueryRow[0];
		$previouslink = "";
		$nextlink = "";
		if ($llimit == 0)
		{
			$previouslink="finance.php";
			$previouslink="";
		}
		elseif ($llimit-$rowsonpage<=0)
		{
			$previouslink="finance.php?llimit=0&rowsonpage=".$rowsonpage;
			$previouslink='<a href=\''.$previouslink.'\'>Previous</a>';
		}
		else
		{
			$previouslink="finance.php?llimit=".strval($llimit-$rowsonpage)."&rowsonpage=".$rowsonpage;
			$previouslink='<a href=\''.$previouslink.'\'>Previous</a>';
		}
		if ($llimit+$rowsonpage>=$lastrow)
		{
			$nextlink="";
		}
		#elseif($llimit+2*$rowsonpage>=$lastrow)
		#{
		#	$nextlink="finance.php?rowsonpage=".$rowsonpage;
		#	$nextlink=' <a href=\''.$nextlink.'\'>Next</a>';
		#}		
		else
		{
			$nextlink="finance.php?llimit=".($llimit+$rowsonpage)."&rowsonpage=".$rowsonpage;
			$nextlink=' <a href=\''.$nextlink.'\'>Next</a>';
		}
		$uppertable .= '<td width=\"64%\"><div align=right>'
                               .$previouslink.$nextlink.'</div></td>
		               <td>&nbsp;</td>
			       </tr><tr>
			       <td>&nbsp;</td>
			       <td align=left>[Balance, previous pages</td>
			       <td>=&nbsp;&nbsp;</td>';
		$balcolor  = ($prevTotal>=0) ? "green" : "red";
		$uppertable .= '<td align=right>$<font color='.$balcolor.'>'
			       .sprintf("%.2f", abs($prevTotal))
                               .'</font>]</td>
			       </tr><tr>
			       <td>&nbsp;</td>';
		$sql="select * from finances order by date limit ";
		$sql.=$llimit.",".strval($rowsonpage);
	}

	#echo $sql;
	$QueryResult = mysql_query($sql);
	$Total = 0;

	while($QueryRow = mysql_fetch_array($QueryResult)) 
	{
		$Total  = ($QueryRow[Type]=="Deposit") ? $Total+$QueryRow[Amount] :
                                                         $Total-$QueryRow[Amount];
		$Buffer = ($QueryRow[Type]=='Deposit') ? $Buffer.'<tr style="color:green">' : 
                                                         $Buffer.'<tr style="color:red">';

		$Buffer .= '<td align="center" colspan=2>'.$QueryRow[date].'</td>';
		$Buffer .= '<td align="center">'.$QueryRow[Type].'</td>';
		$Buffer .= '<td align="right">'.sprintf("%.2f", $QueryRow[Amount]).'&nbsp;</td>';
		#$Buffer .= '<td align="left">'.$QueryRow[Comment].'</td>';	#originally, changed!!
		if ($QueryRow['private'] == 1) 
		  $Buffer .= '<td align="left"><i>Anonymous</i></td>';
		else
		{
		  $Buffer .= '<td align="center">'.$QueryRow[firstname].'
                                                 '.$QueryRow[lastname].'</td>';
		  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
		  {
		  	$Buffer .= '<td><a href=\'finance.php?delrow=1&id='.$QueryRow[ID].'&llimit='.$llimit.'&rowsonpage='.$rowsonpage.'\'><img alt=\'delete\' src="big.gif" width=20 height=20></a><a href=\'finance.php?modrow=1&id='.$QueryRow[ID].'&llimit='.$llimit.'&rowsonpage='.$rowsonpage.'\'><img alt=\'modify\' src="big.gif" width=20 height=20></a></td>';
		  }
		  $Buffer .= '</td>';
		}
		$Buffer .= '</tr>';
		$Buffer .= '<tr><td align="center" >---------------</td>
                                <td align="left" colspan=5>'
				.$QueryRow[Comment].'</td></tr>';
	}
	$Buffer .= '<tr><td>&nbsp;</td></tr>';
	$Buffer .= '<tr><td colspan=6><hr></td></tr>';
	$Buffer .= '<tr><td colspan=3><b>Balance in Account</b></td>
                        <td align=right>$<font color='
			.strval(($Total+$prevTotal>=0) ? "green" : "red")
			.'>'.sprintf("%.2f", abs($Total+$prevTotal))
			.'</font></td>
			</tr><tr>
			<td>&nbsp;</td>
			</tr></table>';
	$uppertable .= '<td align=left>[Balance, current page</td>
                        <td>=&nbsp;&nbsp;</td>';
	$balcolor    = ($Total>=0) ? "green" : "red";
	$uppertable .= '<td align=right>$<font color='.$balcolor.'>'
		       .sprintf("%.2f", abs($Total))
		       .'</font>]</td>
		</tr><tr>
		<td>&nbsp;</td>
		<td align=left>[Balance Total in account</td><td>=&nbsp;&nbsp;</td>';
	$balcolor = strval(($Total+$prevTotal>=0) ? "green" : "red");
	$uppertable .= '<td align=right>$<font color='.$balcolor.'>'
                      .sprintf("%.2f", abs($Total+$prevTotal))
		      .'</font>]</td>
		       </tr><tr>
		       <td>&nbsp;</td>
		       </tr><tr>
		       <td>&nbsp;</td>
		       </tr>
		       </table>';
	$Buffer = $uppertable.$Buffer;

	return DisplaySection('Financial Records (since '.GetDonationsStartdate().')', $Buffer);
}

function addrow_finance($date,$type,$amount,$comment,$firstname,$lastname,$private)
{
	global $llimit,$rowsonpage;
	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
		{
		$sql="INSERT INTO finances ( ID , date , Type , Amount , Comment , firstname , lastname , private ) 
		VALUES ('', '".$date."', '".$type."', '".$amount."', '".$comment."', '".$firstname."', '".$lastname."', '".$private."');";
		#echo $sql;
		$QueryResult = mysql_query($sql);
		}
	header("Location: finance.php?llimit=".$llimit."&rowsonpage=".$rowsonpage);
	exit;
}

function modrow_finance($id,$date,$type,$amount,$comment,$firstname,$lastname,$private)
{
	global $llimit,$rowsonpage;
	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
		{
		$sql="Update finances set date=\"$date\", Type=\"$type\", Amount=\"$amount\" , Comment=\"$comment\" , firstname=\"$firstname\" , lastname=\"$lastname\" , private=\"$private\" where id=$id";
		#echo $sql;
		$QueryResult = mysql_query($sql);
		}
	header("Location: finance.php?llimit=".$llimit."&rowsonpage=".$rowsonpage);
	exit;
}

function delrow_finance($id)
{
	global $llimit,$rowsonpage;

	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
		{
		$sql="delete from finances where ID=".$id;
		$QueryResult = mysql_query($sql);
		}
	header("Location: finance.php?llimit=".$llimit."&rowsonpage=".$rowsonpage);
	exit;
}

function showadd_finance()
{
	$Buffer="";

	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	{
	$Buffer .= "<table align=center width=75% border=0 cellpadding=0 cellspacing=0>"
		."<form name=\"addform\" method=\"get\" action=\"finance.php\">\n"
		."<tr><td>&nbsp;</td></tr>"
		."<tr><td>&nbsp;</td></tr>"
		."<tr>"
	  	."<td>Date</td>\n"
	  	."<td>Type</td>\n"
	  	."<td>Amount</td>\n"
	  	."<td>Firstname</td>\n"
	  	."<td>Lastname</td>\n"
	  	."</tr><tr>"
	  	."<td><input name=\"fidate\" type=\"text\" id=\"fidate\" size=10></td>\n"
                ."<td><select name=\"fitype\">\n"
		."  <option selected>Deposit\n"
		."  <option>Withdrawal\n"
		."</select>\n"
#	  	."<td><input name=\"fitype\" type=\"text\" id=\"fitype\" size=10></td>\n"
	  	."<td><input name=\"amount\" type=\"text\" id=\"amount\" size=5></td>\n"
	  	."<td><input name=\"firstname\" type=\"text\" id=\"entry\" size=20></td>\n"
	  	."<td><input name=\"lastname\" type=\"text\" id=\"entry\" size=20></td></tr>\n"
	  	."</tr><tr>"
	  	."<td>Comment</td>\n"
	  	."</tr><tr>"
	  	."<td colspan=6 align=center><input name=\"comment\" type=\"text\" id=\"comment\" size=85%></td>\n"
	  	."</tr><tr>"
		."<td>&nbsp;</td>"
	  	."</tr><tr>"
	  	."<td colspan=6 align=center><input type=\"submit\" name=\"Submit\" value=\"Add Transaction\"></td></tr>\n"
		."</form>"
		."</table>";
	}
	return $Buffer;
}

function showmod_finance($id)
{
	$Buffer="";
	$sql="select * from finances where id=".$id.";";
	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	{
		$QueryResult = mysql_query($sql);
		if($QueryRow = mysql_fetch_array($QueryResult))
		{
			$fitypealt = ( ($QueryRow[Type] == "Deposit") ? "Withdrawal" : "Deposit" );
			$Buffer="<table align=center width=75% border=0 cellpadding=0 cellspacing=0>"
				."<form name=\"addform\" method=\"get\" action=\"finance.php\">\n"
				."<tr>"
				."<td>Date</td>\n"
				."<td>Type</td>\n"
				."<td>Amount</td>\n"
				."<td>Firstname</td>\n"
				."<td>Lastname</td>\n"
				."</tr><tr>"
				."<td><input name=\"fidate\" type=\"text\" id=\"fidate\" size=10 value=\"".$QueryRow[date]."\"></td>\n"
				."<td><select name=\"fitype\">\n"
		                ."  <option selected>$QueryRow[Type]\n"
		                ."  <option>$fitypealt\n"
		                ."</select>\n"
#				."<td><input name=\"fitype\" type=\"text\" id=\"fitype\" size=10 value=\"".$QueryRow[Type]."\"></td>\n"
				."<td><input name=\"amount\" type=\"text\" id=\"amount\" size=5 value=\"".$QueryRow[Amount]."\"></td>\n"
				."<td><input name=\"firstname\" type=\"text\" id=\"entry\" size=10 value=\"".$QueryRow[firstname]."\"></td>\n"
				."<td><input name=\"lastname\" type=\"text\" id=\"entry\" size=10 value=\"".$QueryRow[lastname]."\"></td></tr>\n"
				."</tr><tr>"
				."<td>Comment</td>\n"
				."</tr><tr>"
				."<td colspan=6 align=center><input name=\"comment\" type=\"text\" id=\"comment\" size=85% value=\"".$QueryRow[Comment]."\"></td>\n"
				."</tr><tr>"
				."<td>&nbsp;</td>"
				."</tr><tr>"
				."<td colspan=6 align=center><input name=\"id\" type=\"hidden\" id=\"id\" value=\"$id\">"
				."<input type=\"submit\" name=\"Submit\" value=\"Modify Transaction\"></td></tr>\n"
				."</form>"
				."</table>";
		}
	}
	return $Buffer;
}

function show_financeoptions()
{
	global $rowsonpage,$llimit;
	$validrowspp=array(5,10,20,50,100,-1);
	reset($validrowspp);
	
	$Buffer="<table align=center width=75% border=0 cellpadding=0 cellspacing=0>"
		."<form name=\"fioptions\" method=\"get\" action=\"finance.php\">\n"
		."<tr>"
	  	."<td align=center><select name=\"rowsonpage\">";
	foreach ($validrowspp as $value)
	{
#		echo $value;
		if ($value==-1)
		{
			$vrppname="All of them";
		}
		else
		{
			$vrppname=$value;
		}
		$Buffer.="<option value=\"$value\"";
		$Buffer.=($value==$rowsonpage) ? " selected":"";
		$Buffer.=">".$vrppname."</option>";
  	}
	$Buffer.="</select><input name=\"llimit\" type=\"hidden\" id=\"llimit\" value=\"$llimit\">\n";
	$Buffer.="<input type=\"submit\" name=\"Submit\" value=\"Change View\"></td></tr>\n";
	$Buffer.="</form></table>";
	return $Buffer;
}

function showelections($id)
{
  $sql="select * from elections";
  $QueryResult = mysql_query($sql);
  $buffer="<center><table width=\"100%\" border=1>";
  $buffer.="<tr><td width =\"25%\" align=center><b>Title</b></td>
                <td align=center><b>Details</b></td></tr>";
  while ($QueryRow = mysql_fetch_array($QueryResult))
  {
    $buffer.="<tr>";
    $buffer.="<td align=center><b><a href=\"admin.php?elections=yes&&manage=yes&electionid=".$QueryRow[id]."\">".$QueryRow[title]."</a></b></td>
              <td><table width=\"100%\"><tr><td><u>Description</u>: <small>".$QueryRow[description]."</small></td></tr>";
    $buffer.="<tr><td><u>Nominations</u>: <small>".$QueryRow[nominate_start_date]." / ".$QueryRow[nominate_end_date]."</small></td></tr>";
    $buffer.="<tr><td><u>Voting</u>: <small>".$QueryRow[vote_start_date]." / ".$QueryRow[vote_end_date]."</small></td></tr>";
    $buffer.="</table></tr>";
  }
  $buffer.="</table></center>";
	
  return $buffer;
  
}

function showelectionform($id, $change)
{
  if ($id)
  {
    $sql="select * from elections where id=".$id;
    $QueryResult = mysql_query($sql);
    if ($QueryRow = mysql_fetch_array($QueryResult))
    {
      $election_title=$QueryRow[title];
      $election_description=$QueryRow[description];
      $election_max_vote_num=$QueryRow[max_vote_num];
      $election_nominate_begin_date=substr($QueryRow[nominate_start_date],0,10);
      $election_nominate_begin_time=substr($QueryRow[nominate_start_date],11,5);
      $election_nominate_end_date=substr($QueryRow[nominate_end_date],0,10);
      $election_nominate_end_time=substr($QueryRow[nominate_end_date],11,5);
      $election_vote_begin_date=substr($QueryRow[vote_start_date],0,10);
      $election_vote_begin_time=substr($QueryRow[vote_start_date],11,5);
      $election_vote_end_date=substr($QueryRow[vote_end_date],0,10);
      $election_vote_end_time=substr($QueryRow[vote_end_date],11,5);
      $election_cvs_commit_c_date=substr($QueryRow[cvs_commit_c],0,10);
      $election_cvs_commit_c_time=substr($QueryRow[cvs_commit_c],11,5);
      $election_cvs_commit_v_date=substr($QueryRow[cvs_commit_v],0,10);
      $election_cvs_commit_v_time=substr($QueryRow[cvs_commit_v],11,5);
      $submit_election="Change Election";
      $election_id="<input type=\"hidden\" name=\"election_id\" value=\"$id\">";
    }
  }
  else
  {
    $election_title="";
    $election_description="";
    $election_max_vote_num="5";
    $election_nominate_begin_date="0000-00-00";
    $election_nominate_begin_time="00:00";
    $election_nominate_end_date="0000-00-00";
    $election_nominate_end_time="00:00";
    $election_vote_begin_date="0000-00-00";
    $election_vote_begin_time="00:00";
    $election_vote_end_date="0000-00-00";
    $election_vote_end_time="00:00";
    $election_cvs_commit_c_date="2004-01-01";
    $election_cvs_commit_c_time="00:00";
    $election_cvs_commit_v_date="2004-01-01";
    $election_cvs_commit_v_time="00:00";
    $submit_election="Add Election";
    $election_id="";
  }

  $buffer="<center><table cellspacing=0 cellpadding=4 border=1
           width=\"100%\" style=\"border-color:#cccccc\">";
  if ($change)
  {
    $buffer.="<form method=\"post\" action=\"admin.php?elections=yes\">";
  }
  $buffer.="<tr><td width=\"25%\"><b>Title</b><br><small>i.e. Core 2004</small></td><td>
              <input name=\"election_title\" value=\"".$election_title."\"></td></tr>";
  $buffer.="<tr><td><b>Election Description</b></td><td>
                <textarea name=\"election_description\" cols=50 rows=5>".$election_description."</textarea></td></tr>";
  $buffer.="<tr><td><b>Max Number of Votes</b></td><td>
                <input name=\"election_max_vote_num\" size=10 value=\"".$election_max_vote_num."\" maxlength=10></td></tr>";
  $buffer.="<tr><td><b>Nomination Start</b></td><td>
                <input name=\"election_nominate_begin_date\" size=10 value=\"".$election_nominate_begin_date."\" maxlength=10>
                <input name=\"election_nominate_begin_time\" size=5 value=\"".$election_nominate_begin_time."\" maxlength=5></td></tr>";
  $buffer.="<tr><td><b>Nomination End</b></td><td>
                <input name=\"election_nominate_end_date\" size=10 value=\"".$election_nominate_end_date."\" maxlength=10>
                <input name=\"election_nominate_end_time\" size=5 value=\"".$election_nominate_end_time."\" maxlength=5></td></tr>";
  $buffer.="<tr><td><b>Vote Start</b></td><td>
                <input name=\"election_vote_begin_date\" size=10 value=\"".$election_vote_begin_date."\" maxlength=10>
                <input name=\"election_vote_begin_time\" size=5 value=\"".$election_vote_begin_time."\" maxlength=5></td></tr>";
  $buffer.="<tr><td><b>Vote End</b></td><td>
	        <input name=\"election_vote_end_date\" size=10 value=\"".$election_vote_end_date."\" maxlength=10>
                <input name=\"election_vote_end_time\" size=5 value=\"".$election_vote_end_time."\" maxlength=5></td></tr>";
  $buffer.="<tr><td><b>Nominee CVS_Commit</b><br><small>Nominee cutoff date</small></td><td>
                <input name=\"election_cvs_commit_c_date\" size=10 value=\"".$election_cvs_commit_c_date."\" maxlength=10>
                <input name=\"election_cvs_commit_c_time\" size=5 value=\"".$election_cvs_commit_c_time."\" maxlength=5></td></tr>";
  $buffer.="<tr><td><b>Voter CVS_Commit</b><br><small>Voter cutoff date</small></td><td>
                <input name=\"election_cvs_commit_v_date\" size=10 value=\"".$election_cvs_commit_v_date."\" maxlength=10>
                <input name=\"election_cvs_commit_v_time\" size=5 value=\"".$election_cvs_commit_v_time."\" maxlength=5></td></tr>";
  if ($change)
  {
      $buffer.="<tr><td colspan=2 align=center>$election_id<input name=\"submit_election\" type=\"submit\" value=\"".$submit_election."\"></td></tr>";
      $buffer.="</form>";
  }
  $buffer.="</table></center>";
  return $buffer;
}

function shownominees($id, $with_votes)
{
  $sql = "SELECT votes.*, user.fname, user.lname, user.id FROM votes, user
          WHERE votes.election_id = ".$id." AND votes.nominee = 1 AND
          user.id = votes.user_id order by votes.vote_count DESC";

  $QueryResult = mysql_query($sql);
  $Buffer = "<center><table cellspacing=0 cellpadding=4 border=1
             width=\"50%\"><tr><td align=center><b>Candidates</b></td>";
  if ($with_votes)
  {
    $Buffer .="<td align=center><b>Votes</b></td>";
  }
  $Buffer .= "</tr>";
  while ($QueryRow = mysql_fetch_array($QueryResult))
  {
    $Buffer .= "<tr><td><a href=\"miniblurb.php?id=$id&&userid=$QueryRow[user_id]\"
                onClick=\"window.open(this.href, 'VoteBlurb',
                'width=400,height=200,resizable=yes,scrollbars=yes'); return false;\">
                $QueryRow[fname] $QueryRow[lname]</a></td>";

    if ($with_votes)
    {
      $Buffer .= "<td align=center>".$QueryRow[vote_count]."</td>";
    }

    $Buffer .= "</tr>";
  }

  $Buffer.="</table></center>";
  return $Buffer;
}

function NominateForm($blurb, $active, $id, $title)
{
  $submit_str = $blurb ? "Modify" : "Submit";
  $submit_box = $active ? "checked" : "";
  $blurb_len = 1000;
  $Buffer  = "If you are interested in nominating yourself for the ";
  $Buffer .= "<a href=\"nominate.php?elections=yes&&electionid=".$id."\">";
  $Buffer .= $title."</a> election, you'll need to describe your plan, main ";
  $Buffer .= "goals and objectives in improving and building this community should you be elected.";
  $Buffer .= "<br><br>Take this opportunity to set forth your agenda and campaign pledges.";
  $Buffer .= "<br><br>Be as concise and as brief as possible (you have a limited number of words).";
  $Buffer .= "<br><br>";
  $Buffer .= "<center><form action=\"nominate.php\" method=\"post\">";
  $Buffer .= "<table cellpadding=0 cellspacing=0 width=\"80%\">";
  $Buffer .= "<tr><td><center><b>Pledge/Plan/Vision/Mission</b></center></td></tr>";
  $Buffer .= "<tr><td><center><small>(Maximum characters: $blurb_len)</small></center></td></tr>";
# $Buffer .= "<tr><td><textarea name=\"election_nomination\" cols=\"60\" rows=\"10\"></textarea></td></tr>";
  $Buffer .= "<tr><td><textarea name=\"nominate_blurb\" cols=\"60\" rows=\"10\"
              onKeyDown=\"limitText(this.form.nominate_blurb,
              this.form.countdown, $blurb_len);\"
              onKeyUp=\"limitText(this.form.nominate_blurb,this.form.countdown,
              $blurb_len);\">$blurb</textarea></td></tr>";
  $Buffer .= "<tr><td>You have <input readonly type=\"text\" name=\"countdown\" size=\"3\"
              value=\"$blurb_len\"> characters left.</font></td></tr>";
  $Buffer .= "<tr><td><input type=\"checkbox\" name=\"nominee\" value=\"1\"
              $submit_box>Nomination is Active</td></tr>";
  $Buffer .= "<tr><td>&nbsp;</td></tr>";
  $Buffer .= "<tr><td colspan=2 align=center>
              <input type=\"hidden\" name=\"modify\" value=\"1\">
              <input type=\"submit\" name=\"submit\"
                value=\"$submit_str Nomination\"></td></tr>";
  $Buffer .= "</table></form></center>";

  return $Buffer;
}

function showvotingbooth($election_id, $max_vote_num)
{
  $sql = "SELECT votes.*, user.fname, user.lname, user.id, elections.title,
                 elections.description FROM votes, user, elections
          WHERE votes.election_id = ".$election_id."
          AND votes.nominee = 1 AND user.id = votes.user_id
          AND elections.id = ".$election_id.";";
  $QueryResult1=mysql_query($sql);

  $in_loop = 0;
  while ($QueryRow1 = mysql_fetch_array($QueryResult1))
  {
    if (!$in_loop)
    {
      $Buffer  = "<table cellpadding=2 cellspacing=2 border=0 width=\"100%\">";
      $Buffer .= "<tr><td width=\"30%\"><b>Election Title:</b></td><td>"
                  .$QueryRow1[title]."</td></tr>
                  <tr><td><b>Election Description:</b></td><td>"
                  .$QueryRow1[description]."</td></tr></table><br>";
      $Buffer .= "Please select a minimum of 1 and a maximum of "
                  .$max_vote_num." candidates.<br><br>
                  Note: You are not allowed to vote for yourself.<br><br>";
      $Buffer .= "<center><form action=\"vote.php\" method=\"post\">";
      $Buffer .= "<table cellpadding=2 cellspacing=2 border=1 width=\"80%\">";
    }
    $in_loop = 1;
    $Buffer .= "<tr><td><a href=\"miniblurb.php?id=$election_id&&userid=$QueryRow1[user_id]\"
                onClick=\"window.open(this.href, 'VoteBlurb',
                'width=400,height=200,resizable=yes,scrollbars=yes'); return false;\">
                $QueryRow1[fname] $QueryRow1[lname]</a></td>";
    if ($QueryRow1[user_id] == $_SESSION[user_id])
    {
      $Buffer .= "    <td align=center><big><b>X</b></big>
                      </td></tr>";
    }
    else
    {
      $Buffer .= "    <td align=center><input type=\"checkbox\"
                        name=\"candidate[]\" value=\"".$QueryRow1[user_id]."\">
                      </td></tr>";
    }
  }
  $Buffer.="<tr><td colspan=2 align=center><input name=\"submit\"
            type=\"submit\" value=\"Submit Vote\"></td></tr>";
  $Buffer.="</table></form></center>";

  return $Buffer;
}

?>
