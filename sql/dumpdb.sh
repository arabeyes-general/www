#!/bin/sh
#--
# Dump database tables with data
# (excluding donate/finances/user tables)
# $Id$
#--

mysqldump arabeyes cvs > arabeyes.sql
mysqldump arabeyes docs >> arabeyes.sql
mysqldump arabeyes docs_versions >> arabeyes.sql
mysqldump arabeyes donate -d >> arabeyes.sql
mysqldump arabeyes finances -d >> arabeyes.sql
mysqldump arabeyes mailinglists >> arabeyes.sql
mysqldump arabeyes news >> arabeyes.sql
mysqldump arabeyes press >> arabeyes.sql
mysqldump arabeyes proj_about >> arabeyes.sql
mysqldump arabeyes proj_contrib >> arabeyes.sql
mysqldump arabeyes proj_history >> arabeyes.sql
mysqldump arabeyes relatedsites >> arabeyes.sql
mysqldump arabeyes resources >> arabeyes.sql
mysqldump arabeyes todos >> arabeyes.sql
mysqldump arabeyes user -d >> arabeyes.sql
mysqldump arabeyes votes -d >> arabeyes.sql
mysqldump arabeyes elections -d >> arabeyes.sql
