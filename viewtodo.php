<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Viewing One of the Actual Connected User's Todos
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(empty($_GET[todoid]))
{
  DisplayPage('Public Todos', '', ShowAllPublicTodos(), '');
  exit;
}

$QueryResult = mysql_query("select * from todos where todoid=$_GET[todoid]");
$QueryRow = mysql_fetch_array($QueryResult);

if($QueryRow[projectid] == -1)
{
  if($QueryRow[lastmodifierid] != $_SESSION[user_id])
    {
      DisplayError('You are not correctly logged in.');
      exit;
    }
}
else
{
  $UserProjects = GetTodoUserProjects($_SESSION[user_id]);
  $Test = false;
  
  if(count($UserProjects) > 0)
   foreach($UserProjects as $Project)
    if($Project == $QueryRow[projectid])
      $Test = true;
  
  if(!$Test)
    {
      if($QueryRow['public'] == $Publics[count($Publics)-1])
	{
	  DisplayError('This todo is not public.');
	  exit;
	}
      
      DisplayPage('View Todo', 'View Todo #'.$_GET[todoid], ViewPublicTodo($_GET[todoid]), '');
      exit;
    }
}

if($_POST[update] || $_POST[cancel] || $_POST[delete])
{
  if($_POST[update])
    {
      mysql_query("update todos set lastmodifierid='$_SESSION[user_id]',executorid='$_POST[executorid]',priority='$_POST[priority]',date='$_POST[date]',lastmodificationdate='".date("Y-m-d H:i:s", date("U")-date("Z"))."',description='$_POST[description]',details='$_POST[details]',level='$_POST[level]',trash='$_POST[trash]',public='$_POST[publicfield]',state='$_POST[state]' where todoid=$_GET[todoid]");
      
      EmailProjectContributors($_GET[todoid], 1);
      
      header("Location: todos.php");
    }
  if($_POST[delete])
    {
      EmailProjectContributors($_GET[todoid], 2);
      
      mysql_query("delete from todos where todoid=$_GET[todoid]");
      
      header("Location: todos.php");
    }
  if($_POST[cancel])
    {
      header("Location: todos.php");
    }
}
else
{
  DisplayPage('View Todo', 'View Todo #'.$_GET[todoid], ViewTodo($_GET[todoid], $_SESSION[user_id]), '');
}
?>
