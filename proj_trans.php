<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Current Arabeyes' Translation Projects
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

DisplayPage('Translation Projects', 'Help in a Translation Project', HelpTranslationProjects(), '');
?>
