<?php
require_once("credentials.inc.php");

@mysql_select_db($db_name, mysql_pconnect($db_host, $db_user, $db_pass));
header('Content-type: application/xml');
$Buffer = '<?xml version="1.0" encoding="ISO-8859-1"?>' . "\n";

$Buffer .= '<rss version="2.0">
<channel>
<title>Arabeyes</title>
<link>http://www.arabeyes.org</link>
<description>Arabeyes is a Meta project that is aimed at fully supporting the Arabic language in the Unix/Linux environment.</description>
<language>EN</language>
<copyright>Copyright 2004, Arabeyes.org</copyright>
<ttl>60</ttl>
';

if (isset($_GET[news]))
{
  $Buffer .= news();
}

if (isset($_GET[update]))
{
  $Buffer .= update();
}

if (!(isset($_GET[news])) && !(isset($_GET[update])))
{
  $Buffer .= update();
  $Buffer .= news();
}

$Buffer .= '</channel></rss>';

print $Buffer;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function update()
{
  $QueryResult = mysql_query(
	  "SELECT proj_history.log_msg as logmsg, proj_about.proj_name as projname,".
	  " Date_FORMAT(proj_history.log_date, '%a, %d %b %Y') as postdate ".
	  "FROM proj_history LEFT JOIN proj_about ".
	  "ON proj_history.proj_id=proj_about.proj_id ".
	  "ORDER BY proj_history.log_date DESC ".
	  "LIMIT 10");

  $Buffer = "";

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$Buffer .= '<item>' . "\n";
	$Buffer .= '<category>' . 'Updates' . '</category>' . "\n";
	$Buffer .= '<title>' . 
	  htmlentities($QueryRow[projname]) . '</title>' . "\n";
	$Buffer .= '<link>' .
	  htmlentities('http://www.arabeyes.org/project.php?proj=' . 
		  $QueryRow[projname]) . '</link>' . "\n";
	$Buffer .= '<description>'. 
	  htmlentities($QueryRow[logmsg]).  '</description>' . "\n";
	$Buffer .= '<pubDate>' . 
	  $QueryRow[postdate] . ' 00:00:00 GMT</pubDate>' . "\n";
	$Buffer .= '</item>' . "\n";
  }
  return $Buffer;
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
function news()
{
  $QueryResult = mysql_query("
	  SELECT title, article, DATE_FORMAT(date, '%a, %d %b %Y') as postdate 
	  FROM news 
	  ORDER BY date DESC 
	  LIMIT 10");


  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	$Buffer .= '<item>' . "\n";
	$Buffer .= '<category>' . 'News' . '</category>' . "\n";
	$Buffer .= '<title>' . htmlentities($QueryRow[title]) . '</title>' . "\n";
	$Buffer .= '<description>'."\n" . htmlentities($QueryRow[article]) ."\n". '</description>' . "\n";
	$Buffer .= '<link>' . htmlentities('http://www.arabeyes.org/index.php#'.$i) . '</link>' . "\n";
	//$Buffer .= '<description>test ' . $i . '</description>' . "\n";
	$Buffer .= '<pubDate>' . $QueryRow[postdate] . ' 00:00:00 GMT</pubDate>' . "\n";
	$Buffer .= '</item>' . "\n";
  }
  return $Buffer;
}
?>
