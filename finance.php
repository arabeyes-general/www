<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Arabeyes.org's Finances PHP code
 *
 * -----------------
 * Revision Details:
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");
if (!isset($llimit))
{
  $llimit=-1;
}
if (!isset($rowsonpage))
{
  $rowsonpage=10;
}
if (isset($_GET[Submit]))
{
  if ($_GET[Submit]=="Add Transaction")
  {
    addrow_finance($_GET[fidate], $_GET[fitype], $_GET[amount], $_GET[comment],
                   $_GET[firstname], $_GET[lastname], $_GET["private"]);
  }
  if ($_GET[Submit]=="Modify Transaction")
  {
    modrow_finance($_GET[fidate], $_GET[fitype], $_GET[amount], $_GET[comment],
                   $_GET[firstname], $_GET[lastname], $_GET["private"]);
  }
}

if (isset($_GET[delrow]))
{
  if ($delrow==1 && isset($id) && is_numeric($id))
  {
    delrow_finance($id);
  }
}

#echo $rowsonpage;
$buffer=Finances()."<br>".show_financeoptions()."<br>";
if (isset($modrow))
{
  if ($modrow==1 && isset($id) && is_numeric($id))
  {
    $buffer.=showmod_finance($id);
  }
}
else
{
  $buffer.=showadd_finance();
}

DisplayPage('Finances', '', $buffer, '');
?>
