<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Displaying the Actual Connected User's Todos
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(!isset($_SESSION[user_id]))
{
  DisplayError('You are not logged in.');
}
else
{
  DisplayPage('My Todos', '', ShowAllUserTodos($_SESSION[user_id]), '');
}
?>
