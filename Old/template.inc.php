<tr>
   <td align=left valign=top width="98%">
   <br>
   <table class=title cellspacing=0 cellpadding=0 border=0 width="98%">
     <tr><th class=title align=left>
     <img src="icons/slice.gif" widht="13" height="16" alt="" align="top">
       New Layout and Design
     </th></tr>
   </table>
   <br>
   You have guessed it right! This is a new look for
   <a href="http://www.arabeyes.org/">Arabeyes</a>. It is still under
   construction, so please bare with us.
   <p>
   Netscape web browsers may <b>not</b> be able to view this page properly.
   Please use Opera, Konqueror, or IE. If you use Mozilla, please report if
   you are having problems. 
   <p>
   We are still looking for banners and logos for the site. Please submit all
   your work to <a href="mailto:contribute@arabeyes.org">
   contribute@arabeyes.org</a>
   <p>
   Enjoy!
   <p>
   <a href="http://validator.w3.org/check/referer"><img border="0"
       src="http://www.w3.org/Icons/valid-html40"
       alt="Valid HTML 4.0!" height="31" width="88"></a>
   </p>
</td>
