<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
  <META name="description" content="Linux/Unix Arabization">
  <META name="keywords" content="linux,unix,arabic,arabize,arabization,aunyx,arabic linux, arabic unix, linux
arabization">
  <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
  <meta HTTP-EQUIV="Content-Style-Type" CONTENT="text/css">
  <link REL="STYLESHEET" HREF="http://www.arabeyes.org/styles.css" 
	  CHARSET="utf-8" TYPE="text/css">
<!--
  <link REL="icon" HREF="arabeyes-100x100.png" type="image/gif">
-->
  <title><?echo $title?></title>
  <base href="http://www.arabeyes.org/">	
</head>
<?
  include "/etc/ae-webdb.inc"; 
?>
<body>
<!--
<p class="img">
  <img src="images/arabeyes-banner.jpg" alt="Arabeyes Banner" />
</p>
-->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr><td>
  <img src="/images/aelogo.png" alt="Arabeyes Logo" /></td>
<td valign="bottom">
  <img src="/images/arabeyes-banner.png" alt="Arabeyes Banner" /></td>
</tr></table>

<!-- main table - everything -->
<table cellpadding="2" cellspacing="0" border="0" width="100%">
<!-- second row - top menu -->
  <tr><th colspan="2">
  <table border="0" width="100%" cellpadding="0" cellspacing="2"
         style="background-color:#000000">
  <tr><td><table cellspacing="0" cellpadding="2" width="100%"><tr>
	<td class="rheader">
    <a href="about.php">|About|</a>&nbsp;&nbsp;
    <a href="news.php">|News|</a>&nbsp;&nbsp;
    <a href="develop.php">|Development|</a>&nbsp;&nbsp;
    <a href="resources.php">|Resources|</a>&nbsp;&nbsp;
    <a href="people.php">|People|</a>&nbsp;&nbsp;
    <a href="contact.php">|Contact Us|</a>
	</td>
	<td class="lheader" style="colspan:2">
		<a
href=".">|Home|</a>
  </td></tr></table></td></tr></table>
</tr>
