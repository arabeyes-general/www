<tr><td>
<br>

<?

  title_header('Donate to Arabeyes');
  echo '<br>';
  show_content('
<p>
By donating money to the Arabeyes Project you are contributing to the Open
Source community in many ways.  You are above all ensuring the survival of
our Arabisation efforts and the ideals that go with it.
Your small donations can help us:
</p>
<ul>
  <li>Purchase better/new hardware
  <li>Purchase more bandwidth
  <li>Advertise/recruit new volunteers
  <li>Accelerate the realization of some of our projects
  <li>Pay for legal services
  <li>Pay for traveling expenses to attend conferences/seminars	
  <li>..and other costs endured for facilitating the project\'s development
</ul>
<br>
<p>
<b>Donations Remarks:</b>
</p>
<ul>
  <li>As a donor you will be entitled to have access to Arabeyes\' 
      donation and expenditure records, upon request.
  <li>Your privacy is paramount to us and as such it will be guarded
      and dealt with in the utmost of care.
  <li>Donations <b>do not</b> influence Arabeyes.org direction and
      commitment to the Open Source and Arab community.
</ul> 
</p>

</td></tr></table>
<table width="100%" border=0 cellpadding=1 cellspacing=0 bgcolor="#000000">
<tr><td>
<table width="100%" cellpadding=3 cellspacing=0 border=0 bgcolor="#eeeeee">
<tr><td>
<font FACE="verdana, arial, helvetica, sans-serif" SIZE=2 COLOR="#333333">
<b>Donate!</b><br>
<!-- Begin PayPal Logo -->
<form action="https://www.paypal.com/cgi-bin/webscr" methd="post">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="donate@arabeyes.org">
<input type="hidden" name="item_name" value="Arabeyes Project">
<input type="hidden" name="item_number" value="donate">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="return" value="http://www.arabeyes.org/donate/thankyou.php">
<input type="hidden" name="cancel_return"
value="http://www.arabeyes.org/">
<table width="100%" border=0 cellpadding=4 cellspacing=0>
<tr>
<td><font FACE="verdana, arial, helvetica, sans-serif" SIZE=2 COLOR="#333333">
Send $ <input type="text" name="amount" size=5> to Arabeyes.
</font>
</td>
<td>
<input type="image" src="http://images.paypal.com/images/x-click-but04.gif" border="0"
name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!">
</td>
</tr></table>
</form>
<!-- End PayPal Logo -->
</font>
</td>
</tr>
</table>', '');

?>




</td>
