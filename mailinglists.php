<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Mailing Lists
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

DisplayPage('Mailing Lists', 'Arabeyes Mailing Lists', MailingLists(), '');
?>
