<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Main Library
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Main function used to display pages.
function DisplayPage($HTMLHeader, $Title, $Content, $JScript)
{
  global $keywords, $hosturl, $desc;
  
  $Buffer = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">

	<html>

	<head>
	<meta name="description" content="'.$desc.'"></meta>
	<meta name="keywords" content="'.$keywords.'"></meta>
	<meta http-equiv="Content-Type" CONTENT="text/html; charset=utf-8"></meta>
	<meta http-equiv="Content-Style-Type" CONTENT="text/css"></meta>
	<link rel="shortcut icon" href="images/arabeyes/logos/logo_arabic.png"></link>
	<link rel="stylesheet" href="CSS/main.css" charset="utf-8" type="text/css"></link>
        <link rel="alternate" href="rss.php" type="application/rss+xml" title="Arabeyes Updates"></link>
	<title>Arabeyes Project - '.$HTMLHeader.'</title>
	<base href="'.$hosturl.'"></base>
        '.$JScript.'
	</head>
	<body>
	';

  $Buffer .= '  
	<!-- MAIN TABLE BUG extra space -->
	<table class="main" align="center" cellpadding=0 cellspacing=0 border=0 width="98%">
	';

  $Buffer .= '
	<!-- BANNER -->
	<tr>
	<td colspan=3 align="center">
	<a href="'.$hosturl.'">
	<table class="banner" align="center" cellpadding=0 cellspacing=0 border=0 width="100%">
	<tr>
	<td align="left"><img src="images/arabeyes/logos/logo_english.png" border="0" alt="English Arabeyes Logo"></td>
	<td align="right"><img src="images/arabeyes/logos/logo_arabic.png" border="0" alt="Arabic Arabeyes Logo"></td>
	</tr>
	</table>
	</a>
	</td>
	</tr>
	<!-- BANNER END -->
	';

  $Buffer .= '
	<!-- NAVIGATION BAR BUG in the selected and in the new line -->
	<tr>
	<td colspan=3 align="left" valign="middle">
	<table class="navbar" cellpadding=0 cellspacing=0 border=0 width="100%">
	<tr>
	<td>
	&nbsp;<b><a href="." class="navbar">HOME</a></b>&nbsp;|
	&nbsp;<a href="about.php" class="navbar">About</a>&nbsp;|
	&nbsp;<a href="news.php" class="navbar">News</a>&nbsp;|
	&nbsp;<a href="project.php" class="navbar">Projects</a>&nbsp;|
	&nbsp;<a href="resources.php" class="navbar">Resources</a>&nbsp;|
	&nbsp;<a href="docs.php" class="navbar">Documentation</a>&nbsp;|
	&nbsp;<a href="http://www.sourceforge.net/project/showfiles.php?group_id=34866" class="navbar">Downloads</a>&nbsp;|
	&nbsp;<a href="people.php" class="navbar">People</a>&nbsp;|
	&nbsp;<a href="press.php" class="navbar">Press</a>&nbsp;|
	&nbsp;<a href="contact.php" class="navbar">Contact Us</a>&nbsp;
  </td>
<!-- No support yet, thus commented out (Nadim)
<td>
&nbsp;<a href="language.php?locale=en_US.utf8" class="navbar">English</a>&nbsp;|
&nbsp;<a href="language.php?locale=fr_FR.utf8" class="navbar">Français</a>&nbsp;|
&nbsp;<a href="language.php?locale=ar_AE.utf8" class="navbar">عربية</a>&nbsp;
</td>
-->
	</tr>
	</table>
	</td>
	</tr>
	<!-- NAVIGATION BAR END -->
	';

  $Buffer .= '
	<!-- PAGE -->
	<tr valign="top">

	<!-- CONTENT -->
	<td width="80%" valign="top" align="left">
	<table align="center" cellspacing=0 cellpadding=0 border=0 width="95%">
	<tr><td><br>'.DisplaySection($Title, $Content).'
	</td></tr>
	</table>
	<p>&nbsp;</p>
	</td>
	<!-- CONTENT END -->
	';

  $Buffer .= '
	<!-- VERTICAL SEPARATOR -->
	<td class="separator" valign="middle"><img src="images/misc/spacer.gif" alt="">
	</td>
	<!-- VERTICAL SEPARATOR END -->
	';

  $Buffer .= '
	<!-- RIGHT MENU BUG button dont work -->
	<td valign="top" align="right">
	<br>'.DisplayRightMenues().'
	<p>&nbsp;</p>
	</td>
	<!-- RIGHT MENU END -->

	</tr>
	<!-- PAGE END -->
	';

  $Buffer .= '
	<!-- FOOTER -->
	<tr><td colspan="3" align="center" class="copyright">&#169; 2001-2007 Arabeyes. All trademarks and copyrights on this page are owned by their respective owners.</td></tr>
	<!-- FOOTER END -->
	';

  $Buffer .= '
	</table>
	<!-- MAIN TABLE END -->

	</body>

	</html>';

  echo $Buffer;
}

function DisplayJSLimitField()
{
  $buffer = '
        <script src="JavaScript/library.js"></script>

        <script language="javascript" type="text/javascript">
        function limitText(limitField, limitCount, limitNum)
{
  if (limitField.value.length > limitNum)
  {
    limitField.value = limitField.value.substring(0, limitNum);
  }
  else
  {
    limitCount.value = limitNum - limitField.value.length;
  }
}
        </script>';
  return $buffer;
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function DisplaySection($Title, $Content)
{
  if(!empty($Title))
  {
	$Buffer = '
	  <p class="headline">'.$Title.'&nbsp;</p>
	  <p>'.$Content.'
	  </p>';
  }
  else
	$Buffer = $Content;

  return $Buffer;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function GetUTCTimeStamp() {
  return date("YmdHis", date("U")-date("Z"));
}

function AdminWebsite()
{
  $Buffer .= '
	<a href="admin.php?news=yes&&add=yes">Add a News</a><br>
	<a href="admin.php?press=yes&&add=yes">Add a Press Release</a><br>
	<a href="admin.php?resource=yes&&add=yes">Add a Resource</a><br>
	<a href="admin.php?document=yes&&add=yes">Add a Document</a><br>
	<a href="project.php?project=yes&&add=yes">Add a Project</a><br>
	<a href="admin.php?mailinglist=yes&&add=yes">Add a Mailing List</a><br>
	<a href="admin.php?user=yes&&add=yes">Add a User</a><br>
	<a href="admin.php?relatedsite=yes&&add=yes">Add a Related Site</a><br>
	<a href="admin.php?elections=yes&&manage=yes">Manage Elections</a>';
  return $Buffer;
}

function ShowNews($Limit)
{
  $Buffer = '
	<table width="100%" border=0 cellspacing=0 cellpadding=0>';

  if($Limit != 0)
	$QueryResult = mysql_query("select *,DATE_FORMAT(date, '%b %d, %Y') as postdate from news order by date DESC limit ".$Limit);
  else
	$QueryResult = mysql_query("select *,DATE_FORMAT(date, '%b %d, %Y') as postdate from news order by date DESC");

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);

	if($i != 0)
	{
	  $Buffer .= '<tr><td colspan=2><br></td></tr>';
	}

	$Title = htmlspecialchars(stripslashes($QueryRow[title]));
	$Date = htmlspecialchars(stripslashes($QueryRow[postdate]));

	if(isset($_SESSION[type]) && $_SESSION[type] == 'c' && $Limit == 0)
	  $Buffer .= '
		<tr valign="bottom"><td class="doc" align="left"><b>'.$Date.': '.$Title.'</b></td><td class="doc" align="right">[<a href="admin.php?news=yes&&edit=yes&&newsid='.$QueryRow[id].'">Edit</a>]</td></tr>';
	else
	  $Buffer .= '
		<tr valign="bottom"><td class="doc" align="left"><b>'.$Date.': '.$Title.'</b></td><td class="doc" align="right"></td></tr>';

	$Buffer .= '
	  <tr><td colspan=2>'.$QueryRow[article].'</td></tr>';
  }

  if($Limit != 0)
	$Buffer .= '<tr><td colspan=2><br></td></tr><tr><td colspan=2><a href="news.php"><i>More news...</i></a></td></tr>';

  $Buffer .= '</table>';

  return DisplaySection('Arabeyes News', $Buffer);
}
/////////////////////////////////////////////////////////////////////
function ShowProjectUpdates()
{
  $Buffer = "\n<!-- Project Updates -->\n";
  $Buffer .= '<table width=\"100%\" border=0 cellspacing=0 cellpadding=0 ' .
	'class="update">';

  /*
	 $QueryResult = mysql_query("select *, DATE_FORMAT(log_date, '%b %d, %Y') " .
	 "as postdate from proj_history where month(log_date)=" .
	 date("m")." and year(log_date)=".date("Y")." order by log_date DESC");
   */

  $QueryResult = mysql_query(
	  "select *, " .
	  "DATE_FORMAT(log_date, '%b %d, %Y') as postdate " .
	  "from proj_history " .
	  //"where month(log_date)=" . date("m")." and year(log_date)=".date("Y") .
	  " order by log_date DESC " .
	  "LIMIT 5");

  if(mysql_num_rows($QueryResult) < 1)
  {
	$Buffer .= '<tr><td></td><td>There are no project updates for this month' . 
	  ' yet.</td><td></td></tr>';
  }
  else
  {
	$Buffer .=
	  '<TR>' .
	  '<TD width="7px"></TD>' .
	  '<TD width="3px"></TD>' .
	  '<TD class="updatetitle">' . 'Updates'  .
	  '</TD>' .
	  '<TD width="3px"></TD>' .
	  '</TR>'; 
	$Buffer .= '<tr bgcolor="white"><td></td><td>&nbsp;</td><td></td><td></td></tr>';
	for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
	{
	  $QueryRow = mysql_fetch_array($QueryResult);

	  $Buffer .= '<tr><td bgcolor="#99CC00"></td><td></td><td><br></td><td></td></tr>';

	  $log_msg = $QueryRow[log_msg];
	  $log_msg = eregi_replace("<a", "<a class=\"update\"", $log_msg);

	  $Buffer .= 
		'<TR valign="bottom">' .
		'<TD bgcolor="#99cc00"></TD>' .
		'<td></td>' . 
		'<td>' . 
		'<TABLE class="updatecontent" width="100%" border="0" cellpadding="0" cellspacing="0">' .
		'<TR><TD class="updateproject">' .
		GetProjectLink($QueryRow[proj_id]) . '<BR>' . $QueryRow[postdate] .  
		'</TD></TR>' .
		'<TR><TD>' .
		$log_msg . '<BR><BR>' .
		//$QueryRow[log_msg] . '<BR><BR>' .
		'</TD></TR>' .
		'</TABLE>' .
		'</td>' .
		'<TD></TD>' .
		'</tr>';
	}
  }

  $Buffer .= '<tr><td bgcolor="white"></td><td></td><td><br></td><td></td></tr>';
  $Buffer .= '</table>';
  return $Buffer;

  //return DisplayUpdate('Updates for '.date("F").' '.date("Y"), $Buffer);
}
/////////////////////////////////////////////////////////////////////

function Menu($Title, $Table, $OrderType)
{
  $QueryResult = mysql_query("select * from $Table $OrderType");

  $Buffer  = '
	<tr><td class="moduleheader">'.$Title.'</td></tr>';

  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	$Buffer  .= '
	  <tr><td>
	  <table width="100%" border=0 cellspacing=0 cellpadding=0>';

  while($QueryRow = mysql_fetch_array($QueryResult))
  {
	if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	  $Buffer .= '<tr><td class="modulecontent"><a class="modulecontent" href="'.$QueryRow[link].'">'.$QueryRow[name].'</a></td><td align="right">&nbsp;[<a href="admin.php?relatedsite=yes&&edit=yes&&relatedsiteid='.$QueryRow[id].'">Edit</a>]&nbsp;</td></tr>';
	else
	  $Buffer .= '<tr><td class="modulecontent"><a class="modulecontent" href="'.$QueryRow[link].'">'.$QueryRow[name].'</a>&nbsp;</td></tr>';
  }

  if(isset($_SESSION[type]) && $_SESSION[type] == 'c')
	$Buffer  .= '</table></td></tr>';

  return $Buffer;
}

function DisplayRightMenues()
{
  $Buffer = '
	<table align="center" width="90%" border=0 cellspacing=0 cellpadding=0>';

  $Percentage = GetDonationsAmount(GetDonationsStartDate())/GetDonationsGoal();
  $Percentage = intval($Percentage*100.);
  if($Percentage < 0.)
	$Percentage = 0.;
  if($Percentage > 100.)
	$Percentage = 100.;

  $Percentage = $Percentage.'%';

  $Buffer .= '
	<tr><td class="moduleheader">Support Us</td></tr>
	<tr><td class="modulecontent">';
  /* <table>
	 <tr><td>Donations</td><td colspan=2>: '.GetDonationsNumber(GetDonationsStartDate()).'</td></tr>
	 <tr><td>Raised</td><td>: '.GetDonationsAmount(GetDonationsStartDate()).'</td><td>USD</td></tr>
	 <tr><td>Goal</td><td>: '.GetDonationsGoal().'</td><td>USD</td></tr>
	 <tr><td colspan=3>'.ProgressBar($Percentage, GetProgressColor(GetDonationsAmount(GetDonationsStartDate()), 0., GetDonationsGoal())).'</td></tr>
	 </table>
   */

  $Buffer .= '
	<div align="center">
          <a class="modulecontent" href="donate.php">Donate !</a>
 	  <br>
	  <a class="modulecontent" href="thankyou.php">Thank You !</a>
        </div></td></tr>
	<tr><td><br></td></tr>';

  $Buffer .= '
	<tr><td class="moduleheader">Search</td></tr>
	<form method="post" action="http://www.arabeyes.org/swish-e">
	<tr><td class="modulecontent">
	&nbsp;<input size=10 type="text" name="query">&nbsp;<input type="submit" value="Search">&nbsp;<br>
	<a class="modulecontent_small" href="http://www.arabeyes.org/swish-e">Advanced Search&nbsp;</a>&nbsp;
  <input type="hidden" name="si" value="0">
	<input type="hidden" name="si" value="1">
	</td></tr>
	</form>
	<tr><td><br></td></tr>';

  if(isset($_SESSION[username]))
  {
    $Buffer .= '
	  <tr><td class="moduleheader">'.
	  $_SESSION[username].'
	  </td></tr>
	  <tr><td class="modulecontent">
	  <a class="modulecontent" href="logout.php">Logout</a>&nbsp;<br>
	  <a class="modulecontent" href="todos.php">My Todos</a>&nbsp;<br>
	  <a class="modulecontent" href="user.php">Preferences</a>&nbsp;';
	  
    # Nomination Code
    # - check if nomination period is still ongoing/valid
    $mytime = GetDateYMDHM();
//    $sql = "select * from elections where nominate_start_date < '"
//            .$mytime."' and nominate_end_date > '".$mytime."'";
    $sql = "select * from elections where nominate_end_date > '".$mytime."'";
    $QueryResult = mysql_query($sql);
    # there is at least an active election
    if ($QueryRow = mysql_fetch_array($QueryResult))
    {
      # Verify user can nominate him/herself
      $cvs_year=substr($QueryRow[cvs_commit_c],0,4);
      $cvs_mon=substr($QueryRow[cvs_commit_c],5,2);
      $cvs_day=substr($QueryRow[cvs_commit_c],8,2);
      $cvs_hour=substr($QueryRow[cvs_commit_c],11,2);
      $cvs_min=substr($QueryRow[cvs_commit_c],14,2);
      $cvs_time=$cvs_year."-".$cvs_mon."-".$cvs_day." ".$cvs_hour.":".$cvs_min;
      $test=GetUserLastCommit($_SESSION[user_id]);
      if ($test > $cvs_time)
      {
	$Buffer.='<br><a class="modulecontent" href="nominate.php">Nominate Yourself</a>&nbsp;';
      }
    }
	  	
    # Vote Code
    # - check if vote period is still ongoing/valid
    $mytime = GetDateYMDHM();
//    $sql = "select * from elections where vote_start_date < '"
//            .$mytime."' and vote_end_date > '".$mytime."'";
    $sql = "select * from elections where vote_end_date > '".$mytime."'";
    $QueryResult = mysql_query($sql);
    # there is at least an active election
    if ($QueryRow = mysql_fetch_array($QueryResult))
    {
      # check if user has a cvs-commit in the last xx days
      $cvs_year=substr($QueryRow[cvs_commit_v],0,4);
      $cvs_mon=substr($QueryRow[cvs_commit_v],5,2);
      $cvs_day=substr($QueryRow[cvs_commit_v],8,2);
      $cvs_hour=substr($QueryRow[cvs_commit_v],11,2);
      $cvs_min=substr($QueryRow[cvs_commit_v],14,2);
      $cvs_time=$cvs_year."-".$cvs_mon."-".$cvs_day." ".$cvs_hour.":".$cvs_min;
      $test=GetUserLastCommit($_SESSION[user_id]);
      if ($test > $cvs_time)
      {
	$Buffer.='<br><a class="modulecontent" href="vote.php">VOTE</a>&nbsp;';
      }
    }

    $Buffer.='</td></tr>
	  <tr><td><br></td></tr>';
  }
  else
  {
	$Buffer .= '
	  <tr><td class="moduleheader">
	  Login
	  </td></tr>
	  <form method="post" action="index.php">
	  <tr><td class="modulecontent">
	  &nbsp;Username:&nbsp;<input size=8 type="text" name="uname">&nbsp;<br>
	  &nbsp;Password:&nbsp;<input size=8 type="password" name="pass">&nbsp;<br>
	  <input type="submit" name="submit" value="Login">&nbsp;<br>
	  <a class="modulecontent_small" href="index.php?forgotpass=yes">Forgot Password ?</a>&nbsp;
	</td></tr>
	  </form>
	  <tr><td><br></td></tr>';
  }

  if($_SESSION[type] == 'c')
	$Buffer .= '
	  <tr><td class="moduleheader">Administration</td></tr>
	  <tr><td class="modulecontent">
	  <a class="modulecontent" href="admin.php">Full Administration</a>&nbsp;<br>
	  <a class="modulecontent" href="admin.php?email=yes">Send Email</a>&nbsp;
  </td></tr>
	<tr><td><br></td></tr>';


  $Buffer .= '
	<tr><td class="moduleheader">Quick Links</td></tr>
	<tr><td class="modulecontent">
	<a class="modulecontent" href="join.php">Join Us !</a>&nbsp;<br />
	<a class="modulecontent" href="mailinglists.php">Mailing Lists</a>&nbsp;<br />
        <hr width="50%" align="right">
	<a class="modulecontent" href="http://cvs.arabeyes.org/viewcvs">CVS</a>&nbsp;<br />
	<a class="modulecontent" href="http://art.arabeyes.org/">Art</a>&nbsp;<br />
	<a class="modulecontent" href="http://wiki.arabeyes.org/">Wiki</a>&nbsp;<br />
	<a class="modulecontent" href="http://bugs.arabeyes.org/">Bugs</a>&nbsp;<br>
	<a class="modulecontent" href="http://planet.arabeyes.org/">Planet</a>&nbsp;<br />
	<a class="modulecontent" href="viewtodo.php">Todos</a>&nbsp;<br />
	<a class="modulecontent" href="http://qamoose.arabeyes.org/">QaMoose</a>&nbsp;<br />
	<hr width="50%" align="right">
	<a class="modulecontent" href="ace.php">ACE\'s</a>&nbsp;<br />
	<a class="modulecontent" href="history.php">History</a>&nbsp;
  </td></tr>
	<tr><td><br></td></tr>';


  $Buffer .= '
	<tr><td class="moduleheader">Translation Projects</td></tr>
	<tr><td class="modulecontent">';

  $QueryResult = mysql_query("select * from proj_about where hide='N' and type='t' order by proj_name");
  while($QueryRow = mysql_fetch_array($QueryResult))
  {
	$Buffer .= '
	  <a class="modulecontent" href="project.php?proj='.$QueryRow[proj_name].'">'.$QueryRow[proj_name].'</a>&nbsp;<br>';
  }

  $Buffer .= '
	</td></tr>
	<tr><td><br></td></tr>';

  $Buffer .= '
	<tr><td class="moduleheader">Development Projects</td></tr>
	<tr><td class="modulecontent">';

  $QueryResult = mysql_query("select * from proj_about where hide='N' and type='d' order by proj_name");
  while($QueryRow = mysql_fetch_array($QueryResult))
  {
	$Buffer .= '
	  <a class="modulecontent" href="project.php?proj='.$QueryRow[proj_name].'">'.$QueryRow[proj_name].'</a>&nbsp;<br>';
  }

  $Buffer .= '
	</td></tr>
	<tr><td><br></td></tr>';

  $Buffer .= '
	<tr><td class="moduleheader">External Projects</td></tr>
	<tr><td class="modulecontent">';

  $QueryResult = mysql_query("select * from proj_about where hide='N' and type='e' order by proj_name");
  while($QueryRow = mysql_fetch_array($QueryResult))
  {
	$Buffer .= '
	  <a class="modulecontent" href="project.php?proj='.$QueryRow[proj_name].'">'.$QueryRow[proj_name].'</a>&nbsp;<br>';
  }

  $Buffer .= '
	</td></tr>
	<tr><td><br></td></tr>';

  $Buffer .= '
	<tr><td class="moduleheader">Fonts/Art Projects</td></tr>
	<tr><td class="modulecontent">';

  $QueryResult = mysql_query("select * from proj_about where hide='N' and type='f' order by proj_name");
  while($QueryRow = mysql_fetch_array($QueryResult))
  {
	$Buffer .= '
	  <a class="modulecontent" href="project.php?proj='.$QueryRow[proj_name].'">'.$QueryRow[proj_name].'</a>&nbsp;<br>';
  }

  $Buffer .= '
	</td></tr>
	<tr><td><br></td></tr>';

  $Buffer .= Menu('Related Sites', 'relatedsites', 'order by name').'
	<tr><td><br></td></tr>';

  $Buffer .= '
	<tr><td><p>&nbsp;</p></td></tr>
	<tr><td align="center">
            <a href="http://sourceforge.net/projects/arabeyes/">
            <img src="images/misc/sflogo.png" width=90 height=30 border=0
                 alt="SourceForge Logo"></a></td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td align="center">
            <a href="http://www.arabeyes.org/rss.php">
            <img src="images/misc/xmlfeed.gif" border=0
             alt="RSS-XML"></a></td></tr>
';

  $Buffer .= '
	</table>';

  return $Buffer;
}

function JoinUs()
{
  $Buffer = '
	<a href="help.php">How can I help ?</a><br>
	<a href="benefits.php">Benefits</a><br>
	<a href="register.php">Register for an account</a>
	';

  return $Buffer;
}


// Display Project Updates
function DisplayUpdates($Title, $Content)
{
  $Buffer = 
	'<!-- Project Updates -->' .
	'<TABLE border=\"1\">' .
	'<TR>' .
	'<TD width=\"10px\">' .
	'</TD>' .
	'<TD>' . $Title .
	'</TD>' .
	'</TR>' . $Content .
	'</TABLE>';

  return $Buffer;
}

function DisplayOK($Message)
{
  DisplayPage("OK", "Success", '<font color=#33CC00>'.$Message.'</font>', '');
}

function DisplayError($Message)
{
  DisplayPage("Error", "Failure", '<font color=#FF0000>'.$Message.'</font>', '');
}

function ProgressBar($Progress, $BGColor)
{
  $Buffer  = '
	<table width="100%" border=1 cellspacing=0 cellpadding=0>
	<tr><td><table width="'.$Progress.'" cellspacing=0 cellpadding=0>
	<tr><td style="background-color:'.$BGColor.'"><div align=right>'.$Progress.'</div></td></tr>
	</table></td></tr>
	</table>';

  return $Buffer;
}

function GetProgressColor($ActualVal, $MinVal, $MaxVal)
{
  if($ActualVal <= $MinVal)
	return '#FF0000';
  if($ActualVal >= $MaxVal)
	return '#00FF00';

  if($ActualVal <= $MinVal+0.5*($MaxVal-$MinVal))
  {
	$Val = dechex(($ActualVal-$MinVal)/(0.5*($MaxVal-$MinVal))*255);
	if(strlen($Val) < 2)
	  $Val = '0'.$Val;
	return '#FF'.$Val.'00';
  }
  else
  {
	$Val = dechex((1-($ActualVal-0.5*($MinVal+$MaxVal))/(0.5*($MaxVal-$MinVal)))*255);
	if(strlen($Val) < 2)
	  $Val = '0'.$Val;
	return '#'.$Val.'FF00';
  }
}

function GetDateYMD()
{
  $now  = getdate();
  $year = $now[year];
  $mon  = $now[mon];
  $mday = $now[mday];
  $date = sprintf("%04d%02d%02d", $year, $mon, $mday);

  return $date;
}

function GetDateYMDHM()
{
  $datetime = gmdate("Y-m-d H:i");

  return $datetime;
}

function GetUserName($UserID)
{
  $QueryResult = mysql_query("select * from user where id=$UserID");
  if(mysql_num_rows($QueryResult) != 0)
  {
	$QueryRow = mysql_fetch_array($QueryResult);
	return $QueryRow[username];
  }
  else
  {
	return '';
  }
}

function GetUserID($UserName)
{
  $QueryResult = mysql_query("select * from user where username='$UserName'");
  if(mysql_num_rows($QueryResult) != 0)
  {
	$QueryRow = mysql_fetch_array($QueryResult);
	return $QueryRow[id];
  }
  else
  {
	return '';
  }
}

function GetUserFLNames($UserID)
{
  //echo '<BR><BR>'.$UserID.'<BR><BR>';
  if($UserID != '')
  {
	$QueryResult = mysql_query("select * from user where id=$UserID");
	if(mysql_num_rows($QueryResult))
	{
	  $QueryRow = mysql_fetch_array($QueryResult);
	  return $QueryRow[fname].' '.$QueryRow[lname];
	}
	else
	{
	  return '';
	}
  }
  else
  {
	return '';
  }
}

function GetUserEmail($UserID)
{
  $QueryResult = mysql_query("select * from user where id=$UserID");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[email];
}

function GetUserIDFromEmail($UserEmail)
{
  $QueryResult = mysql_query("select * from user where email='$UserEmail'");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[id];
}

function GetUserInfo($UserID)
{
  $QueryResult = mysql_query("select * from user where id='$UserID'");
  $QueryRow = mysql_fetch_array($QueryResult);
  $UserInfo = $QueryRow[fname]." ".$QueryRow[lname]." (".$QueryRow[username].")";
  return $UserInfo;
}

function GetUserIDLink($UserID)
{
  return '<a href="people.php?username='.GetUserName($UserID).'">'.$UserID.'</a>';
}

function GetUserNameLink($UserID)
{
  return '<a href="people.php?username='.GetUserName($UserID).'">'.GetUserName($UserID).'</a>';
}

function GetUserFLNamesLink($UserID)
{
  return '<a href="people.php?username='.GetUserName($UserID).'">'.GetUserFLNames($UserID).'</a>';
}

function GetUserInfoLink($UserID)
{
  return '<a href="people.php?username='.GetUserName($UserID).'">'.GetUserInfo($UserID).'</a>';
}

function GetUserProjects($UserID)
{
  $QueryResult = mysql_query("select * from proj_contrib where userid=$UserID");

  $Buffer = '';

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);
	if ($i != 0)
	  $Buffer = $Buffer.' | ';
	$Buffer = $Buffer.'<a href="project.php?proj='.GetProjectName($QueryRow[proj_id]).'">'.GetProjectName($QueryRow[proj_id]).'</a>';
  }

  if(mysql_num_rows($QueryResult) == 0)
	$Buffer = 'No projects';

  return $Buffer;
}

function GetUserStatus($UserID)
{
  $QueryResult = mysql_query("select * from user where id=$UserID");
  $QueryRow = mysql_fetch_array($QueryResult);

  if($QueryRow[active] == 'Y')
	return 'Active';
  else
	return 'Inactive';
}

function GetUserPicture($UserID)
{
  if(is_file('images/people/'.GetUserName($UserID).'.png'))
  {
	return 'images/people/'.GetUserName($UserID).'.png';
  }
  else
  {
	return 'images/people/nopic.png';
  }
}

function GetUserCommitsNumber($UserID)
{
  $QueryResult = mysql_query("select COUNT(*) as numc from cvs where userid=$UserID");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[numc];
}

function GetUserFirstCommit($UserID)
{
  $QueryResult = mysql_query("select logdate,DATE_FORMAT(logdate, '%Y-%m-%d %H:%i:%s') as commitdate from cvs where userid=$UserID order by logdate");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[commitdate];
}

function GetUserLastCommit($UserID)
{
  $QueryResult = mysql_query("select logdate,DATE_FORMAT(logdate, '%Y-%m-%d %H:%i:%s') as commitdate from cvs where userid=$UserID order by logdate DESC");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[commitdate];
}

function IsPasswordCorrect($UserId, $Password)
{
  $QueryResult = mysql_query("select * from user where pass=PASSWORD('$Password') and id=$UserId");

  if (mysql_num_rows($QueryResult) == 1)
	return true;
  else
	return false;
}

function GetUserAccessLevel($UserID, $ProjectID)
{
  if(empty($UserID) || empty($ProjectID))
	return -1;

  $QueryResult = mysql_query("select * from proj_contrib where userid=$UserID and proj_id=$ProjectID");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[access_level];
}

function GetProjectName($ProjectID)
{
  $QueryResult = mysql_query("select * from proj_about where proj_id=$ProjectID");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[proj_name];
}

function GetProjectID($ProjectName)
{
  $QueryResult = mysql_query("select * from proj_about where proj_name='$ProjectName'");
  $QueryRow = mysql_fetch_array($QueryResult);
  return $QueryRow[proj_id];
}

function GetProjectLink($ProjectID)
{
  return '<a href="project.php?proj=' .
	GetProjectName($ProjectID) . '" class="updateproject">' .
	GetProjectName($ProjectID).'</a>';
}

function GetProjectUsers($ProjectID, $AccessLevel, $MaintainerFlag = false)
{
  if($MaintainerFlag)
	$QueryResult = mysql_query("select * from proj_contrib where proj_id=$ProjectID and access_level>=$AccessLevel");
  else
	$QueryResult = mysql_query("select * from proj_contrib where proj_id=$ProjectID and access_level<=$AccessLevel");

  $Buffer = '';

  for($i = 0; $i < mysql_num_rows($QueryResult); $i++)
  {
	$QueryRow = mysql_fetch_array($QueryResult);
	if ($i != 0)
	  $Buffer = $Buffer.' :: ';
	$Buffer = $Buffer.GetUserNameLink($QueryRow[userid]);
  }

  if(mysql_num_rows($QueryResult) == 0)
  {
	if($MaintainerFlag)
	  $Buffer = 'No maintainer';
	else
	  $Buffer = 'No contributors';
  }

  return $Buffer;
}

function CheckedURL($URL)
{
  if(substr($URL, 0, 7) !=  "http://")
	$URL = "http://".$URL;

  return $URL;
}

function GetVolunteerType()
{
  $VolunteerType = array (
			  "ns" => "Select",
			  "d"  => "Developer",
			  "t"  => "Technical Writer",
			  "g"  => "Graphics Designer",
			  "p"  => "Public Relations Officer"
			 );

  return $VolunteerType;
}

function GetExperianceLevels()
{
  $ExperianceLevels = array (
			     "ns"        => "Select",
			     "sixmonths" => "6 months or less",
			     "about1"    => "1-3 years",
			     "about3"    => "3 Years or more",
			     "5years"    => "5 Years or more"
			     );

  return $ExperianceLevels;
}

function GetArabLeagueCountries()
{
  $ArabLeagueCountries = array (
	  "Algeria", "Bahrain", "Comores", "Djibouti",
	  "Egypt", "Iraq", "Jordan", "Kuwait", "Lebanon",
	  "Libya", "Mauritania", "Morocco", "Oman", "Palestine",
	  "Qatar", "Saudi Arabia", "Somalia", "Sudan", "Syria",
	  "Tunisia", "United Arab Emirates", "Yemen",
	  "Other"
	  );

  return $ArabLeagueCountries;
}

function GetWorldCountries()
{
  $WorldCountries = array (
	  "dz"=>"Algeria",
	  "bh"=>"Bahrain",
	  "km"=>"Comoros",
	  "dj"=>"Djibouti",
	  "eg"=>"Egypt",
	  "iq"=>"Iraq",
	  "jo"=>"Jordan",
	  "kw"=>"Kuwait",
	  "lb"=>"Lebanon",
	  "ly"=>"Libyan Arab Jamahiriya",
	  "mr"=>"Mauritania",
	  "ma"=>"Morocco",
	  "om"=>"Oman",
	  "ps"=>"Palestinian Territories",
	  "qa"=>"Qatar",
	  "sa"=>"Saudi Arabia",
	  "so"=>"Somalia",
	  "sd"=>"Sudan",
	  "sy"=>"Syrian Arab Republic",
	  "tn"=>"Tunisia",
	  "ae"=>"United Arab Emirates",
	  "ye"=>"Yemen",
	  "aa"=>"-----",
	  "af"=>"Afghanistan",
	  "al"=>"Albania",
	  "as"=>"American Samoa",
	  "ad"=>"Andorra",
	  "ao"=>"Angola",
	  "ai"=>"Anguilla",
	  "aq"=>"Antarctica",
	  "ag"=>"Antigua and Barbuda",
	  "ar"=>"Argentina",
	  "am"=>"Armenia",
	  "aw"=>"Aruba",
	  "ac"=>"Ascension Island",
	  "au"=>"Australia",
	  "at"=>"Austria",
	  "az"=>"Azerbaijan",
	  "bs"=>"Bahamas",
	  "bd"=>"Bangladesh",
	  "bb"=>"Barbados",
	  "by"=>"Belarus",
	  "be"=>"Belgium",
	  "bz"=>"Belize",
	  "bj"=>"Benin",
	  "bm"=>"Bermuda",
	  "bt"=>"Bhutan",
	  "bo"=>"Bolivia",
	  "ba"=>"Bosnia and Herzegovina",
	  "bw"=>"Botswana",
	  "bv"=>"Bouvet Island",
	  "br"=>"Brazil",
	  "io"=>"British Indian Ocean Territory",
	  "bn"=>"Brunei Darussalam",
	  "bg"=>"Bulgaria",
	  "bf"=>"Burkina Faso",
	  "bi"=>"Burundi",
	  "kh"=>"Cambodia",
	  "cm"=>"Cameroon",
	  "ca"=>"Canada",
	  "cv"=>"Cap Verde",
	  "ky"=>"Cayman Islands",
	  "cf"=>"Central African Republic",
	  "td"=>"Chad",
	  "cl"=>"Chile",
	  "cn"=>"China",
	  "cx"=>"Christmas Island",
	  "cc"=>"Cocos (Keeling) Islands",
	  "co"=>"Colombia",
	  "cd"=>"Congo, Democratic Republic of the",
	  "cg"=>"Congo, Republic of",
	  "ck"=>"Cook Islands",
	  "cr"=>"Costa Rica",
	  "ci"=>"Cote d'Ivoire",
	  "hr"=>"Croatia/Hrvatska",
	  "cu"=>"Cuba",
	  "cy"=>"Cyprus",
	  "cz"=>"Czech Republic",
	  "dk"=>"Denmark",
	  "dm"=>"Dominica",
	  "do"=>"Dominican Republic",
	  "tp"=>"East Timor",
	  "ec"=>"Ecuador",
	  "sv"=>"El Salvador",
	  "gq"=>"Equatorial Guinea",
	  "er"=>"Eritrea",
	  "ee"=>"Estonia",
	  "et"=>"Ethiopia",
	  "fk"=>"Falkland Islands (Malvina)",
	  "fo"=>"Faroe Islands",
	  "fj"=>"Fiji",
	  "fi"=>"Finland",
	  "fr"=>"France",
	  "gf"=>"French Guiana",
	  "pf"=>"French Polynesia",
	  "tf"=>"French Southern Territories",
	  "ga"=>"Gabon",
	  "gm"=>"Gambia",
	  "ge"=>"Georgia",
	  "de"=>"Germany",
	  "gh"=>"Ghana",
	  "gi"=>"Gibraltar",
	  "gr"=>"Greece",
	  "gl"=>"Greenland",
	  "gd"=>"Grenada",
	  "gp"=>"Guadeloupe",
	  "gu"=>"Guam",
	  "gt"=>"Guatemala",
	  "gg"=>"Guernsey",
	  "gn"=>"Guinea",
	  "gw"=>"Guinea-Bissau",
	  "gy"=>"Guyana",
	  "ht"=>"Haiti",
	  "hm"=>"Heard and McDonald Islands",
	  "va"=>"Holy See (City Vatican State)",
	  "hn"=>"Honduras",
	  "hk"=>"Hong Kong",
	  "hu"=>"Hungary",
	  "is"=>"Iceland",
	  "in"=>"India",
	  "id"=>"Indonesia",
	  "ir"=>"Iran (Islamic Republic of)",
	  "ie"=>"Ireland",
	  "im"=>"Isle of Man",
	  "il"=>"Israel",
	  "it"=>"Italy",
	  "jm"=>"Jamaica",
	  "jp"=>"Japan",
	  "je"=>"Jersey",
	  "kz"=>"Kazakhstan",
	  "ke"=>"Kenya",
	  "ki"=>"Kiribati",
	  "kp"=>"Korea, Democratic People's Republic",
	  "kr"=>"Korea, Republic of",
	  "kg"=>"Kyrgyzstan",
	  "la"=>"Lao People's Democratic Republic",
	  "lv"=>"Latvia",
	  "ls"=>"Lesotho",
	  "lr"=>"Liberia",
	  "li"=>"Liechtenstein",
	  "lt"=>"Lithuania",
	  "lu"=>"Luxembourg",
	  "mo"=>"Macau",
	  "mk"=>"Macedonia, Former Yugoslav Republic",
	  "mg"=>"Madagascar",
	  "mw"=>"Malawi",
	  "my"=>"Malaysia",
	  "mv"=>"Maldives",
	  "ml"=>"Mali",
	  "mt"=>"Malta",
	  "mh"=>"Marshall Islands",
	  "mq"=>"Martinique",
	  "mu"=>"Mauritius",
	  "yt"=>"Mayotte",
	  "mx"=>"Mexico",
	  "fm"=>"Micronesia, Federal State of",
	  "md"=>"Moldova, Republic of",
	  "mc"=>"Monaco",
	  "mn"=>"Mongolia",
	  "ms"=>"Montserrat",
	  "mz"=>"Mozambique",
	  "mm"=>"Myanmar",
	  "na"=>"Namibia",
	  "nr"=>"Nauru",
	  "np"=>"Nepal",
	  "nl"=>"Netherlands",
	  "an"=>"Netherlands Antilles",
	  "nc"=>"New Caledonia",
	  "nz"=>"New Zealand",
	  "ni"=>"Nicaragua",
	  "ne"=>"Niger",
	  "ng"=>"Nigeria",
	  "nu"=>"Niue",
	  "nf"=>"Norfolk Island",
	  "mp"=>"Northern Mariana Islands",
	  "no"=>"Norway",
	  "pk"=>"Pakistan",
	  "pw"=>"Palau",
	  "pa"=>"Panama",
	  "pg"=>"Papua New Guinea",
	  "py"=>"Paraguay",
	  "pe"=>"Peru",
	  "ph"=>"Philippines",
	  "pn"=>"Pitcairn Island",
	  "pl"=>"Poland",
	  "pt"=>"Portugal",
	  "pr"=>"Puerto Rico",
	  "re"=>"Reunion Island",
	  "ro"=>"Romania",
	  "ru"=>"Russian Federation",
	  "rw"=>"Rwanda",
	  "kn"=>"Saint Kitts and Nevis",
	  "lc"=>"Saint Lucia",
	  "vc"=>"Saint Vincent and the Grenadines",
	  "sm"=>"San Marino",
	  "st"=>"Sao Tome and Principe",
	  "sn"=>"Senegal",
	  "sc"=>"Seychelles",
	  "sl"=>"Sierra Leone",
	  "sg"=>"Singapore",
	  "sk"=>"Slovak Republic",
	  "si"=>"Slovenia",
	  "sb"=>"Solomon Islands",
	  "za"=>"South Africa",
	  "gs"=>"South Georgia and the South Sandwich Islands",
	  "es"=>"Spain",
	  "lk"=>"Sri Lanka",
	  "sh"=>"St. Helena",
	  "pm"=>"St. Pierre and Miquelon",
	  "sr"=>"Suriname",
	  "sj"=>"Svalbard and Jan Mayen Islands",
	  "sz"=>"Swaziland",
	  "se"=>"Sweden",
	  "ch"=>"Switzerland",
	  "tw"=>"Taiwan",
	  "tj"=>"Tajikistan",
	  "tz"=>"Tanzania",
	  "th"=>"Thailand",
	  "tg"=>"Togo",
	  "tk"=>"Tokelau",
	  "to"=>"Tonga",
	  "tt"=>"Trinidad and Tobago",
	  "tr"=>"Turkey",
	  "tm"=>"Turkmenistan",
	  "tc"=>"Turks and Caicos Islands",
	  "tv"=>"Tuvalu",
	  "um"=>"US Minor Outlying Islands",
	  "ug"=>"Uganda",
	  "ua"=>"Ukraine",
	  "uk"=>"United Kingdom",
	  "us"=>"United States",
	  "uy"=>"Uruguay",
	  "uz"=>"Uzbekistan",
	  "vu"=>"Vanuatu",
	  "ve"=>"Venezuela",
	  "vn"=>"Vietnam",
	  "vg"=>"Virgin Islands (British)",
	  "vi"=>"Virgin Islands (USA)",
	  "wf"=>"Wallis and Futuna Islands",
	  "eh"=>"Western Sahara",
	  "ws"=>"Western Samoa",
	  "yu"=>"Yugoslavia",
	  "zm"=>"Zambia",
	  "zw"=>"Zimbabwe"
		);
	  return $WorldCountries;
}

function DisplaySelectField($Array, $VisibleArray, $InitialValue)
{
  $Buffer = '';

  for($i = 0; $i < count($Array); $i++)
  {
	if($Array[$i] == $InitialValue)
	  $Buffer .= '<option selected value="'.$Array[$i].'">'.$VisibleArray[$i];
	else
	  $Buffer .= '<option value="'.$Array[$i].'">'.$VisibleArray[$i];
  }

  return $Buffer;
}

function SetLanguage($Locale)
{
  $Domain = "messages";
  $Path   = "./locale";
  
  putenv("LANG=$Locale");
  
  setlocale(LC_ALL, $Locale);
  
  bindtextdomain($Domain, $Path);
  
  textdomain($Domain);
}

function create_selectform($name, $array, $picked)
{
  $mybuffer = '<select name="'.$name.'">';
  foreach ($array as $key => $value)
  {
    if ($key == $picked)
    {
      $mybuffer .= '<option selected value="'.$key.'">'.$value;
    }
    else
    {
      $mybuffer .= '<option value="'.$key.'">'.$value;
    }
  }
  $mybuffer .= '</select>';
  return $mybuffer;
}

function select_arableague($name, $picked)
{
  $arableague = GetArabLeagueCountries();
  $mybuffer = '<select name="'.$name.'">';
  foreach ($arableague as $c) {
	if ($c == $picked) {
	  $mybuffer .= '<option selected value="'.$c.'">'.$c;
	}
	else {
	  $mybuffer .= '<option value="'.$c.'">'.$c;
	}
  }
  return $mybuffer;
}

function select_world($name, $picked)
{
  $world = GetWorldCountries();
  $mybuffer = '<select name="'.$name.'">'.
  '<option value="aa" selected>Select</option>';
  foreach ($world as $c_code => $c_name) {
	if ($c_code == $picked) {
	if($picked != "aa") {
	  $mybuffer .= '<option value="'.$c_code.'" selected>'.$c_name.'</option>';
	  }
	}
	else {
	  $mybuffer .= '<option value="'.$c_code.'">'.$c_name.'</option>';
	}
  }
  return $mybuffer;
}

function NewUserName($FirstName, $LastName)
{
  $FirstName = strtolower(str_replace(" ", "", $FirstName));
  $LastName  = strtolower(str_replace(" ", "", $LastName));
  $LastName  = strtolower(str_replace("-", "_", $LastName));
  $NewUserName = $LastName;

  $QueryResult = mysql_query("select * from user where username='$NewUserName'");
  $FirstNameNbChars = 1;

  while(mysql_num_rows($QueryResult) > 0)
  {
	if($FirstNameNbChars > strlen($FirstName))
	{
	  return ""; 		
	}
	$NewUserName = substr($FirstName, 0, $FirstNameNbChars)."".$LastName;
	$FirstNameNbChars++;
	$QueryResult = mysql_query("select * from user where username='$NewUserName'");
  }

  return $NewUserName;
}

function NewPassword()
{
  global $PasswordMinLength; 

  $NewPassword = "";

  for($i = 0; $i < rand($PasswordMinLength, 2*$PasswordMinLength); $i++)
  {
	$RandomNumber = rand(48,122);

	if(($RandomNumber > 97 && $RandomNumber < 122))
	{
	  $NewPassword .= chr($RandomNumber);
	}
	else if(($RandomNumber > 65 && $RandomNumber < 90))
	{
	  $NewPassword .= chr($RandomNumber);
	}
	else if(($RandomNumber > 48 && $RandomNumber < 57))
	{
	  $NewPassword .= chr($RandomNumber);
	}
	else if($RandomNumber == 95)
	{
	  $NewPassword .= chr($RandomNumber);
	}
	else
	{
	  $i--;
	}
  }

  return $NewPassword;
}

function GetDonationsNumber($StartDate)
{
  $QueryResult = mysql_query("select * from donate where date>='$StartDate'");

  return mysql_num_rows($QueryResult);
}

function GetDonationsGoal()
{
  return "1200";
}

function GetDonationsAmount($StartDate)
{
  $QueryResult = mysql_query("select * from donate where date>='$StartDate'");
  $Total = 0.;

  while($QueryRow = mysql_fetch_array($QueryResult)) 
	$Total += $QueryRow[amount];

  return $Total;
}

function GetDonationsStartDate()
{
  //return "2003-04-25";
  return "2001-07-29";
}

function GetDonationsDeadline()
{
  return "01-Jun-2003";
}

function IsFormComplete($FormVars)
{
  foreach($FormVars as $Key => $Value)
  {
	if (!isset($Key) || ($Value == ""))
	  return false;
  }

  if($FormVars[type] == "ns" || $FormVars[ixexp] == "ns")
  {
	return false;
  }

  return true;
}

function IsCountryValid($CCode) 
{
  if($CCode == "aa")
	return false;
  else 
	return true;
}

function IsEmailValid($Email) 
{
  if(eregi("^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,3}$", $Email))
	return true;
  else
	return false;
}

function SendMail($Sender, $ReplyTo, $To, $Cc, $Bcc, $Subject, $Content)
{
  $Headers  = "MIME-Version: 1.0\n";
  $Headers .= "Content-type: text; charset=utf-8\n";
  $Headers .= "From: ".$Sender."\n";
  $Headers .= "Date: ".date("D, d M Y H:i:s", date("U")-date("Z"))." UTC\n";
  $Headers .= "Reply-To: ".$ReplyTo."\n";
  $Headers .= "Cc: ".$Cc."\n";
  $Headers .= "Bcc: ".$Bcc."\n";
  $Headers .= "X-Priority: 1\n";
  $Headers .= "X-MSMail-Priority: High\n";
  $Headers .= "X-Mailer: Arabeyes.org";

  mail($To, $Subject, $Content, $Headers);
}

function SendRegistrationConfirmation($FirstName, $LastName, $Email)
{
  $Message = "
	Salam ".$FirstName.",

  Thank you for registering with the Arabeyes Project. We are always in
  need of volunteers like yourself.

  Our coordinators will review your application for completeness and will
  perform the necessary tasks to ensure that we can proceed in getting
  you involved in earnest !  If you had entered any wrong information
  in your registration form, please do NOT re-apply and await instructions
  from an Arabeyes representative else mail the CONTACT email address
  mentioned on the website.

	Arabeyes.org Team.";

  SendMail("Arabeyes Support <support@arabeyes.org>", "", $FirstName." ".$LastName." <".$Email.">", "", "", "Arabeyes Registration", $Message);
}

function SendRegistrationNotification($UserID)
{
  global $hosturl;

  $QueryResult = mysql_query("select * from user where id=$UserID");
  $QueryRow = mysql_fetch_array($QueryResult);

  $Message = "
A new user, ".$QueryRow[fname]." ".$QueryRow[lname].", has registered.

User Information:
-----------------

- Full Name       : ".$QueryRow[fname]." ".$QueryRow[lname]."
- Birthdate       : ".$QueryRow[birthdate]."
- Country/Origin  : ".$QueryRow[country]."
- Residence       : ".$QueryRow[residence]."
- Phone           : ".$QueryRow[phone]."
- Email           : ".$QueryRow[email]."
- Homepage        : ".$QueryRow[homepage]."

- UserID          : ".$UserID."
- Username        : ".$QueryRow[username]."
- Userpage        : ".$hosturl."people.php?username=".$QueryRow[username]."

- Volunteer Type  : ".$QueryRow[type]." (d=developer, t=translator, g=graphics, p=PR)
- *n*x Experience : ".$QueryRow[ixexp]."

- About             :
".$QueryRow[about]."

- How did you hear about Arabeyes ?
".$QueryRow[howheard];

  SendMail("Arabeyes Support <support@arabeyes.org>", $QueryRow[email], "Arabeyes Contact <contact@arabeyes.org>", "", "", "Registered [".$QueryRow[type]."] - ".$QueryRow[fname]." ".$QueryRow[lname], $Message);
}

function SendNewPassword($UserID, $NewPassword)
{
  $Message = "
	Salam,

  This is your Arabeyes auto-generated password: ".$NewPassword."

	Please remember it, and keep it in a safe place ;-)

	Thank you,

  Arabeyes.org Support.";

  SendMail("Arabeyes Support <support@arabeyes.org>", "", GetUserFLNames($UserID)." <".GetUserEmail($UserID).">", "", "", "Arabeyes User Password", $Message);
}

function getalluserinfo($id)
{
  $sql="select * from user where id=".$id;
  $QueryResult=mysql_query($sql);
  return mysql_fetch_array($QueryResult);
}

?>
