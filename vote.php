<?php
/************************************************************************
 * $Id: vote.php,v 1.0 2004/10/08 00:04:00 omar
 *
 * ------------
 * Description:
 * ------------
 * Arabeyes.org's Vote PHP code
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

if(isset($_SESSION[username]))
{
  # check if there is an open and not ended election going on
  $do_sendmail = 1;
  $mytime = GetDateYMDHM();
//  $sql = "SELECT * from elections WHERE vote_start_date < '".$mytime."'
//          AND vote_end_date > '".$mytime."'";
  $sql = "SELECT * from elections WHERE vote_end_date > '".$mytime."'";
  $QueryResult = mysql_query($sql);
  # Find out if there is a valid election
  if ($QueryRow = mysql_fetch_array($QueryResult))
  {
    $sql = "SELECT * from elections WHERE vote_start_date > '".$mytime."'
            AND id =".$QueryRow[id];
    $QueryResult1 = mysql_query($sql);
    # Find out if we are too early to it
    if ($QueryRow1 = mysql_fetch_array($QueryResult1))
    {
      $Buffer .= "The upcoming '".$QueryRow1[title]."' election is slated to
                  commence at ".$QueryRow1[vote_start_date]."
                  , so please come back then to vote.
                  <br><br>In short, you are a bit too early :-)";
    }
    else
    {
      # check if user has a cvs-commit in the last xx days
      $cvs_year=substr($QueryRow[cvs_commit_v],0,4);
      $cvs_mon=substr($QueryRow[cvs_commit_v],5,2);
      $cvs_day=substr($QueryRow[cvs_commit_v],8,2);
      $cvs_hour=substr($QueryRow[cvs_commit_v],11,2);
      $cvs_min=substr($QueryRow[cvs_commit_v],14,2);
      $cvs_time=$cvs_year."-".$cvs_mon."-".$cvs_day." ".$cvs_hour.":".$cvs_min;
      $test=GetUserLastCommit($_SESSION[user_id]);
      if ($test > $cvs_time)
      {
        # Check if user has already voted to this election
	$sql="SELECT * from votes WHERE election_id=".$QueryRow[id]."
              AND user_id=".$_SESSION[user_id]." AND voted = 1";
	$QueryResult1=mysql_query($sql);
	if ($QueryRow1 = mysql_fetch_array($QueryResult1))
	{
	  $Buffer .= "You've already voted for the '".$QueryRow[title]."'
                      election.";
	}
	else
	{
	  if ($_POST[submit] == "Submit Vote")
	  {
            # Make sure user voted for specified number of candidates
	    $vote_self = 0;
	    $vote_count = 0;
	    if ($_POST[candidate])
	    {
	      foreach ($_POST[candidate] as $key => $value)
	      {
		$vote_count++;
		if ($value == $_SESSION[user_id])
		{
		  $vote_self = 1;
		}
	      }
	    }
	    if ($vote_self)
	    {
	      $Buffer.="Sorry, you are <b>not</b> allowed to vote for
                        yourself.<br><br>
                        Please choose again.<br><br>";
	      $Buffer.=showvotingbooth($QueryRow[id], $QueryRow[max_vote_num]);
	    }
	    else if ($vote_count > $QueryRow[max_vote_num])
	    {
	      $Buffer.="You have voted for more than the allowable number
                        of candidates.  You are asked to select a maximum
                        of <b>".$QueryRow[max_vote_num]."</b> nominees.<br><br>
                        Please choose again.<br><br>";
	      $Buffer.=showvotingbooth($QueryRow[id], $QueryRow[max_vote_num]);
	    }
	    else if (!$vote_count)
	    {
	      $Buffer.="You didn't make any selections.<br><br>
                        Please choose again.<br><br>";
	      $Buffer.=showvotingbooth($QueryRow[id], $QueryRow[max_vote_num]);
	    }
	    else
	    {
	      # We have a valid number of votes
	      $userinfo = getalluserinfo($_SESSION[user_id]);
	      $webc   ="Thank you for taking part in the '".$QueryRow[title]."' vote.<br><br>";
	      $webc  .= "An email has been dispatched to <b> ";
	      $webc  .= "'".$userinfo[email]."'</b> as a means to confirm ";
	      $webc  .= "your selections. If you have any problems contact ";
	      $webc  .= "'support -at- Arabeyes -dot- org'<br><br>";
	      $webc  .="Voter Name : ".$userinfo[fname]." "
		       .$userinfo[lname]."<br>";
	      $webc  .="Date : ".$mytime." UTC<br>";
	      $webc  .="Election : ".$QueryRow[title]."<br>";

	      $email  ="Thank you for participating in Arabeyes.org ";
	      $email .="\"".$QueryRow[title]."\" elections.\n";
	      $email .="This email is meant as a confirmation of your selections.\n\n";
	      $email .="Voter Name : ".$userinfo[fname]." ".$userinfo[lname]."\n";
	      $email .="Date       : ".$mytime." UTC\n";
	      $email .="Election   : ".$QueryRow[title]."\n";

	      $myrand =rand();
	      $webc  .="Random # : ".$myrand."\n<br>";
	      $email .="Random Num : ".$myrand."\n\n";

	      $count = 0;
	      foreach ($_POST[candidate] as $key => $value)
	      {
		if ($value != 0)
		{
		  $count++;
		  $sql = "UPDATE votes SET vote_count = vote_count+1
                          WHERE election_id=".$QueryRow[id]."
                          AND user_id=".$value." AND nominee = 1; ";
	          # Do the actual update here
		  mysql_query($sql);

		  # Store off voter ID in those you voted for (sanity check)
  		  $sql = "UPDATE votes SET vote_from =
                           CONCAT_WS(',', vote_from, '".$_SESSION[user_id]."')
                           WHERE user_id = ".$value;
 	          # Do the actual update here
 		  mysql_query($sql);

		  $QR = mysql_fetch_array(mysql_query
                          ("SELECT fname, lname FROM user WHERE id=".$value));
		  $email .= "Voted for  : ".$QR[fname]." ".$QR[lname]."\n";
		  $webc .= "$count. Voted for : ".$QR[fname]." ".$QR[lname]."\n<br>";
		}
	      }

	      $Buffer  .= $webc;
	      $emailmd5 = md5($email);
	      $email   .= "---\nmd5-hash   : ".$emailmd5."\n";

              # Note that the voter has voted (update vs. insert)
	      $sql = "SELECT * from votes
                      WHERE election_id=".$QueryRow[id]." AND user_id="
                      .$_SESSION[user_id];
	      $QueryResult2 = mysql_query($sql);
	      if ($QueryRow2=mysql_fetch_array($QueryResult2))
	      {
 		$sql = "UPDATE votes set voted = 1, voted_date = '".$mytime."',
                         email_verif = '".$email."' WHERE election_id = "
 		        .$QueryRow[id]." AND user_id=".$_SESSION[user_id];
	      }
	      else
	      {
 		$sql = "INSERT into votes (user_id, election_id, voted,
                         email_verif)
                        VALUES
 			('".$_SESSION[user_id]."','".$QueryRow[id]."', 1,
                         '".$email."');";
	      }

	      $QueryResult2=mysql_query($sql);

	      if ($do_sendmail)
	      {
		SendMail("Arabeyes Support <support@arabeyes.org>", "",
			 GetUserFLNames($_SESSION[user_id])
			 ." <".GetUserEmail($_SESSION[user_id]).">",
			 "", "", "Arabeyes Election confirmation", $email);
	      }
	    }
	  }
	  else
	  {
	    $Buffer .= showvotingbooth($QueryRow[id], $QueryRow[max_vote_num]);
	  }
        }
      }
      else
      {
        $Buffer .= "Sorry, you don't have the proper credentials to vote.";
      }
    }
  }
  else
  {
      $Buffer .= "Sorry there are no valid elections now.";
  }
}
else
{
  DisplayError('You are not correctly logged in.');
}

DisplayPage('Vote', 'Vote', $Buffer, '');
?>
