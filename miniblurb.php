<?php
/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 * Code to access a candidate's election blurb or pledge
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

require_once("arabeyes.php");

$Buffer = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
	   <html>
	   <head>
	   <meta http-equiv="Content-Type" CONTENT="text/html; charset=utf-8">
           </meta>
	   <title>Arabeyes - Candidate Blurb</title>
	   <base href="'.$hosturl.'"></base>
	   </head>
	   <body>';

$sql = "SELECT votes.nominate_blurb, user.fname, user.lname FROM votes, user
        WHERE votes.election_id=".$_GET[id]."
        AND votes.user_id=".$_GET[userid]." AND user.id=".$_GET[userid];

$QueryResult1 = mysql_query($sql);

if ($QueryRow1 = mysql_fetch_array($QueryResult1))
{
  $Buffer .= "<u>Candidate Name</u>: ".$QueryRow1[fname]." ".$QueryRow1[lname];
  $Buffer .= "<br><br>$QueryRow1[nominate_blurb]";
}
else
{
  $Buffer .= "<br>Something went horribly wrong.  You've tried to access
              something that doesn't have an election blurb.<br>";
}

$Buffer .='</body>
	   </html>';

echo $Buffer;

?>
